<?php
return array(
    'router' => array(
        'routes' => array(
            'amandia_es' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/aes',
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'Aes',
                        'action' => 'index',
                    ),
                ),
            ),
            /*
             *  order
             */
            'amandia_es_admin_order' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-order-admin[/:id][/]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminOrder',
                        'action' => 'index',
                    ),
                ),
            ),
            'amandia_es_admin_order_search' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-order-admin-search[/:page][/]',
                    'constraints' => array(
                        'page' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminOrder',
                        'action' => 'search',
                    ),
                ),
            ),
            /*
             *  product
             */
            'amandia_es_admin_product' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-product-admin[/:id][/]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminProduct',
                        'action' => 'index',
                    ),
                ),
            ),
            'amandia_es_admin_product_choose' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-product-choose/for[/:origin[/:group[/[:page]]]][/]', // must be compatible with pagination/*.phtml
                    'constraints' => array(
                        'origin' => '[0-9]+',
                        'group' => '[0-9]+',
                        'page' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminProduct',
                        'action' => 'choose',
                    ),
                ),
            ),
            'amandia_es_admin_product_search' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-product-search[/:page][/]',
                    'constraints' => array(
                        'page' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminProduct',
                        'action' => 'search',
                    ),
                ),
            ),
            /*
             *  article
             */
            'amandia_es_admin_article' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-article-admin[/:id][/]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminArticle',
                        'action' => 'index',
                    ),
                ),
            ),
            'amandia_es_admin_article_choose' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-article-choose/for[/:origin[/:group[/[:page]]]][/]', // must be compatible with pagination/*.phtml
                    'constraints' => array(
                        'origin' => '[a-z0-9-]+', // origintype-originId
                        'group' => '[0-9]+',
                        'page' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminArticle',
                        'action' => 'choose',
                    ),
                ),
            ),
            'amandia_es_admin_article_search' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-article-search[/[:page]][/]',
                    'constraints' => array(
                        'page' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminArticle',
                        'action' => 'search',
                    ),
                ),
            ),
            /*
             *  material
             */
            'amandia_es_admin_material' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-material-admin[/[:id]][/]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminMaterial',
                        'action' => 'index',
                    ),
                ),
            ),
            'amandia_es_admin_material_choose' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-material-choose/for[/:origin[/:group[/[:page]]]][/]', // must be compatible with pagination/*.phtml
                    'constraints' => array(
                        'origin' => '[a-z0-9-]+', // origintype-originId
                        'group' => '[0-9]+',
                        'page' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminMaterial',
                        'action' => 'choose',
                    ),
                ),
            ),
            'amandia_es_admin_material_search' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-material-search[/[:page]][/]',
                    'constraints' => array(
                        'page' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminMaterial',
                        'action' => 'search',
                    ),
                ),
            ),
            /*
             *  service
             */
            'amandia_es_admin_service' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-service-admin[/:id][/]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminService',
                        'action' => 'index',
                    ),
                ),
            ),
            'amandia_es_admin_service_choose' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-service-choose/for[/:origin[/:group[/[:page]]]][/]', // must be compatible with pagination/*.phtml
                    'constraints' => array(
                        'origin' => '[a-z0-9-]+', // origintype-originId
                        'group' => '[0-9]+',
                        'page' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminService',
                        'action' => 'choose',
                    ),
                ),
            ),
            'amandia_es_admin_service_search' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-service-search[/[:page]][/]',
                    'constraints' => array(
                        'page' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminService',
                        'action' => 'search',
                    ),
                ),
            ),
            /*
             *  factory order
             */
            'amandia_es_admin_factoryorder' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-factory-order-admin[/faid/:faid][/foid/:foid][/]',
                    'constraints' => array(
                        'faid' => '[0-9]+',
                        'foid' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminFactoryOrder',
                        'action' => 'index',
                    ),
                ),
            ),
            'amandia_es_admin_factoryorder_search' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-factory-order-search[/:page][/]',
                    'constraints' => array(
                        'page' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminFactoryOrder',
                        'action' => 'search',
                    ),
                ),
            ),
            /*
             *  factory article
             */
            'amandia_es_admin_factoryarticle' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-factory-article-admin[/:id][/]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminFactoryArticle',
                        'action' => 'index',
                    ),
                ),
            ),
            'amandia_es_admin_factoryarticle_search' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-factory-article-search[/:page][/]',
                    'constraints' => array(
                        'page' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminFactoryArticle',
                        'action' => 'search',
                    ),
                ),
            ),
            'amandia_es_admin_factoryarticle_choose' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-factoryarticle-choose/for[/:origin[/:group[/[:page]]]][/]', // must be compatible with pagination/*.phtml
                    'constraints' => array(
                        'origin' => '[a-z0-9-]+',
                        'group' => '[0-9]+',
                        'page' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminFactoryArticle',
                        'action' => 'choose',
                    ),
                ),
            ),
            /*
             *  customer
             */
            'amandia_es_admin_customer' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-customer-admin[/:id][/]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminCustomer',
                        'action' => 'index',
                    ),
                ),
            ),
            'amandia_es_admin_customer_choose' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-customer-choose[/:origintype[/:origin[/:group[/:page]]]][/]', // must be compatible with pagination/choose-advanced.phtml
                    'constraints' => array(
                        'origintype' => '[a-z]+',
                        'origin' => '[0-9]+',
                        'group' => '[0-9]+',
                        'page' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminCustomer',
                        'action' => 'choose',
                    ),
                ),
            ),
            'amandia_es_admin_customer_search' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-customer-search[/:page][/]',
                    'constraints' => array(
                        'page' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller',
                        'controller' => 'AdminCustomer',
                        'action' => 'search',
                    ),
                ),
            ),
            /*
             *  REST
             */
            'amandia_es_rest_customer' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-rest-customer[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller\Rest',
                        'controller' => 'RestCustomer',
                    ),
                ),
            ),
            'amandia_es_rest_product_article' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-rest-productarticle[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller\Rest',
                        'controller' => 'RestProductArticle',
                    ),
                ),
            ),
            'amandia_es_rest_product_material' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-rest-productmaterial[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller\Rest',
                        'controller' => 'RestProductMaterial',
                    ),
                ),
            ),
            'amandia_es_rest_product_service' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-rest-productservice[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller\Rest',
                        'controller' => 'RestProductService',
                    ),
                ),
            ),
            'amandia_es_rest_product_factoryarticle' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-rest-productfactoryarticle[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller\Rest',
                        'controller' => 'RestProductFactoryArticle',
                    ),
                ),
            ),
            'amandia_es_rest_factoryarticle_article' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-rest-factoryarticlearticle[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller\Rest',
                        'controller' => 'RestFactoryArticleArticle',
                    ),
                ),
            ),
            /*
             *  AJAX
             */
            'amandia_es_ajax_modal_product' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/aes-ajax-modal-product[/]',
//                    'constraints' => array(
//                        'id' => '[0-9]+',
//                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'AmandiaES\Controller\Ajax',
                        'controller' => 'ModalProduct',
                        'action' => 'chooseProduct',
                    ),
                ),
            ),
        ),
    ),
);
