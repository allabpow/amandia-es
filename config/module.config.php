<?php
return array(
    'controllers' => array(
        'factories' => array(
        ),
        'invokables' => array(
//            'AmandiaES\Controller\RestCustomer' => 'AmandiaES\Controller\RestCustomerController',
        ),
    ),
    'controller_plugins' => array(
        'invokables' => array(
            'cleanCooseContainer' => 'AmandiaES\Controller\Plugin\CleanChooseContainer',
            'routeNameWithoutEndingSlash' => 'AmandiaES\Controller\Plugin\RouteNameWithoutEndingSlash',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
        ),
        'invokables' => array(
            // Tables
            'AmandiaES\Table\Order' => 'AmandiaES\Table\OrderTable',
            'AmandiaES\Table\Customer' => 'AmandiaES\Table\CustomerTable',
            'AmandiaES\Table\CustomerGroup' => 'AmandiaES\Table\CustomerGroupTable',
            'AmandiaES\Table\Product' => 'AmandiaES\Table\ProductTable',
            'AmandiaES\Table\ProductGroup' => 'AmandiaES\Table\ProductGroupTable',
            'AmandiaES\Table\Material' => 'AmandiaES\Table\MaterialTable',
            'AmandiaES\Table\MaterialGroup' => 'AmandiaES\Table\MaterialGroupTable',
            'AmandiaES\Table\Article' => 'AmandiaES\Table\ArticleTable',
            'AmandiaES\Table\ArticleGroup' => 'AmandiaES\Table\ArticleGroupTable',
            'AmandiaES\Table\FactoryOrder' => 'AmandiaES\Table\FactoryOrderTable',
            'AmandiaES\Table\FactoryArticle' => 'AmandiaES\Table\FactoryArticleTable',
            'AmandiaES\Table\FactoryArticleGroup' => 'AmandiaES\Table\FactoryArticleGroupTable',
            'AmandiaES\Table\Service' => 'AmandiaES\Table\ServiceTable',
            'AmandiaES\Table\ServiceGroup' => 'AmandiaES\Table\ServiceGroupTable',
            // Tables Disposition
            'AmandiaES\Table\DispositionMaterial' => 'AmandiaES\Table\DispositionMaterialTable',
            'AmandiaES\Table\DispositionProduct' => 'AmandiaES\Table\DispositionProductTable',
            // Tables Stock
//            'AmandiaES\Table\StockProduct' => 'AmandiaES\Table\StockProductTable',
            'AmandiaES\Table\StockMaterial' => 'AmandiaES\Table\StockMaterialTable',
            // Tables etc
            'AmandiaES\Table\Config' => 'AmandiaES\Table\ConfigTable',
            'AmandiaES\Table\Supplier' => 'AmandiaES\Table\SupplierTable',
            'AmandiaES\Table\CostCenter' => 'AmandiaES\Table\CostCenterTable',
            'AmandiaES\Table\SymbolGeom' => 'AmandiaES\Table\SymbolGeomTable',
            'AmandiaES\Table\SymbolWeight' => 'AmandiaES\Table\SymbolWeightTable',
            'AmandiaES\Table\Location' => 'AmandiaES\Table\LocationTable',
            'AmandiaES\Table\Shipment' => 'AmandiaES\Table\ShipmentTable',
            'AmandiaES\Table\ShipmentGroup' => 'AmandiaES\Table\ShipmentGroupTable',
            'AmandiaES\Table\Language\Lang' => 'AmandiaES\Table\Language\Lang',
            'AmandiaES\Table\Country' => 'AmandiaES\Table\CountryTable',
            // Forms
            // Listener
            'AmandiaES\Listener\Amandia' => 'AmandiaES\Listener\AmandiaListener',
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'pagination/chooseSimple' => __DIR__ . '/../view/pagination/choose-simple.phtml',
            'pagination/chooseDefault' => __DIR__ . '/../view/pagination/choose-default.phtml',
            'pagination/chooseAdvanced' => __DIR__ . '/../view/pagination/choose-advanced.phtml',
            'pagination/searchDefault' => __DIR__ . '/../view/pagination/search-default.phtml',
            /*
             * Layout
             */
            'layout/amandia' => __DIR__ . '/../view/layout/amandia01.phtml',
            'layout/modal' => __DIR__ . '/../view/layout/modal.phtml',
            /*
             * Template
             */
            'layout/header' => __DIR__ . '/../view/layout/header.phtml',
            'layout/sidebar' => __DIR__ . '/../view/layout/sidebar.phtml',
            'layout/footer' => __DIR__ . '/../view/layout/footer.phtml',
        ),
        'template_path_stack' => array(
             __DIR__ . '/../view', // 'AmandiaES' =>
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'view_helpers' => array(
        'invokables' => array(
        ),
        'factories' => array(
        ),
    ),
    'amandia_es' => array(
        'demo' => FALSE,
        'default_lang' => 'de',
        'languages' => array('en'),
        'shop_enabled' => FALSE,
        'default_currency' => 'EUR',
        'paginationCount' => 10,
        'tax' => 19,
        'document_root' => __DIR__ . '/../data/docs',
        'draft_path' => __DIR__ . '/../data/draft',
        'origintypes' => array( // Routen muessen mit ID funzen
            'order' => 'amandia_es_admin_order',
            'product' => 'amandia_es_admin_product',
            'service' => 'amandia_es_admin_service',
            'facart' => 'amandia_es_admin_factoryarticle',
        ),
    ),
);
