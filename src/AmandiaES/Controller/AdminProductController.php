<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Db\Sql\Sql;
use Zend\Crypt\Password\Bcrypt;

/**
 * 
 */
class AdminProductController extends AbstractActionController {

    /**
     *
     * @var \BitkornUser\Service\AccessService
     */
    private $accessService;

    /**
     *
     * @var \AmandiaES\Form\SearchProductForm 
     */
    private $searchProductForm;

    /**
     *
     * @var \AmandiaES\Form\ProductForm 
     */
    private $formProduct;

    /**
     *
     * @var \AmandiaES\Form\ProductTextForm 
     */
    private $formProductText;

    /**
     *
     * @var \AmandiaES\Table\ProductTable
     */
    private $productTable;

    /**
     *
     * @var \AmandiaES\Table\ProductGroupTable
     */
    private $productGroupTable;

    /**
     *
     * @var \AmandiaES\Table\StockProductTable
     */
//    private $stockProductTable;

    /**
     *
     * @var \AmandiaES\Table\DispositionProductTable
     */
//    private $dispositionProductTable;

    /**
     *
     * @var \AmandiaES\Table\MaterialTable 
     */
    private $materialTable;

    /**
     *
     * @var \AmandiaES\Table\ArticleTable 
     */
    private $articleTable;

    /**
     *
     * @var \AmandiaES\Table\SupplierTable 
     */
    private $supplierTable;

    /**
     *
     * @var \AmandiaES\Table\OrderTable
     */
    private $orderTable;

    /**
     *
     * @var \AmandiaES\Table\FactoryOrderTable 
     */
    private $factoryOrderTable;

    /**
     *
     * @var \AmandiaES\Table\FactoryArticleTable 
     */
    private $factoryArticleTable;

    /**
     *
     * @var \AmandiaES\Table\ServiceTable 
     */
    private $serviceTable;

    /**
     *
     * @var \AmandiaES\Table\Language\Lang 
     */
    private $langTable;

    function __construct() {
        
    }

    /**
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction() {
        if (!$this->accessService->isUserInGroup(2)) {
            return new ViewModel(array(
                'message' => array(
                    'level' => 'warn',
                    'text' => 'du kommst hier net rein'
                ),
            ));
        }
        $logger = $this->serviceLocator->get('logger');
        $config = $this->serviceLocator->get('config');
        $aesConfig = $config['amandia_es'];

        $product = array();
        $productTotalPriceSell = 0;
        $textKeysAvailable = FALSE;

        $id = $this->params('id');
        $this->formProduct = $this->serviceLocator->get('AmandiaES\Form\Product');
        $this->formProductText = $this->serviceLocator->get('AmandiaES\Form\ProductText');
        $productCalculator = $this->serviceLocator->get('AmandiaES\Service\Shop\Calculator');
        $articlesKeyVal = array();
        $articles = array();
        $materialKeyVal = array();
        $materials = array();
        $serviceKeyVal = array();
        $services = array();
        $factoryArticleKeyVal = array();
        $factoryArticles = array();
        if (isset($id)) {
            $product = $this->productTable->getProduct($id);
            if (!$product) {
                return $this->redirect()->toRoute('amandia_es_admin_product_search');
            }
            $this->formProduct->setData($product);
            // articles
            $articlesKeyVal = json_decode($product['product']['articles'], TRUE);
            if ($articlesKeyVal) {
                $articles = $this->articleTable->getArticlesByIds(array_keys($articlesKeyVal));
            }
            // materials
            $materialKeyVal = json_decode($product['product']['materials'], TRUE);
            if ($materialKeyVal) {
                $materials = $this->materialTable->getMaterialsByIds(array_keys($materialKeyVal));
            }
            // services
            $serviceKeyVal = json_decode($product['product']['services'], TRUE);
            if ($serviceKeyVal) {
                $services = $this->serviceTable->getServicesByIds(array_keys($serviceKeyVal));
            }
            // factory articles
            $factoryArticleKeyVal = json_decode($product['product']['factory_articles'], TRUE);
            if ($factoryArticleKeyVal) {
                $factoryArticles = $this->factoryArticleTable->getFactoryArticlesByIds(array_keys($factoryArticleKeyVal));
            }
            // product price
            $productTotalPriceSell = $productCalculator->setProduct($product['product'])->calculateProductPriceSell();
            // product text
            $textKeysAvailable = $this->productTable->textKeysAvailable($id);
            if ($textKeysAvailable) {
                $textFormData = array(
                    'text_display_name' => $this->langTable->getLangValue($aesConfig['default_lang'], $id . 'text_display_name'),
                    'text_short' => $this->langTable->getLangValue($aesConfig['default_lang'], $id . 'text_short'),
                    'text_long' => $this->langTable->getLangValue($aesConfig['default_lang'], $id . 'text_long'),
                );
                $this->formProductText->setData($textFormData);
            }
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
            if (isset($postData['submit_product']) || isset($postData['choose_article']) || isset($postData['choose_material']) || isset($postData['choose_service']) || isset($postData['choose_factory_article'])) { // Product Form
                $this->formProduct->setData($postData);
                if ($this->formProduct->isValid()) {
                    $formData = $this->formProduct->getData();
                    // product speichern
                    $productId = $this->productTable->saveProduct($formData);
                    if (is_int($productId)) {
                        if(isset($postData['submit_product'])) {
                            // muss nicht 2015-06-28
//                            return $this->redirect()->toRoute('amandia_es_admin_product', array('id' => $productId));
                        }
                        if (isset($postData['choose_article'])) { // Artikel choose
                            return $this->redirect()->toRoute('amandia_es_admin_article_choose', array('origin' => 'product-' . $productId));
                        }
                        if (isset($postData['choose_material'])) { // Material choose
                            return $this->redirect()->toRoute('amandia_es_admin_material_choose', array('origin' => 'product-' . $productId));
                        }
                        if (isset($postData['choose_service'])) { // Service choose
                            return $this->redirect()->toRoute('amandia_es_admin_service_choose', array('origin' => 'product-' . $productId));
                        }
                        if (isset($postData['choose_factory_article'])) { // Factory Article choose
                            return $this->redirect()->toRoute('amandia_es_admin_factoryarticle_choose', array('origin' => 'product-' . $productId));
                        }
                        $product = $this->productTable->getProduct($productId);
                        $this->formProduct->setData($product);
                        $productTotalPriceSell = $productCalculator->setProduct($product['product'])->calculateProductPriceSell();
                        $textKeysAvailable = $this->productTable->textKeysAvailable($id);
                        $this->layout()->message = array(
                            'level' => 'info',
                            'text' => 'erfolgreich gespeichert - id=' . $productId
                        );
                    } else { // database error
                        $this->layout()->message = array(
                            'level' => 'error',
                            'text' => 'nicht gespeichert - result=' . $productId
                        );
                    }
                } else { // form invalid
                    $this->layout()->message = array(
                        'level' => 'warn',
                        'text' => 'nicht alle eingegebenen Daten sind valide'
                    );
                }
            } elseif (isset($postData['submit_product_text'])) { // Product Text Form
                $this->formProductText->setData($postData);
                if ($this->formProductText->isValid()) {
                    $resultName = $this->langTable->saveWithPreGluePostKey('de', $id, 'text_display_name', $postData['text_display_name']);
                    $resultShort = $this->langTable->saveWithPreGluePostKey('de', $id, 'text_short', $postData['text_short']);
                    $resultLong = $this->langTable->saveWithPreGluePostKey('de', $id, 'text_long', $postData['text_long']);
                    if ($resultName && $resultShort && $resultLong) {
                        $this->layout()->message = array(
                            'level' => 'info',
                            'text' => 'Texte gespeichert'
                        );
                    }
                } else { // form invalid
                    $formData = $this->formProductText->getData();
                    $this->layout()->message = array(
                        'level' => 'warn',
                        'text' => 'nicht alle eingegebenen Daten sind valide'
                    );
                }
            }
        } // isPost

        return new ViewModel(array(
            'productForm' => $this->formProduct,
            'productTotalPriceSell' => $productTotalPriceSell,
            'articleKeyVal' => $articlesKeyVal,
            'articles' => $articles,
            'materialKeyVal' => $materialKeyVal,
            'materials' => $materials,
            'serviceKeyVal' => $serviceKeyVal,
            'services' => $services,
            'factoryArticleKeyVal' => $factoryArticleKeyVal,
            'factoryArticles' => $factoryArticles,
            'textKeysAvailable' => $textKeysAvailable,
            'productId' => $id,
            'formProductText' => $this->formProductText,
        ));
    }

    /**
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function chooseAction() {
        $origin = (int) $this->params()->fromRoute('origin');
        if (empty($origin)) {
            return;
        }
        $order = $this->orderTable->getOrderById($origin);

        $config = $this->serviceLocator->get('config');
        $aesConfig = $config['amandia_es'];

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
        }

        $page = (int) $this->params()->fromRoute('page');

        $group = 1;
        $groupParam = (int) $this->params()->fromRoute('group');
        if ($groupParam) {
            $group = $groupParam;
        }
        if (isset($postData) && isset($postData['group_select'])) {
            $group = (int) $postData['group_select'];
            return $this->redirect()->toRoute('amandia_es_admin_product_choose', array('origin' => $origin, 'group' => $group, 'page' => $page));
        }

        // Choosing POST
        if (isset($postData)) {
            if (isset($postData['choose_add'])) {
                $chooseId = (int) $postData['choose_id'];
                if ($this->orderTable->updateProducts($origin, $chooseId, 'plus')) {
                    return $this->redirect()->refresh();
                }
            } elseif ($postData['choose_cull']) {
                $chooseId = (int) $postData['choose_id'];
                $productFoCount = $this->factoryOrderTable->countFactoryOrders($order['id'], array($chooseId));
                $orderTmp = $this->orderTable->getOrderById($order['id']);
                $productsTmp = json_decode($orderTmp['products'], TRUE);
                $productsCount = $productsTmp[$chooseId];
                if (($productsCount - $productFoCount) > 0) { // product without factory order
                    if ($this->orderTable->updateProducts($origin, $chooseId, 'minus')) {
                        return $this->redirect()->refresh();
                    }
                } else {
                    $this->layout()->message = array(
                        'level' => 'warn',
                        'text' => 'Es bestehen Betriebsaufträge die zuerst gelöscht werden müssen'
                    );
                }
            }
        }

        // Pagination
        $selectProducts = $this->productTable->getSelectProductsAll($group, 'id');
        $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\DbSelect($selectProducts, $this->productTable->getSql()));
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($aesConfig['paginationCount']); // muss noch aus einer Configuration geholt werden
        $productsCount = $paginator->getCurrentItemCount();

        $orderProducts = json_decode($order['products'], TRUE);

        return new ViewModel(array(
            'isProducts' => ($productsCount ? TRUE : FALSE),
            'paginator' => $paginator,
            'page' => $page,
            'group' => $group,
            'groupsIdNameAssoc' => $this->productGroupTable->getProductGroupsIdNameAssocc(),
            'origin' => $origin,
            'orderProducts' => ($orderProducts ? $orderProducts : array()),
        ));
    }

    /**
     * 
     */
    public function searchAction() {
        $logger = $this->serviceLocator->get('logger');
        $config = $this->serviceLocator->get('config');
        $aesConfig = $config['amandia_es'];
        $productGroupsIdNameAssoc = $this->productGroupTable->getProductGroupsIdNameAssocc();
        $suppliersIdNameAssoc = $this->supplierTable->getSuppliersIdNameAssocc();
        $paginator = NULL;
        $page = 0;
        $productsCount = 0;
        $orderColumn = 'name';

        $this->searchProductForm = $this->serviceLocator->get('AmandiaES\Form\SearchProduct');

        $getParams = $this->params()->fromQuery();
        if ($getParams) {
            $this->searchProductForm->setData($getParams);
            if ($this->searchProductForm->isValid()) {
                // Pagination
                $page = $this->params()->fromRoute('page');
                $selectProducts = $this->productTable->searchProduct($logger, $this->searchProductForm->getData(), $orderColumn, TRUE);
                $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\DbSelect($selectProducts, $this->productTable->getSql()));
                $paginator->setCurrentPageNumber($page);
                $paginator->setItemCountPerPage($aesConfig['paginationCount']); // muss noch aus einer Configuration geholt werden
                $productsCount = $paginator->getCurrentItemCount();
            }
        }

        return new ViewModel(array(
            'searchProductForm' => $this->searchProductForm,
            'productGroupsIdName' => $productGroupsIdNameAssoc,
            'suppliersIdName' => $suppliersIdNameAssoc,
            'paginator' => $paginator,
            'page' => $page,
            'isProducts' => $productsCount,
            'paramsSearch' => $getParams,
        ));
    }

    public function setAccessService(\BitkornUser\Service\AccessService $accessService) {
        $this->accessService = $accessService;
    }

    public function setProductTable(\AmandiaES\Table\ProductTable $productTable) {
        $this->productTable = $productTable;
    }

    public function setProductGroupTable(\AmandiaES\Table\ProductGroupTable $productGroupTable) {
        $this->productGroupTable = $productGroupTable;
    }

//    public function setStockProductTable(\AmandiaES\Table\StockProductTable $stockProductTable) {
//        $this->stockProductTable = $stockProductTable;
//    }
//
//    public function setDispositionProductTable(\AmandiaES\Table\DispositionProductTable $dispositionProductTable) {
//        $this->dispositionProductTable = $dispositionProductTable;
//    }

    public function setMaterialTable(\AmandiaES\Table\MaterialTable $materialTable) {
        $this->materialTable = $materialTable;
    }

    public function setArticleTable(\AmandiaES\Table\ArticleTable $articleTable) {
        $this->articleTable = $articleTable;
    }

    public function setSupplierTable(\AmandiaES\Table\SupplierTable $supplierTable) {
        $this->supplierTable = $supplierTable;
    }

    public function setOrderTable(\AmandiaES\Table\OrderTable $orderTable) {
        $this->orderTable = $orderTable;
    }

    public function setFactoryOrderTable(\AmandiaES\Table\FactoryOrderTable $factoryOrderTable) {
        $this->factoryOrderTable = $factoryOrderTable;
    }

    public function setFactoryArticleTable(\AmandiaES\Table\FactoryArticleTable $factoryArticleTable) {
        $this->factoryArticleTable = $factoryArticleTable;
    }

    public function setServiceTable(\AmandiaES\Table\ServiceTable $serviceTable) {
        $this->serviceTable = $serviceTable;
    }

    public function setLangTable(\AmandiaES\Table\Language\Lang $langTable) {
        $this->langTable = $langTable;
    }

}
