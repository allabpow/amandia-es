<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

/**
 * 
 */
class AdminCustomerController extends AbstractActionController {

    
    /**
     *
     * @var \Zend\Log\Logger
     */
    private $logger;

    /**
     *
     * @var \AmandiaES\Table\CustomerTable 
     */
    private $customerTable;

    /**
     *
     * @var \AmandiaES\Table\CustomerGroupTable 
     */
    private $customerGroupTable;

    /**
     *
     * @var \AmandiaES\Table\OrderTable
     */
    private $orderTable;

    
    public function indexAction() {
        $this->logger = $this->serviceLocator->get('logger');
        $containerUser = new Container('user');
        if (!isset($containerUser->groups) || !in_array('2', $containerUser->groups)) {
            return new ViewModel(array(
                'message' => array(
                    'level' => 'warn',
                    'text' => 'du bist kein Manager'
                ),
            ));
        }
        $customer = FALSE;
        $id = $this->params('id');
        $customerForm = $this->serviceLocator->get('AmandiaES\Form\Customer');

        if (isset($id)) {
            $customer = $this->customerTable->getCustomerById($id);
            if ($customer) {
                $customerForm->setData(array('customer' => $customer));
            } else {
                unset($id);
            }
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
            $customerForm->setData($postData);
            if ($customerForm->isValid()) {
//                $this->logger->log(\Zend\Log\Logger::DEBUG, print_r($customerForm->getData(), TRUE));
                $customerId = $this->customerTable->saveCustomer($customerForm->getData(), $this->logger);
                if (is_int($customerId)) {
                    $customer = $this->customerTable->getCustomerById($customerId);
                    $customerForm->setData(array('customer' => $customer));
                    $this->layout()->message = array(
                        'level' => 'info',
                        'text' => 'erfolgreich gespeichert - id=' . $customerId
                    );
                } else { // database error
                    $this->layout()->message = array(
                        'level' => 'error',
                        'text' => 'nicht gespeichert - bitte den Administrator benachrichtigen'
                    );
                }
            }
        }

        return new ViewModel(array(
            'customerForm' => $customerForm,
        ));
    }

    /**
     * per POST aufgerufen
     * aufrufendes Formular (Referer) gibt natürlich alle Daten mit
     * u.a. auch die schon vorhandenen (gechoosden) materials
     * 
     * hier wird ein Formular mit allen materials angezeigt
     * ...in denen die schon gewählt sind, die mit POST kamen.
     * 
     * Wird das chooseFormular abgesendet
     * ...Empfänger ist der Referer
     * ...kommt das Referer-Formular mit aktualisierten materials
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function chooseAction() {
        $originType = $this->params()->fromRoute('origintype');
        $originId = (int)$this->params()->fromRoute('origin');
        $config = $this->serviceLocator->get('config');
        $aesConfig = $config['amandia_es'];
        $refererRoute = $aesConfig['origintypes'][$originType];
        if(empty($originType) || empty($originId) || empty($refererRoute)) {
            return;
        }

        $page = $this->params()->fromRoute('page');

        $group = 1;
        $groupParam = $this->params()->fromRoute('group');
        if ($groupParam) {
            $group = $groupParam;
        }
        
        $groupSelect = (int)$this->params()->fromPost('group_select');
        if ($groupSelect) {
            return $this->redirect()->toRoute('amandia_es_admin_customer_choose', array('origintype' => $originType, 'origin' => $originId, 'group' => $groupSelect, 'page' => $page));
        }

        // Choosing
        $customerGet = (int)$this->params()->fromQuery('chooseCustomer');
        if($customerGet) {
            switch ($originType) {
                case 'order':
                    if($this->orderTable->updateField($originId, 'customer_id', $customerGet)) {
                        return $this->redirect()->toRoute($refererRoute, array('id' => $originId));
                    }
            }
        }
        
        // Pagination
        $selectCustomers = $this->customerTable->getSelectCustomersAll($group, 'name_first');
        $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\DbSelect($selectCustomers, $this->customerTable->getSql()));
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($aesConfig['paginationCount']);
        $customerCount = $paginator->getCurrentItemCount();
        
        return new ViewModel(array(
            'isCustomers' => ($customerCount ? TRUE : FALSE),
            'paginator' => $paginator,
            'origintype' => $originType,
            'origin' => $originId,
            'page' => $page,
            'group' => $group,
            'groupsIdNameAssoc' => $this->customerGroupTable->getCustomerGroupsIdNameAssocc(),
            'refererRoute' => $refererRoute,
        ));
    }

    /**
     * 
     */
    public function searchAction() {
        $this->logger = $this->serviceLocator->get('logger');
        $config = $this->serviceLocator->get('config');
        $aesConfig = $config['amandia_es'];
        $paginator = NULL;
        $page = 0;
        $customerCount = 0;
        $orderColumn = 'customer_nr DESC';

        $searchCustomerForm = $this->serviceLocator->get('AmandiaES\Form\SearchCustomer');

        $getParams = $this->params()->fromQuery();
        if ($getParams) {
            $searchCustomerForm->setData($getParams);
            if ($searchCustomerForm->isValid()) {
                // Pagination
                $page = $this->params()->fromRoute('page');
                $selectCustomer = $this->customerTable->searchCustomer($this->logger, $searchCustomerForm->getData(), $orderColumn, TRUE);
                $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\DbSelect($selectCustomer, $this->customerTable->getSql()));
                $paginator->setCurrentPageNumber($page);
                $paginator->setItemCountPerPage($aesConfig['paginationCount']); // muss noch aus einer Configuration geholt werden
                $customerCount = $paginator->getCurrentItemCount();
            }
        }

        return new ViewModel(array(
            'searchCustomerForm' => $searchCustomerForm,
            'paginator' => $paginator,
            'page' => $page,
            'customerCount' => $customerCount,
            'paramsSearch' => $getParams,
        ));
    }

    public function setCustomerTable(\AmandiaES\Table\CustomerTable $customerTable) {
        $this->customerTable = $customerTable;
    }
    
    public function setCustomerGroupTable(\AmandiaES\Table\CustomerGroupTable $customerGroupTable) {
        $this->customerGroupTable = $customerGroupTable;
    }
    
    public function setOrderTable(\AmandiaES\Table\OrderTable $orderTable) {
        $this->orderTable = $orderTable;
    }


}
