<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

/**
 * 
 */
class AdminArticleController extends AbstractActionController {


    /**
     *
     * @var \Zend\Log\Logger
     */
    private $logger;

    /**
     *
     * @var \AmandiaES\Table\ArticleTable 
     */
    private $articleTable;

    /**
     *
     * @var \AmandiaES\Table\ArticleGroupTable 
     */
    private $articleGroupTable;

    /**
     *
     * @var \AmandiaES\Table\ProductTable 
     */
    private $productTable;

    /**
     *
     * @var \AmandiaES\Table\FactoryArticleTable 
     */
    private $factoryArticleTable;

    /**
     *
     * @var \AmandiaES\Table\SupplierTable 
     */
    private $supplierTable;

    public function indexAction() {
        $this->logger = $this->serviceLocator->get('logger');
        $containerUser = new Container('user');
        if (!isset($containerUser->groups) || !in_array('2', $containerUser->groups)) {
            $this->layout()->message = array(
                'level' => 'warn',
                'text' => 'du bist kein Manager'
            );
            return new ViewModel(array());
        }
        $config = $this->serviceLocator->get('config');
        $aesConfig = $config['amandia_es'];

        $id = $this->params('id');
        $articleForm = $this->serviceLocator->get('AmandiaES\Form\Article');

        if (isset($id)) {
            $article = $this->articleTable->getArticleById($id);
            if ($article) {
                $articleForm->setData(array('article' => $article));
            } else {
                uset($id);
            }
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
            $articleForm->setData($postData);
            if ($articleForm->isValid()) {
                $articleId = $this->articleTable->saveArticle($postData['article']);
                if (is_int($articleId)) {
                    $this->layout()->message = array(
                        'level' => 'info',
                        'text' => 'erfolgreich gespeichert - id=' . $articleId
                    );
                } else { // database error
                    $this->layout()->message = array(
                        'level' => 'error',
                        'text' => 'nicht gespeichert - bitte den Administrator benachrichtigen'
                    );
                }
            }
        }

        return new ViewModel(array(
            'articleForm' => $articleForm,
        ));
    }

    /**
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function chooseAction() {
        $this->logger = $this->serviceLocator->get('logger');
        $originString = $this->params()->fromRoute('origin');
        $origins = explode('-', $originString);
        if (!is_array($origins) || (count($origins) != 2)) {
            $this->logger->log(\Zend\Log\Logger::NOTICE, 'Bad call of chooseAction(crash) in ' . __CLASS__ . '; ' . print_r($origins,true));
            return;
        }
        $originType = htmlspecialchars(trim($origins[0]));
        $originId = (int)$origins[1];
        $config = $this->serviceLocator->get('config');
        $aesConfig = $config['amandia_es'];
        if (!in_array($originType, array_keys($aesConfig['origintypes']))) {
            $this->logger->log(\Zend\Log\Logger::NOTICE, 'Bad call of chooseAction(origintype) in ' . __CLASS__);
            return;
        }
        $chooser = NULL;
        $originTypeName = '';
        if ($originType == 'product') {
            $chooser = $this->productTable->getProductById($origins[1]);
            $originTypeName = 'Produkt';
        } elseif ($originType == 'facart') {
            $chooser = $this->factoryArticleTable->getFactoryArticleById($origins[1]);
            $originTypeName = 'B-Artikel';
        }
        $originRoute = $aesConfig['origintypes'][$origins[0]];


        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
        }

        $page = $this->params()->fromRoute('page');

        $group = 1;
        $groupParam = $this->params()->fromRoute('group');
        if ($groupParam) {
            $group = $groupParam;
        }
        if (isset($postData) && isset($postData['group_select'])) {
            $group = (int) $postData['group_select'];
            return $this->redirect()->toRoute('amandia_es_admin_article_choose', array('origin' => $originString, 'group' => $group, 'page' => $page));
        }

        $paramsGet = $this->params()->fromQuery();

        // Choosing
        if (isset($paramsGet['chooseAdd'])) {
            $chooseId = (int) $paramsGet['chooseAdd'];
            if ($originType == 'product') {
                if ($this->productTable->updateArticles($originId, $chooseId, 'plus')) {
                    return $this->redirect()->refresh();
                }
            } elseif ($originType == 'facart') {
                if ($this->factoryArticleTable->updateArticles($originId, $chooseId, 'plus')) {
                    return $this->redirect()->refresh();
                }
            }
        }
        if (isset($paramsGet['chooseCull'])) {
            $chooseId = (int) $paramsGet['chooseCull'];
            if ($originType == 'product') {
                if ($this->productTable->updateArticles($originId, $chooseId, 'minus')) {
                    return $this->redirect()->refresh();
                }
            } elseif ($originType == 'facart') {
                if ($this->factoryArticleTable->updateArticles($originId, $chooseId, 'minus')) {
                    return $this->redirect()->refresh();
                }
            }
        }

        // Pagination
        $selectArticles = $this->articleTable->getSelectArticlesAll($group, 'id');
        $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\DbSelect($selectArticles, $this->articleTable->getSql()));
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($aesConfig['paginationCount']); // muss noch aus einer Configuration geholt werden
        $articlesCount = $paginator->getCurrentItemCount();

        $chooserArticles = json_decode($chooser['articles'], TRUE);

        return new ViewModel(array(
            'isArticles' => ($articlesCount ? TRUE : FALSE),
            'paginator' => $paginator,
            'page' => $page, // wird pagination-route-parameter
            'group' => $group, // wird pagination-route-parameter
            'groupsIdNameAssoc' => $this->articleGroupTable->getArticleGroupsIdNameAssocc(),
            'origin' => $originString, // wird pagination-route-parameter
            'originId' => $originId,
            'originTypeName' => $originTypeName,
            'originName' => $chooser['name'],
            'originRoute' => $originRoute,
            'chooserArticles' => ($chooserArticles ? $chooserArticles : array()),
        ));
    }

    /**
     * 
     */
    public function searchAction() {
        $this->logger = $this->serviceLocator->get('logger');
        $config = $this->serviceLocator->get('config');
        $aesConfig = $config['amandia_es'];
        $articleGroupsIdNameAssoc = $this->articleGroupTable->getArticleGroupsIdNameAssocc();
        $suppliersIdNameAssoc = $this->supplierTable->getSuppliersIdNameAssocc();
        $paginator = NULL;
        $page = 0;
        $articleCount = 0;
        $orderColumn = 'article_nr DESC';

        $searchArticleForm = $this->serviceLocator->get('AmandiaES\Form\SearchArticle');

        $getParams = $this->params()->fromQuery();
        if ($getParams) {
            $searchArticleForm->setData($getParams);
            if ($searchArticleForm->isValid()) {
                // Pagination
                $page = $this->params()->fromRoute('page');
                $selectArticle = $this->articleTable->searchArticle($this->logger, $searchArticleForm->getData(), $orderColumn, TRUE);
                $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\DbSelect($selectArticle, $this->articleTable->getSql()));
                $paginator->setCurrentPageNumber($page);
                $paginator->setItemCountPerPage($aesConfig['paginationCount']); // muss noch aus einer Configuration geholt werden
                $articleCount = $paginator->getCurrentItemCount();
            }
        }

        return new ViewModel(array(
            'searchArticleForm' => $searchArticleForm,
            'articleGroupsIdName' => $articleGroupsIdNameAssoc,
            'suppliersIdName' => $suppliersIdNameAssoc,
            'paginator' => $paginator,
            'page' => $page,
            'articleCount' => $articleCount,
            'paramsSearch' => $getParams,
        ));
    }

    public function setArticleTable(\AmandiaES\Table\ArticleTable $articleTable) {
        $this->articleTable = $articleTable;
    }

    public function setArticleGroupTable(\AmandiaES\Table\ArticleGroupTable $articleGroupTable) {
        $this->articleGroupTable = $articleGroupTable;
    }

    public function setProductTable(\AmandiaES\Table\ProductTable $productTable) {
        $this->productTable = $productTable;
    }

    public function setFactoryArticleTable(\AmandiaES\Table\FactoryArticleTable $factoryArticleTable) {
        $this->factoryArticleTable = $factoryArticleTable;
    }

    public function setSupplierTable(\AmandiaES\Table\SupplierTable $supplierTable) {
        $this->supplierTable = $supplierTable;
    }

}
