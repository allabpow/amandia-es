<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Db\Sql\Sql;
use Zend\Crypt\Password\Bcrypt;

/**
 * 
 */
class AdminFactoryOrderController extends AbstractActionController {

    /**
     *
     * @var \AmandiaES\Table\FactoryOrderTable
     */
    private $factoryOrderTable;

    /**
     *
     * @var \AmandiaES\Table\FactoryArticleTable
     */
    private $factoryArticleTable;

    /**
     *
     * @var \AmandiaES\Table\LocationTable
     */
    private $locationTable;

    /**
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction() {
        $logger = $this->serviceLocator->get('logger');
        $factoryOrders = FALSE;
        $factoryOrder = FALSE;
        $factoryArticle = FALSE;
        $factoryOrderForm = $this->serviceLocator->get('AmandiaES\Form\FactoryOrder');

        $factoryArticleId = (int) $this->params('faid');
        $factoryOrderId = (int) $this->params('foid');
        $request = $this->getRequest();

        if (!empty($factoryArticleId) && empty($factoryOrderId)) { // factory-article-ID
            $factoryArticle = $this->factoryArticleTable->getFactoryArticleById($factoryArticleId);
            if (!isset($factoryArticle['id'])) { // factory article does not exist
//                $logger->log(\Zend\Log\Logger::DEBUG, 'factory article does not exist');
                return $this->redirect()->toRoute('amandia_es_admin_order_search');
            }
            $factoryOrders = $this->factoryOrderTable->getFactoryOrdersForFactoryArticle($factoryArticle['id']);
        } elseif (!empty($factoryOrderId) && empty($factoryArticleId)) { // factoryOrder-ID
            $factoryOrder = $this->factoryOrderTable->getFactoryOrderById($factoryOrderId);
            if (!$factoryOrder) {
                return $this->redirect()->toRoute('amandia_es_admin_factoryorder_search');
            }
            $factoryArticle = $this->factoryArticleTable->getFactoryArticleById($factoryOrder['factory_article_id']);

            if ($request->isPost()) {
                $postData = $request->getPost()->toArray();
                $factoryOrderForm->setData($postData);
                if ($factoryOrderForm->isValid()) {
                    $formData = $factoryOrderForm->getData();
                    $result = $this->factoryOrderTable->updateFactoryOrder($formData);
                    if($result) {
                        $this->layout()->message = array(
                            'level' => 'info',
                            'text' => 'erfolgreich gespeichert'
                        );
                    }
                } else { // form invalid
                    $this->layout()->message = array(
                        'level' => 'warn',
                        'text' => 'Form is NICHT valide'
                    );
                }
            } else {
                $factoryOrderForm->setData(array('factory_order' => $factoryOrder));
            }
        }

        return new ViewModel(array(
            'factoryOrders' => $factoryOrders,
            'factoryOrder' => $factoryOrder,
            'factoryArticle' => $factoryArticle,
            'factoryOrderForm' => $factoryOrderForm,
        ));
    }

    /**
     * 
     */
    public function searchAction() {
        $logger = $this->serviceLocator->get('logger');
        $config = $this->serviceLocator->get('config');
        $aesConfig = $config['amandia_es'];
        $paginator = NULL;
        $page = 0;
        $factoryOrdersCount = 0;
        $orderColumn = 'factory_article_id factory_order_nr';

        $searchFactoryOrderForm = $this->serviceLocator->get('AmandiaES\Form\SearchFactoryOrder');

        $getParams = $this->params()->fromQuery();
        if ($getParams) {
            $searchFactoryOrderForm->setData($getParams);
            if ($searchFactoryOrderForm->isValid()) {
                // Pagination
                $page = $this->params()->fromRoute('page');
                $selectFactoryOrder = $this->factoryOrderTable->searchFactoryOrder($logger, $searchFactoryOrderForm->getData(), $orderColumn, TRUE);
                $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\DbSelect($selectFactoryOrder, $this->factoryOrderTable->getSql()));
                $paginator->setCurrentPageNumber($page);
                $paginator->setItemCountPerPage($aesConfig['paginationCount']);
                $factoryOrdersCount = $paginator->getCurrentItemCount();
            }
        }

        return new ViewModel(array(
            'searchFactoryOrderForm' => $searchFactoryOrderForm,
            'paginator' => $paginator,
            'page' => $page,
            'isFactoryOrders' => $factoryOrdersCount,
            'paramsSearch' => $getParams,
        ));
    }

    public function setFactoryOrderTable(\AmandiaES\Table\FactoryOrderTable $factoryOrderTable) {
        $this->factoryOrderTable = $factoryOrderTable;
    }
    
    public function setFactoryArticleTable(\AmandiaES\Table\FactoryArticleTable $factoryArticleTable) {
        $this->factoryArticleTable = $factoryArticleTable;
    }

    public function setLocationTable(\AmandiaES\Table\LocationTable $locationTable) {
        $this->locationTable = $locationTable;
    }

}
