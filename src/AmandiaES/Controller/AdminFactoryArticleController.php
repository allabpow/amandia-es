<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * 
 */
class AdminFactoryArticleController extends AbstractActionController {

    /**
     *
     * @var \Zend\Log\Logger
     */
    private $logger;
    
    /**
     *
     * @var \AmandiaES\Table\FactoryArticleTable
     */
    private $factoryArticleTable;

    /**
     *
     * @var \AmandiaES\Table\FactoryArticleGroupTable
     */
    private $factoryArticleGroupTable;

    /**
     *
     * @var \AmandiaES\Table\FactoryOrderTable
     */
    private $factoryOrderTable;

    /**
     *
     * @var \AmandiaES\Table\ProductTable 
     */
    private $productTable;

    /**
     *
     * @var \AmandiaES\Table\LocationTable
     */
    private $locationTable;

    /**
     *
     * @var \AmandiaES\Form\FactoryArticleForm
     */
    private $factoryArticleForm;

    /**
     *
     * @var \AmandiaES\Table\ArticleTable 
     */
    private $articleTable;

    /**
     *
     * @var \AmandiaES\Table\MaterialTable 
     */
    private $materialTable;

    /**
     *
     * @var \AmandiaES\Table\ServiceTable 
     */
    private $serviceTable;

    /**
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction() {
        $logger = $this->serviceLocator->get('logger');
        $factoryArticle = FALSE;
        $factoryOrders = FALSE;
        $this->factoryArticleForm = $this->serviceLocator->get('AmandiaES\Form\FactoryArticle');

        $factoryArticleId = (int) $this->params('id');

        $articlesKeyVal = array();
        $articles = array();
        $materialKeyVal = array();
        $materials = array();
        $serviceKeyVal = array();
        $services = array();
        if (!empty($factoryArticleId)) { // factory-article-ID
            $factoryArticle = $this->factoryArticleTable->getFactoryArticleById($factoryArticleId);
            if (!$factoryArticle) { // factory article does not exist
                return $this->redirect()->toRoute('amandia_es_admin_factoryarticle_search');
            }
            $this->factoryArticleForm->setData(array('factory_article' => $factoryArticle));
            $factoryOrders = $this->factoryOrderTable->getFactoryOrdersForFactoryArticle($factoryArticle['id']);
            // articles
            $articlesKeyVal = json_decode($factoryArticle['articles'], TRUE);
            if ($articlesKeyVal) {
                $articles = $this->articleTable->getArticlesByIds(array_keys($articlesKeyVal));
            }
            // materials
            $materialKeyVal = json_decode($factoryArticle['materials'], TRUE);
            if ($materialKeyVal) {
                $materials = $this->materialTable->getMaterialsByIds(array_keys($materialKeyVal));
            }
            // services
            $serviceKeyVal = json_decode($factoryArticle['services'], TRUE);
            if ($serviceKeyVal) {
                $services = $this->serviceTable->getServicesByIds(array_keys($serviceKeyVal));
            }
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $this->factoryArticleForm->setData($postData);
            if ($this->factoryArticleForm->isValid()) {
                $result = $this->factoryArticleTable->saveFactoryArticle($postData['factory_article']);
                if (is_int($result)) {
                    if (isset($postData['choose_article'])) { // Artikel choose
                        return $this->redirect()->toRoute('amandia_es_admin_article_choose', array('origin' => 'facart-' . $result));
                    }
                    if (isset($postData['choose_material'])) { // Material choose
                        return $this->redirect()->toRoute('amandia_es_admin_material_choose', array('origin' => 'facart-' . $result));
                    }
                    if (isset($postData['choose_service'])) { // Service choose
                        return $this->redirect()->toRoute('amandia_es_admin_service_choose', array('origin' => 'facart-' . $result));
                    }
                    $this->layout()->message = array(
                        'level' => 'info',
                        'text' => 'erfolgreich gespeichert - id=' . $result
                    );
                } else {
                    $this->layout()->message = array(
                        'level' => 'warn',
                        'text' => 'Fehler beim Speichern'
                    );
                }
            }
        }

        return new ViewModel(array(
            'factoryArticeForm' => $this->factoryArticleForm,
            'factoryArticle' => $factoryArticle,
            'factoryOrders' => $factoryOrders,
            'articleKeyVal' => $articlesKeyVal,
            'articles' => $articles,
            'materialKeyVal' => $materialKeyVal,
            'materials' => $materials,
            'serviceKeyVal' => $serviceKeyVal,
            'services' => $services,
        ));
    }

    /**
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function chooseAction() {
        $originString = $this->params()->fromRoute('origin');
//        $this->logger->log(\Zend\Log\Logger::NOTICE, ' ' . $originString);
        if (empty($originString)) {
            return;
        }
        $origins = explode('-', $originString);
        if (!is_array($origins) || (count($origins) != 2)) {
            $this->logger->log(\Zend\Log\Logger::NOTICE, 'Bad call of chooseAction(crash) in ' . __CLASS__ . '; ' . print_r($origins,true));
            return;
        }
        $originType = htmlspecialchars(trim($origins[0]));
        $originId = (int)$origins[1];
        $config = $this->serviceLocator->get('config');
        $aesConfig = $config['amandia_es'];
        if (!in_array($originType, array_keys($aesConfig['origintypes']))) {
            $this->logger->log(\Zend\Log\Logger::NOTICE, 'Bad call of chooseAction(origintype) in ' . __CLASS__);
            return;
        }
        $chooser = NULL;
        $chooserTypeName = '';
        if ($originType == 'product') {
            $chooser = $this->productTable->getProductById($originId);
            $chooserTypeName = 'Produkt';
        } elseif ($originType == 'facart') {
            $chooser = $this->factoryArticleTable->getFactoryArticleById($originId);
            $chooserTypeName = 'B-Artikel';
        }
        $originRoute = $aesConfig['origintypes'][$originType];
        
        
        


        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
        }

        $page = $this->params()->fromRoute('page');

        $group = 1;
        $groupParam = $this->params()->fromRoute('group');
        if ($groupParam) {
            $group = $groupParam;
        }
        if (isset($postData) && isset($postData['group_select'])) {
            $group = (int) $postData['group_select'];
            return $this->redirect()->toRoute('amandia_es_admin_factoryarticle_choose', array('origin' => $originString, 'group' => $group, 'page' => $page));
        }

        $paramsGet = $this->params()->fromQuery();

        // Choosing
        if (isset($paramsGet['chooseAdd'])) {
            $chooseId = (int) $paramsGet['chooseAdd'];
            if ($this->productTable->updateFactoryArticles($originId, $chooseId, 'plus')) {
                return $this->redirect()->refresh();
            }
        }
        if (isset($paramsGet['chooseCull'])) {
            $chooseId = (int) $paramsGet['chooseCull'];
            if ($this->productTable->updateFactoryArticles($originId, $chooseId, 'minus')) {
                return $this->redirect()->refresh();
            }
        }

        // Pagination
        $selectFactoryArticles = $this->factoryArticleTable->getSelectFactoryArticlesAll($group, 'id');
        $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\DbSelect($selectFactoryArticles, $this->factoryArticleTable->getSql()));
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($aesConfig['paginationCount']); // muss noch aus einer Configuration geholt werden
        $faCount = $paginator->getCurrentItemCount();

        $chooserFactoryArticles = json_decode($chooser['factory_articles'], TRUE);

        return new ViewModel(array(
            'faCount' => $faCount,
            'paginator' => $paginator,
            'page' => $page,
            'group' => $group,
            'groupsIdNameAssoc' => $this->factoryArticleGroupTable->getFactoryArticleGroupsIdNameAssocc(),
            'origin' => $originString, // wird pagination-route-parameter
            'originId' => $originId,
            'originRoute' => $originRoute,
            'chooserTypeName' => $chooserTypeName,
            'chooserName' => $chooser['name'],
            'chooserFactoryArticles' => ($chooserFactoryArticles ? $chooserFactoryArticles : array()),
        ));
    }

    /**
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function searchAction() {
        $logger = $this->serviceLocator->get('logger');
        $config = $this->serviceLocator->get('config');
        $aesConfig = $config['amandia_es'];
        $paginator = NULL;
        $page = 0;
        $factoryArticlesCount = 0;
        $orderColumn = 'factory_article_nr';

        $searchFactoryArticleForm = $this->serviceLocator->get('AmandiaES\Form\SearchFactoryArticle');

        $getParams = $this->params()->fromQuery();
        if ($getParams) {
            $searchFactoryArticleForm->setData($getParams);
            if ($searchFactoryArticleForm->isValid()) {
                // Pagination
                $page = $this->params()->fromRoute('page');
                $formData = $searchFactoryArticleForm->getData();
                $selectFactoryArticle = $this->factoryArticleTable->searchFactoryArticle($logger, $formData, $orderColumn, TRUE);
                $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\DbSelect($selectFactoryArticle, $this->factoryArticleTable->getSql()));
                $paginator->setCurrentPageNumber($page);
                $paginator->setItemCountPerPage($aesConfig['paginationCount']);
                $factoryArticlesCount = $paginator->getCurrentItemCount();
            }
        }

        return new ViewModel(array(
            'searchFactoryArticleForm' => $searchFactoryArticleForm,
            'paginator' => $paginator,
            'page' => $page,
            'factoryArticlesCount' => $factoryArticlesCount,
            'paramsSearch' => $getParams,
        ));
    }
    
    public function setLogger(\Zend\Log\Logger $logger) {
        $this->logger = $logger;
    }

    public function setFactoryArticleTable(\AmandiaES\Table\FactoryArticleTable $factoryArticleTable) {
        $this->factoryArticleTable = $factoryArticleTable;
    }

    public function setFactoryArticleGroupTable(\AmandiaES\Table\FactoryArticleGroupTable $factoryArticleGroupTable) {
        $this->factoryArticleGroupTable = $factoryArticleGroupTable;
    }

    public function setFactoryOrderTable(\AmandiaES\Table\FactoryOrderTable $factoryOrderTable) {
        $this->factoryOrderTable = $factoryOrderTable;
    }

    public function setProductTable(\AmandiaES\Table\ProductTable $productTable) {
        $this->productTable = $productTable;
    }

    public function setLocationTable(\AmandiaES\Table\LocationTable $locationTable) {
        $this->locationTable = $locationTable;
    }

    public function setArticleTable(\AmandiaES\Table\ArticleTable $articleTable) {
        $this->articleTable = $articleTable;
    }

    public function setMaterialTable(\AmandiaES\Table\MaterialTable $materialTable) {
        $this->materialTable = $materialTable;
    }

    public function setServiceTable(\AmandiaES\Table\ServiceTable $serviceTable) {
        $this->serviceTable = $serviceTable;
    }

}
