<?php
/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Controller\Ajax;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * erstmal unused 2014-02-21
 * ...weil so'n JavaScript Modal Fenster mit AJAX Aufrufen & Aktualisierung problematisch ist
 * http://www.scriptiny.com/2011/03/javascript-modal-windows/
 * http://nyromodal.nyrodev.com/
 * und den vielleicht noch
 * http://kylefox.ca/jquery-modal/examples/
 * 
 */
class ModalProductController extends AbstractActionController {

    /**
     *
     * @var \BitkornUser\Service\AccessService
     */
    private $accessService;
    
    /**
     *
     * @var \AmandiaES\Form\SearchProductForm 
     */
    private $searchProductForm;
    
    /**
     *
     * @var \AmandiaES\Table\ProductTable
     */
    private $productTable;

    /**
     *
     * @var \AmandiaES\Table\ProductGroupTable
     */
    private $productGroupTable;

    /**
     *
     * @var \AmandiaES\Table\StockProductTable
     */
    private $stockProductTable;

    /**
     *
     * @var \AmandiaES\Table\SupplierTable 
     */
    private $supplierTable;

    /**
     * 
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function chooseProductAction() {
        $logger = $this->serviceLocator->get('logger');
        $postParams = $this->params()->fromPost();
        $getParams = $this->params()->fromQuery();
        $order = 'name';
        $page = 1;
        $limitCount = 3;
        $products = NULL;
        $productCount = 0;
        
//        $logger->log(\Zend\Log\Logger::DEBUG, print_r($getParams, true));
        
        $this->searchProductForm = $this->serviceLocator->get('AmandiaES\Form\SearchProduct');
        $this->searchProductForm->setAttribute('action', '/aes-ajax-modal-product'); // muss per AJAX abgesendet werden
        
        if(isset($getParams['product_search']['page'])) {
            $page = intval($getParams['product_search']['page']);
        }
        if(isset($getParams['product_search']['limitCount'])) {
            $limitCount = intval($getParams['product_search']['limitCount']);
        }
        if(isset($getParams['product_search']['order'])) {
            $order = intval($getParams['product_search']['order']);
        }
        
        if ($getParams) {
            $this->searchProductForm->setData($getParams);
            if ($this->searchProductForm->isValid()) {
                $products = $this->productTable->searchProduct($logger, $this->searchProductForm->getData(), $order, FALSE, $limitCount, $page);
                $productCount = $this->productTable->searchProduct($logger, $this->searchProductForm->getData(), $order, FALSE, 0, 0, TRUE);
            }
        } else {
//            $logger->log(\Zend\Log\Logger::DEBUG, 'auch ohne params');
            $products = $this->productTable->searchProduct($logger, array('product_search' => array()), $order, FALSE, $limitCount, $page);
            $productCount = $this->productTable->searchProduct($logger, array('product_search' => array()), $order, FALSE, 0, 0, TRUE);
        }
        
        // LayoutSwitch
        $layout = $this->layout();
        $layout->setTemplate('layout/modal');
                
        return new ViewModel(array(
            'products' => $products,
            'productCount' => $productCount,
            'pageCount' => ceil($productCount / $limitCount),
            'limitCount' => $limitCount,
            'searchProductForm' => $this->searchProductForm,
        ));
    }
    
    public function setAccessService(\BitkornUser\Service\AccessService $accessService) {
        $this->accessService = $accessService;
    }
 
    public function setProductTable(\AmandiaES\Table\ProductTable $productTable) {
        $this->productTable = $productTable;
    }

    public function setProductGroupTable(\AmandiaES\Table\ProductGroupTable $productGroupTable) {
        $this->productGroupTable = $productGroupTable;
    }

    public function setStockProductTable(\AmandiaES\Table\StockProductTable $stockProductTable) {
        $this->stockProductTable = $stockProductTable;
    }

    public function setSupplierTable(\AmandiaES\Table\SupplierTable $supplierTable) {
        $this->supplierTable = $supplierTable;
    }

}

