<?php

namespace AmandiaES\Controller\Plugin;

use Zend\Session\Container;

/**
 * RefererWithoutEndingSlash
 *
 * @author allapow
 */
class RouteNameWithoutEndingSlash extends \Zend\Mvc\Controller\Plugin\AbstractPlugin {

    /**
     * 
     */
    public function __invoke(\Zend\Mvc\Controller\AbstractActionController $ctr) {
        $referer = $ctr->url()->fromRoute($ctr->getEvent()->getRouteMatch()->getMatchedRouteName());
        $strPos = strrpos($referer, '/');
        if ($strPos == (strlen($referer) - 1)) {
            return substr($referer, 0, strlen($referer) - 1);
        }
        return $referer;
    }

}
