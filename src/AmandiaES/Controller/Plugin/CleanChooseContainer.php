<?php

namespace AmandiaES\Controller\Plugin;

use Zend\Session\Container;

/**
 * CleanChooseContainer
 *
 * @author allapow
 */
class CleanChooseContainer extends \Zend\Mvc\Controller\Plugin\AbstractPlugin {

    /**
     * @param string $noClean Kind of Choose-Container wich will not be clean
     */
    public function __invoke($noClean = '') {
        if($noClean != 'products') {
            $containerChooseProducts = new Container('chooseProducts');
            $containerChooseProducts->data = NULL;
        }
        if($noClean != 'materials') {
            $containerChooseMaterial = new Container('chooseMaterial');
            $containerChooseMaterial->data = NULL;
        }
        if($noClean != 'articles') {
            $containerChooseArticles = new Container('chooseArticles');
            $containerChooseArticles->data = NULL;
        }
    }

}
