<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

/**
 * 
 */
class AdminServiceController extends AbstractActionController {


    /**
     *
     * @var \Zend\Log\Logger
     */
    private $logger;

    /**
     *
     * @var \AmandiaES\Table\ServiceTable 
     */
    private $serviceTable;

    /**
     *
     * @var \AmandiaES\Table\ServiceGroupTable 
     */
    private $serviceGroupTable;

    /**
     *
     * @var \AmandiaES\Table\ProductTable 
     */
    private $productTable;

    /**
     *
     * @var \AmandiaES\Table\FactoryArticleTable 
     */
    private $factoryArticleTable;
    
    /**
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction() {
        $this->logger = $this->serviceLocator->get('logger');
        $containerUser = new Container('user');
        if (!isset($containerUser->groups) || !in_array('2', $containerUser->groups)) {
            $this->layout()->message = array(
                'level' => 'warn',
                'text' => 'du bist kein Manager'
            );
            return new ViewModel(array());
        }
        $config = $this->serviceLocator->get('config');
        $aesConfig = $config['amandia_es'];

        $id = $this->params('id');
        $serviceForm = $this->serviceLocator->get('AmandiaES\Form\Service');

        if (isset($id)) {
            $service = $this->serviceTable->getServiceById($id);
            if ($service) {
                $serviceForm->setData(array('service' => $service));
            } else {
                uset($id);
            }
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
            $serviceForm->setData($postData);
            if ($serviceForm->isValid()) {
                $serviceId = $this->serviceTable->saveService($postData['service']);
                if (is_int($serviceId)) {
                    $this->layout()->message = array(
                        'level' => 'info',
                        'text' => 'erfolgreich gespeichert - id=' . $serviceId
                    );
                } else { // database error
                    $this->layout()->message = array(
                        'level' => 'error',
                        'text' => 'nicht gespeichert - bitte den Administrator benachrichtigen'
                    );
                }
            }
        }

        return new ViewModel(array(
            'serviceForm' => $serviceForm,
        ));
    }

    /**
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function chooseAction() {
        $originString = $this->params()->fromRoute('origin');
        $origins = explode('-', $originString);
        if (!is_array($origins) || (count($origins) != 2)) {
            $this->logger->log(\Zend\Log\Logger::NOTICE, 'Bad call of chooseAction(crash) in ' . __CLASS__ . '; ' . print_r($origins,true));
            return;
        }
        $originType = htmlspecialchars(trim($origins[0]));
        $originId = (int)$origins[1];
        $config = $this->serviceLocator->get('config');
        $aesConfig = $config['amandia_es'];
        if (!in_array($originType, array_keys($aesConfig['origintypes']))) {
            $this->logger->log(\Zend\Log\Logger::NOTICE, 'Bad call of chooseAction(origintype) in ' . __CLASS__);
            return;
        }
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
        }

        $page = $this->params()->fromRoute('page');

        $group = 1;
        $groupParam = $this->params()->fromRoute('group');
        if ($groupParam) {
            $group = $groupParam;
        }
        if (isset($postData) && isset($postData['group_select'])) {
            $group = (int) $postData['group_select'];
            return $this->redirect()->toRoute('amandia_es_admin_service_choose', array('origin' => $originString, 'group' => $group, 'page' => $page));
        }

        $chooser = NULL;
        $chooserTypeName = '';
        if ($originType == 'product') {
            $chooser = $this->productTable->getProductById($origins[1]);
            $chooserTypeName = 'Produkt';
        } elseif ($originType == 'facart') {
            $chooser = $this->factoryArticleTable->getFactoryArticleById($origins[1]);
            $chooserTypeName = 'B-Artikel';
        }
        $originRoute = $aesConfig['origintypes'][$origins[0]];

        // Choosing
        if (isset($postData) && isset($postData['choose_geom'])) {
            $serviceId = (int) $postData['id'];
            $amount = floatval(str_replace(',','.',$postData['amount']));
            if ($originType == 'product') {
                if ($this->productTable->updateServices($originId, $serviceId, $amount)) {
                    return $this->redirect()->refresh();
                }
            } elseif ($originType == 'facart') {
                if ($this->factoryArticleTable->updateServices($originId, $serviceId, $amount)) {
                    return $this->redirect()->refresh();
                }
            }
        }

        // Pagination
        $selectServices = $this->serviceTable->getSelectServicesAll($group, 'id');
        $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\DbSelect($selectServices, $this->serviceTable->getSql()));
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($aesConfig['paginationCount']); // muss noch aus einer Configuration geholt werden
        $servicesCount = $paginator->getCurrentItemCount();

        $chooserServices = json_decode($chooser['services'], TRUE);

        return new ViewModel(array(
            'servicesCount' => $servicesCount,
            'paginator' => $paginator,
            'page' => $page, // wird pagination-route-parameter
            'group' => $group, // wird pagination-route-parameter
            'groupsIdNameAssoc' => $this->serviceGroupTable->getServiceGroupsIdNameAssocc(),
            'origin' => $originString, // wird pagination-route-parameter
            'originId' => $originId,
            'originRoute' => $originRoute,
            'chooserTypeName' => $chooserTypeName,
            'chooserName' => $chooser['name'],
            'chooserServices' => ($chooserServices ? $chooserServices : array()),
        ));
    }

    /**
     * 
     */
    public function searchAction() {
        $this->logger = $this->serviceLocator->get('logger');
        $config = $this->serviceLocator->get('config');
        $aesConfig = $config['amandia_es'];
        $serviceGroupsIdNameAssoc = $this->serviceGroupTable->getServiceGroupsIdNameAssocc();
        $paginator = NULL;
        $page = 0;
        $serviceCount = 0;
        $orderColumn = 'service_nr DESC';

        $searchServiceForm = $this->serviceLocator->get('AmandiaES\Form\SearchService');

        $getParams = $this->params()->fromQuery();
        if ($getParams) {
            $searchServiceForm->setData($getParams);
            if ($searchServiceForm->isValid()) {
                // Pagination
                $page = $this->params()->fromRoute('page');
                $selectService = $this->serviceTable->searchService($this->logger, $searchServiceForm->getData(), $orderColumn, TRUE);
                $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\DbSelect($selectService, $this->serviceTable->getSql()));
                $paginator->setCurrentPageNumber($page);
                $paginator->setItemCountPerPage($aesConfig['paginationCount']); // muss noch aus einer Configuration geholt werden
                $serviceCount = $paginator->getCurrentItemCount();
            }
        }

        return new ViewModel(array(
            'searchServiceForm' => $searchServiceForm,
            'serviceGroupsIdName' => $serviceGroupsIdNameAssoc,
            'paginator' => $paginator,
            'page' => $page,
            'serviceCount' => $serviceCount,
            'paramsSearch' => $getParams,
        ));
    }
    
    public function setLogger(\Zend\Log\Logger $logger) {
        $this->logger = $logger;
    }

    public function setServiceTable(\AmandiaES\Table\ServiceTable $serviceTable) {
        $this->serviceTable = $serviceTable;
    }

    public function setServiceGroupTable(\AmandiaES\Table\ServiceGroupTable $serviceGroupTable) {
        $this->serviceGroupTable = $serviceGroupTable;
    }

    public function setProductTable(\AmandiaES\Table\ProductTable $productTable) {
        $this->productTable = $productTable;
    }

    public function setFactoryArticleTable(\AmandiaES\Table\FactoryArticleTable $factoryArticleTable) {
        $this->factoryArticleTable = $factoryArticleTable;
    }


}
