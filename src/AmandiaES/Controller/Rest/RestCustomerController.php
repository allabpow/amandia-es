<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Controller\Rest;

/**
 * Description of RestCustomerController
 *
 * @author bitkorn
 */
class RestCustomerController extends \Zend\Mvc\Controller\AbstractRestfulController {
    
    /**
     *
     * @var \AmandiaES\Table\CustomerTable
     */
    private $customerTable;
    
    public function getList() {
        $logger = $this->serviceLocator->get('logger');
//        $logger->log(\Zend\Log\Logger::DEBUG, 'im REST getList()');
        $customers = $this->customerTable->getCustomers();
        
        return new \Zend\View\Model\JsonModel(array(
            'customers' => $customers,
        ));
    }
    
    public function create($data) {
        parent::create($data);
    }

    public function delete($id) {
        parent::delete($id);
    }

    public function get($id) {
        parent::get($id);
    }

    public function update($id, $data) {
        parent::update($id, $data);
    }
    
    public function setCustomerTable(\AmandiaES\Table\CustomerTable $customerTable) {
        $this->customerTable = $customerTable;
    }



    
//    public function options() {
//        $logger = $this->serviceLocator->get('logger');
//        $logger->log(\Zend\Log\Logger::DEBUG, 'im REST options()');
//        $response = $this->getResponse();
//        $headers  = $response->getHeaders();
//
//        // If you want to vary based on whether this is a collection or an
//        // individual item in that collection, check if an identifier from
//        // the route is present
//        if ($this->params()->fromRoute('id', false)) {
//            // Allow viewing, partial updating, replacement, and deletion
//            // on individual items
//            $headers->addHeaderLine('Allow', implode(',', array(
//                'GET',
//                'PATCH',
//                'PUT',
//                'DELETE',
//            )));
//            return $response;
//        }
//
//        // Allow only retrieval and creation on collections
//        $headers->addHeaderLine('Allow', implode(',', array(
//            'GET',
//            'POST',
//        )));
//        return $response;
//    }

}
