<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Controller\Rest;

/**
 * Description of RestProductArticleController
 *
 * @author bitkorn
 */
class RestProductFactoryArticleController extends \Zend\Mvc\Controller\AbstractRestfulController {

    /**
     *
     * @var \AmandiaES\Table\FactoryArticleTable
     */
    private $factoryArticleTable;

    /**
     *
     * @var \AmandiaES\Table\ProductTable 
     */
    private $productTable;

    public function create($data) {
        parent::create($data);
    }

    public function delete($id) {
        parent::delete($id);
    }

    /**
     * 
     * @param int $id ID (product-ID) -> fetch articles from this product
     */
    public function get($id) {
//        $logger = $this->serviceLocator->get('logger');
//        $logger->log(\Zend\Log\Logger::DEBUG, 'im REST getList()');
        $intId = (int) $id;
        $product = $this->productTable->getProductById($intId);
        $factoryArticleArr = json_decode($product['factory_articles'], TRUE);
        $articles = $this->factoryArticleTable->getFactoryArticlesByIds(array_keys($factoryArticleArr));
        $articleCount = count($articles);
        for ($i = 0; $i < $articleCount; $i++) {
            $articles[$i]['amount'] = $factoryArticleArr[$articles[$i]['id']];
        }
        return new \Zend\View\Model\JsonModel(array(
            'product' => array(
                'articles' => $articles,
                'title' => $product['name']
            ),
        ));
    }

    public function update($id, $data) {
        parent::update($id, $data);
    }

    public function setFactoryArticleTable(\AmandiaES\Table\FactoryArticleTable $factoryArticleTable) {
        $this->factoryArticleTable = $factoryArticleTable;
    }

    public function setProductTable(\AmandiaES\Table\ProductTable $productTable) {
        $this->productTable = $productTable;
    }

}
