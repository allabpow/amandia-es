<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Controller\Rest;

/**
 * Description of RestProductMaterialController
 *
 * @author bitkorn
 */
class RestProductMaterialController extends \Zend\Mvc\Controller\AbstractRestfulController {
    
    /**
     *
     * @var \AmandiaES\Table\MaterialTable
     */
    private $materialTable;
    
    /**
     *
     * @var \AmandiaES\Table\ProductTable 
     */
    private $productTable;
    
    private $symbolsGeom;
    private $symbolsWeight;
    
    public function create($data) {
        parent::create($data);
    }

    public function delete($id) {
        parent::delete($id);
    }

    /**
     * 
     * @param int $id ID (product-ID) -> fetch materials from this product
     */
    public function get($id) {
        $logger = $this->serviceLocator->get('logger');
//        $logger->log(\Zend\Log\Logger::DEBUG, 'im REST getList()');
        $intId = (int)$id;
        $product = $this->productTable->getProductById($intId);
        $materialArr = json_decode($product['materials'], TRUE);
        $materials = $this->materialTable->getMaterialsByIds(array_keys($materialArr));
        $materialCount = count($materials);
        for($i = 0; $i < $materialCount; $i++) {
            $materials[$i]['measure_symbol_geom'] = $this->symbolsGeom[$materials[$i]['measure_symbol_geom']];
            $materials[$i]['measure_symbol_weight'] = $this->symbolsWeight[$materials[$i]['measure_symbol_weight']];
            $materials[$i]['amount'] = $materialArr[$materials[$i]['id']];
        }
        return new \Zend\View\Model\JsonModel(array(
            'product' => array('materials' => $materials, 'prodname' => $product['name']),
        ));
    }

    public function update($id, $data) {
        parent::update($id, $data);
    }
    
    public function setMaterialTable(\AmandiaES\Table\MaterialTable $materialTable) {
        $this->materialTable = $materialTable;
    }

    public function setProductTable(\AmandiaES\Table\ProductTable $productTable) {
        $this->productTable = $productTable;
    }
    public function setSymbolsGeom($symbolsGeom) {
        $this->symbolsGeom = $symbolsGeom;
    }

    public function setSymbolsWeight($symbolsWeight) {
        $this->symbolsWeight = $symbolsWeight;
    }


}
