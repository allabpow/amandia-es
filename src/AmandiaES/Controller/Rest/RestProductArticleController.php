<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Controller\Rest;

/**
 * Description of RestProductArticleController
 *
 * @author bitkorn
 */
class RestProductArticleController extends \Zend\Mvc\Controller\AbstractRestfulController {

    /**
     *
     * @var \AmandiaES\Table\ArticleTable
     */
    private $articleTable;

    /**
     *
     * @var \AmandiaES\Table\ProductTable 
     */
    private $productTable;

    public function create($data) {
        parent::create($data);
    }

    public function delete($id) {
        parent::delete($id);
    }

    /**
     * 
     * @param int $id ID (product-ID) -> fetch articles from this product
     */
    public function get($id) {
        $logger = $this->serviceLocator->get('logger');
//        $logger->log(\Zend\Log\Logger::DEBUG, 'im REST getList()');
        $intId = (int) $id;
        $product = $this->productTable->getProductById($intId);
        $articleArr = json_decode($product['articles'], TRUE);
        $articles = $this->articleTable->getArticlesByIds(array_keys($articleArr));
        $articleCount = count($articles);
        for ($i = 0; $i < $articleCount; $i++) {
            $articles[$i]['amount'] = $articleArr[$articles[$i]['id']];
        }
        return new \Zend\View\Model\JsonModel(array(
            'product' => array(
                'articles' => $articles,
                'title' => $product['name']
            ),
        ));
    }

    public function update($id, $data) {
        parent::update($id, $data);
    }

    public function setArticleTable(\AmandiaES\Table\ArticleTable $articleTable) {
        $this->articleTable = $articleTable;
    }

    public function setProductTable(\AmandiaES\Table\ProductTable $productTable) {
        $this->productTable = $productTable;
    }

}
