<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Controller\Rest;

/**
 * Description of RestProductServiceController
 *
 * @author bitkorn
 */
class RestProductServiceController extends \Zend\Mvc\Controller\AbstractRestfulController {

    /**
     *
     * @var \AmandiaES\Table\ServiceTable
     */
    private $serviceTable;

    /**
     *
     * @var \AmandiaES\Table\ProductTable 
     */
    private $productTable;

    public function create($data) {
        parent::create($data);
    }

    public function delete($id) {
        parent::delete($id);
    }

    /**
     * 
     * @param int $id ID (product-ID) -> fetch services from this product
     */
    public function get($id) {
//        $logger = $this->serviceLocator->get('logger');
//        $logger->log(\Zend\Log\Logger::DEBUG, 'im REST getList()');
        $intId = (int) $id;
        $product = $this->productTable->getProductById($intId);
        $serviceArr = json_decode($product['services'], TRUE);
        $services = $this->serviceTable->getServicesByIds(array_keys($serviceArr));
        $serviceCount = count($services);
        for ($i = 0; $i < $serviceCount; $i++) {
            $services[$i]['amount'] = $serviceArr[$services[$i]['id']];
        }
        return new \Zend\View\Model\JsonModel(array(
            'product' => array(
                'services' => $services,
                'title' => $product['name']
            ),
        ));
    }

    public function update($id, $data) {
        parent::update($id, $data);
    }
    
    public function setServiceTable(\AmandiaES\Table\ServiceTable $serviceTable) {
        $this->serviceTable = $serviceTable;
    }

    public function setProductTable(\AmandiaES\Table\ProductTable $productTable) {
        $this->productTable = $productTable;
    }

}
