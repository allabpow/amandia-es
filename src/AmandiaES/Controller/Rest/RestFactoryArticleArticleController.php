<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Controller\Rest;

/**
 * Description of RestFactoryArticleArticleController
 *
 * @author bitkorn
 */
class RestFactoryArticleArticleController extends \Zend\Mvc\Controller\AbstractRestfulController {

    /**
     *
     * @var \AmandiaES\Table\FactoryArticleTable
     */
    private $factoryArticleTable;

    /**
     *
     * @var \AmandiaES\Table\ArticleTable
     */
    private $articleTable;

    public function create($data) {
        parent::create($data);
    }

    public function delete($id) {
        parent::delete($id);
    }

    /**
     * 
     * @param int $id ID (factory-article-ID) -> fetch articles from this factory-article
     */
    public function get($id) {
        $logger = $this->serviceLocator->get('logger');
//        $logger->log(\Zend\Log\Logger::DEBUG, 'im REST getList()');
        $intId = (int) $id;
        $factoryArticle = $this->factoryArticleTable->getFactoryArticleById($intId);
        $articleArr = json_decode($factoryArticle['articles'], TRUE);
        $articles = $this->articleTable->getArticlesByIds(array_keys($articleArr));
        $articleCount = count($articles);
        for ($i = 0; $i < $articleCount; $i++) {
            $articles[$i]['amount'] = $articleArr[$articles[$i]['id']];
        }
        return new \Zend\View\Model\JsonModel(array(
            'articles' => array(
                'articles' => $articles,
                'title' => $factoryArticle['name']
            ),
        ));
    }

    public function update($id, $data) {
        parent::update($id, $data);
    }
    
    public function setFactoryArticleTable(\AmandiaES\Table\FactoryArticleTable $factoryArticleTable) {
        $this->factoryArticleTable = $factoryArticleTable;
    }

    public function setArticleTable(\AmandiaES\Table\ArticleTable $articleTable) {
        $this->articleTable = $articleTable;
    }

}
