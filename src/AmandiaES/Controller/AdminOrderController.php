<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

/**
 * 
 */
class AdminOrderController extends AbstractActionController {

    /**
     *
     * @var \AmandiaES\Form\SearchOrderForm 
     */
    private $searchOrderForm;

    /**
     *
     * @var \AmandiaES\Table\OrderTable 
     */
    private $orderTable;

    /**
     *
     * @var \AmandiaES\Table\FactoryOrderTable 
     */
    private $factoryOrderTable;

    /**
     *
     * @var \AmandiaES\Table\ProductTable 
     */
    private $productTable;

    /**
     *
     * @var \AmandiaES\Table\CostCenterTable 
     */
    private $costCenterTable;

    function __construct() {
        
    }

    /**
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction() {
        $containerUser = new Container('user');
        if (!isset($containerUser->groups) || !in_array('5', $containerUser->groups)) {
            $this->layout()->message = array(
                'level' => 'warn',
                'text' => 'du bist nicht in der Gruppe Aufträge'
            );
            return new ViewModel(array(
            ));
        }
        $logger = $this->serviceLocator->get('logger');
        $config = $this->serviceLocator->get('config');
//        $folderService = new \AmandiaES\Service\FolderService($logger, $config['amandia_es']['document_root'], time(), 'kd-12xy34', 'order', 'ord-31345');
//        $this->layout()->message = array(
//            'level' => 'warn',
//            'text' => 'foldername: ' . $folderService->computeFolder(),
//        );
        
        $id = (int) $this->params('id');
        $orderForm = $this->serviceLocator->get('AmandiaES\Form\Order');

        $order = NULL;
        $productsKeyVal = array();
        $products = array();
        if (isset($id)) {
            $order = $this->orderTable->getOrderById($id);
            if ($order) {
                $orderForm->setData(array('order' => $order));
                $productsKeyVal = json_decode($order['products'], TRUE);
                if($productsKeyVal) {
                    $products = $this->productTable->getProductsByIds(array_keys($productsKeyVal));
                }
            } else {
                unset($id);
            }
        } else { // employee_id ...wenns ne neue Order wird
            $formUserCreate = array('order' => array('user_create' => $containerUser->userid)); // set current employee
            $orderForm->setData($formUserCreate);
        }
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
            $orderForm->setData($postData);
            if ($orderForm->isValid()) {
                $formData = $orderForm->getData();
//                $this->layout()->data = $formData; // DEBUG
                $orderId = $this->orderTable->saveOrder($formData['order']);
                if (is_int($orderId)) {
                    $postData['order']['id'] = $orderId;
                    if (isset($postData['choose_products'])) { // product choose
                        return $this->redirect()->toRoute('amandia_es_admin_product_choose', array('origin' => $orderId));
                    }
                    if (isset($postData['choose_customer'])) { // Customer choose
                        return $this->redirect()->toRoute('amandia_es_admin_customer_choose', array('origintype' => 'order', 'origin' => $orderId));
                    }
                    $orderForm->setData($postData);
                    $this->layout()->message = array(
                        'level' => 'info',
                        'text' => 'erfolgreich gespeichert - id=' . $orderId
                    );
                } else { // database error
                    $this->layout()->message = array(
                        'level' => 'error',
                        'text' => 'nicht gespeichert - bitte den Administrator benachrichtigen'
                    );
                }
            } else { // form invalid
                $this->layout()->message = array(
                    'level' => 'warn',
                    'text' => 'nicht alle eingegebenen Daten sind valide'
                );
            }
        }
        return new ViewModel(array(
            'order' => $order,
            'userLastEditor' => $containerUser->userid,
            'orderForm' => $orderForm,
            'productKeyVal' => $productsKeyVal,
            'products' => $products,
        ));
    }

    /**
     * 
     */
    public function searchAction() {
        $logger = $this->serviceLocator->get('logger');
        $config = $this->serviceLocator->get('config');
        $aesConfig = $config['amandia_es'];
        $costCenterIdNameAssoc = $this->costCenterTable->getCostCentersIdNameAssocc();
//        $result = array();
        $paginator = NULL;
        $page = 0;
        $ordersCount = 0;
        $orderColumn = 'id';

        $this->searchOrderForm = $this->serviceLocator->get('AmandiaES\Form\SearchOrder');

        $getParams = $this->params()->fromQuery();
        if ($getParams) {
            $this->searchOrderForm->setData($getParams);
            if ($this->searchOrderForm->isValid()) {
//                $result = $this->orderTable->searchOrder($logger, $this->searchOrderForm->getData());
                // Pagination
                $page = $this->params()->fromRoute('page');
                $selectOrders = $this->orderTable->searchOrder($logger, $this->searchOrderForm->getData(), $orderColumn, TRUE);
                $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\DbSelect($selectOrders, $this->orderTable->getSql()));
                $paginator->setCurrentPageNumber($page);
                $paginator->setItemCountPerPage($aesConfig['paginationCount']); // muss noch aus einer Configuration geholt werden
                $ordersCount = $paginator->getCurrentItemCount();
            }
        }

        return new ViewModel(array(
            'searchOrderForm' => $this->searchOrderForm,
            'costCenterIdName' => $costCenterIdNameAssoc,
            'paginator' => $paginator,
            'page' => $page,
            'isOrders' => $ordersCount,
            'paramsSearch' => $getParams,
//            'searchResult' => $result,
        ));
    }

    public function setOrderTable(\AmandiaES\Table\OrderTable $orderTable) {
        $this->orderTable = $orderTable;
    }
    
    public function setFactoryOrderTable(\AmandiaES\Table\FactoryOrderTable $factoryOrderTable) {
        $this->factoryOrderTable = $factoryOrderTable;
    }

    public function setProductTable(\AmandiaES\Table\ProductTable $productTable) {
        $this->productTable = $productTable;
    }

    public function setCostCenterTable(\AmandiaES\Table\CostCenterTable $costCenterTable) {
        $this->costCenterTable = $costCenterTable;
    }

}
