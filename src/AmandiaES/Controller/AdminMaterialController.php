<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

/**
 * 
 */
class AdminMaterialController extends AbstractActionController {

    /**
     *
     * @var \AmandiaES\Form\SearchMaterialForm 
     */
    private $searchMaterialForm;

    /**
     *
     * @var \AmandiaES\Table\MaterialTable 
     */
    private $materialTable;

    /**
     *
     * @var \AmandiaES\Table\MaterialGroupTable 
     */
    private $materialGroupTable;

    /**
     *
     * @var \AmandiaES\Table\ProductTable 
     */
    private $productTable;

    /**
     *
     * @var \AmandiaES\Table\FactoryArticleTable 
     */
    private $factoryArticleTable;

    /**
     *
     * @var \AmandiaES\Table\SupplierTable 
     */
    private $supplierTable;

    public function indexAction() {
        $logger = $this->serviceLocator->get('logger');
        $containerUser = new Container('user');
        if (!isset($containerUser->groups) || !in_array('2', $containerUser->groups)) {
            return new ViewModel(array(
                'message' => array(
                    'level' => 'warn',
                    'text' => 'du bist kein Manager'
                ),
            ));
        }

        $id = $this->params('id');
        $materialForm = $this->serviceLocator->get('AmandiaES\Form\Material');

        if (isset($id)) {
            $article = $this->materialTable->getMaterialById($id);
            if ($article) {
                $materialForm->setData($article);
            } else {
                unset($id);
            }
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
            $materialForm->setData($postData);
            if ($materialForm->isValid()) {
                $materialId = $this->materialTable->saveMaterial($postData);
                if (is_int($materialId)) {
                    $this->layout()->message = array(
                        'level' => 'info',
                        'text' => 'erfolgreich gespeichert - id=' . $materialId
                    );
                } else { // database error
                    $this->layout()->message = array(
                        'level' => 'error',
                        'text' => 'nicht gespeichert - bitte den Administrator benachrichtigen'
                    );
                }
            }
        }

        return new ViewModel(array(
            'materialForm' => $materialForm,
        ));
    }

    /**
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function chooseAction() {
        $this->logger = $this->serviceLocator->get('logger');
        $originString = $this->params()->fromRoute('origin');
        $origins = explode('-', $originString);
        if (!is_array($origins) || (count($origins) != 2)) {
            $this->logger->log(\Zend\Log\Logger::NOTICE, 'Bad call of chooseAction(crash) in ' . __CLASS__ . '; ' . print_r($origins,true));
            return;
        }
        $originType = htmlspecialchars(trim($origins[0]));
        $originId = (int)$origins[1];
        $config = $this->serviceLocator->get('config');
        $aesConfig = $config['amandia_es'];
        if (!in_array($originType, array_keys($aesConfig['origintypes']))) {
            $this->logger->log(\Zend\Log\Logger::NOTICE, 'Bad call of chooseAction(origintype) in ' . __CLASS__);
            return;
        }
        $chooser = NULL;
        $chooserTypeName = '';
        if ($originType == 'product') {
            $chooser = $this->productTable->getProductById($origins[1]);
            $chooserTypeName = 'Produkt';
        } elseif ($originType == 'facart') {
            $chooser = $this->factoryArticleTable->getFactoryArticleById($origins[1]);
            $chooserTypeName = 'B-Artikel';
        }
        $originRoute = $aesConfig['origintypes'][$origins[0]];


        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
        }

        $page = $this->params()->fromRoute('page');

        $group = 1;
        $groupParam = $this->params()->fromRoute('group');
        if ($groupParam) {
            $group = $groupParam;
        }
        if (isset($postData) && isset($postData['group_select'])) {
            $group = (int) $postData['group_select'];
            return $this->redirect()->toRoute('amandia_es_admin_material_choose', array('origin' => $originString, 'group' => $group, 'page' => $page));
        }

        // Choosing
        if (isset($postData) && isset($postData['choose_geom'])) {
            $materialId = (int) $postData['id'];
            $amount = floatval(str_replace(',', '.',$postData['amount']));
            if ($originType == 'product') {
                if ($this->productTable->updateMaterials($originId, $materialId, $amount)) {
                    return $this->redirect()->refresh();
                }
            } elseif ($originType == 'facart') {
                if ($this->factoryArticleTable->updateMaterials($originId, $materialId, $amount)) {
                    return $this->redirect()->refresh();
                }
            }
        }

        // Pagination
        $selectMaterials = $this->materialTable->getSelectMaterialsAll($group, 'id');
        $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\DbSelect($selectMaterials, $this->materialTable->getSql()));
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($aesConfig['paginationCount']); // muss noch aus einer Configuration geholt werden
        $materialsCount = $paginator->getCurrentItemCount();

        $chooserMaterials = json_decode($chooser['materials'], TRUE);

        return new ViewModel(array(
            'isMaterials' => ($materialsCount ? TRUE : FALSE),
            'paginator' => $paginator,
            'page' => $page,
            'group' => $group,
            'groupsIdNameAssoc' => $this->materialGroupTable->getMaterialGroupsIdNameAssocc(),
            'origin' => $originString, // wird pagination-route-parameter
            'originId' => $originId,
            'originRoute' => $originRoute,
            'chooserTypeName' => $chooserTypeName,
            'chooserName' => $chooser['name'],
            'chooserMaterials' => ($chooserMaterials ? $chooserMaterials : array()),
        ));
    }

    /**
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function searchAction() {
        $logger = $this->serviceLocator->get('logger');
        $config = $this->serviceLocator->get('config');
        $aesConfig = $config['amandia_es'];
        $materialGroupsIdNameAssoc = $this->materialGroupTable->getMaterialGroupsIdNameAssocc();
        $suppliersIdNameAssoc = $this->supplierTable->getSuppliersIdNameAssocc();
        $paginator = NULL;
        $page = 0;
        $materialCount = 0;
        $orderColumn = 'name DESC';

        $this->searchMaterialForm = $this->serviceLocator->get('AmandiaES\Form\SearchMaterial');

        $getParams = $this->params()->fromQuery();
        if ($getParams) {
            $this->searchMaterialForm->setData($getParams);
            if ($this->searchMaterialForm->isValid()) {
                // Pagination
                $page = $this->params()->fromRoute('page');
                $selectMaterial = $this->materialTable->searchMaterial($logger, $this->searchMaterialForm->getData(), $orderColumn, TRUE);
                $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\DbSelect($selectMaterial, $this->materialTable->getSql()));
                $paginator->setCurrentPageNumber($page);
                $paginator->setItemCountPerPage($aesConfig['paginationCount']); // muss noch aus einer Configuration geholt werden
                $materialCount = $paginator->getCurrentItemCount();
            }
        }

        return new ViewModel(array(
            'searchMaterialForm' => $this->searchMaterialForm,
            'materialGroupsIdName' => $materialGroupsIdNameAssoc,
            'suppliersIdName' => $suppliersIdNameAssoc,
            'paginator' => $paginator,
            'page' => $page,
            'materialCount' => $materialCount,
            'paramsSearch' => $getParams,
        ));
    }

    public function setMaterialTable(\AmandiaES\Table\MaterialTable $materialTable) {
        $this->materialTable = $materialTable;
    }

    public function setMaterialGroupTable(\AmandiaES\Table\MaterialGroupTable $materialGroupTable) {
        $this->materialGroupTable = $materialGroupTable;
    }

    public function setProductTable(\AmandiaES\Table\ProductTable $productTable) {
        $this->productTable = $productTable;
    }

    public function setFactoryArticleTable(\AmandiaES\Table\FactoryArticleTable $factoryArticleTable) {
        $this->factoryArticleTable = $factoryArticleTable;
    }

    public function setSupplierTable(\AmandiaES\Table\SupplierTable $supplierTable) {
        $this->supplierTable = $supplierTable;
    }

}
