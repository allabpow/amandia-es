<?php
/**
 * Description of Float
 *
 * @author allapow
 */
namespace AmandiaES\Validator;


class Float extends \Zend\Validator\AbstractValidator {
    
    const FLOAT_FALSE = 'Falsches Format';
    
    protected $messageTemplates = array(
        self::FLOAT_FALSE => "'%value%' hat ein falsches Zahlenformat"
    );
    
    public function isValid($value) {
        $this->setValue($value);
        $floatVal = floatval($value);
        $floatValString = '' . $floatVal;
        if(!is_float($floatVal) || $floatValString != $value) {
            $this->error(self::FLOAT_FALSE);
            return false;
        }
        return true;
    }    
}

?>
