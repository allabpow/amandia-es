<?php
/**
 * Description of Float
 *
 * @author allapow
 */
namespace AmandiaES\Validator;


class Int extends \Zend\Validator\AbstractValidator {
    
    const INT_FALSE = 'Falsches Format';
    
    protected $messageTemplates = array(
        self::INT_FALSE => "'%value%' ist keine positive Zahl"
    );
    
    public function isValid($value) {
        $this->setValue($value);
        $intVal = intval($value);
        $intValString = '' . $intVal;
        if(!is_int($intVal) || $intValString != $value) {
            $this->error(self::INT_FALSE);
            return false;
        }
        return true;
    }    
}

?>
