<?php
namespace AmandiaES\Validator;


/**
 * Description of CsvInt
 *
 * @author allapow
 */
class CsvInt extends \Zend\Validator\AbstractValidator {
    
    const FLOAT_FALSE = 'kein CSV';
    
    protected $messageTemplates = array(
        self::FLOAT_FALSE => "'%value%' ist keine CSV"
    );
    
    public function isValid($value) {
        $this->setValue($value);
        $intArray = explode(',', $this->value);
        foreach ($intArray as $int) {
            $intString = $int;
            $intInt = (int)$int;
            if($intString != $intInt) {
                return FALSE;
            }
        }
        return true;
    }    
}

?>
