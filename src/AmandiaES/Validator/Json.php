<?php
namespace AmandiaES\Validator;


/**
 * Description of CsvInt
 *
 * @author allapow
 */
class Json extends \Zend\Validator\AbstractValidator {
    
    const FLOAT_FALSE = 'kein JSON';
    
    protected $messageTemplates = array(
        self::FLOAT_FALSE => "'%value%' ist keine JSON"
    );
    
    public function isValid($value) {
        $this->setValue($value);
        $tmp = json_decode($this->value);
        if(NULL === $tmp) {
            return FALSE;
        }
        return true;
    }    
}

?>
