<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Listener;

use \Zend\EventManager\ListenerAggregateInterface;

/**
 * Description of AmandiaListener
 *
 * @author bitkorn
 */
class AmandiaListener implements ListenerAggregateInterface {

    protected $listeners = array();

    public function attach(\Zend\EventManager\EventManagerInterface $events) {
        $this->listeners[] = $events->attach(\Zend\Mvc\MvcEvent::EVENT_RENDER, array($this, 'renderLayoutSegments'), -100);
        $this->listeners[] = $events->attach(\Zend\Mvc\MvcEvent::EVENT_DISPATCH, array($this, 'setupLocalization'), -100);
    }

    public function detach(\Zend\EventManager\EventManagerInterface $events) {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

    public function renderLayoutSegments(\Zend\Mvc\MvcEvent $e) { // \Zend\EventManager\EventInterface
        $viewModel = $e->getViewModel();
        $viewModel->setVariable('message', $viewModel->getVariable('message')); // Messages
        
//        $app = $e->getApplication();
//        $sm = $e->getApplication()->getServiceManager();
//        $logger = $sm->get('logger');
//        $logger->log(\Zend\Log\Logger::DEBUG, 'Instance of EventInterface: ' . get_class($e));
//        
        // mit dem Layout (nicht ViewModel) koennt ich hier wohl das Layout auch umschalten :)
        // im Controller gehts so:
        // $layout = $this->layout();
        // $layout->setTemplate('layout/formway');
        // in der Module.php->init() mach ichs ...bootstrap waere auch OK denk ich

        $header = new \Zend\View\Model\ViewModel();
        $header->setTemplate('layout/header');
//        $header->setVariable('message', $viewModel->getVariable('message')); // Messages
        $viewModel->addChild($header, 'header');

        $sidebar = new \Zend\View\Model\ViewModel();
        $sidebar->setTemplate('layout/sidebar');
        $viewModel->addChild($sidebar, 'sidebar');

        $footer = new \Zend\View\Model\ViewModel();
        $footer->setTemplate('layout/footer');
        $viewModel->addChild($footer, 'footer');
    }

    public function setupLocalization(\Zend\EventManager\EventInterface $e) {
        $sm = $e->getApplication()->getServiceManager();
        $viewManager = $sm->get('viewmanager');

        \Locale::setDefault('de_DE');

        $helper = $viewManager->getRenderer()->plugin('currencyformat');
        $helper->setCurrencyCode('EUR');
        $helper->setShouldShowDecimals(TRUE);
    }

}
