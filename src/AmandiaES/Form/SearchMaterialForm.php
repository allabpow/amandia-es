<?php
namespace AmandiaES\Form;

use Zend\Form\Fieldset;

/**
 * SearchMaterialForm
 *
 * @author allapow
 */
class SearchMaterialForm extends \Zend\Form\Form implements \Zend\InputFilter\InputFilterProviderInterface {
    
    /**
     *
     * @var \AmandiaES\Table\MaterialGroupTable
     */
    private $materialGroupTable;
    
    /**
     *
     * @var \AmandiaES\Table\SupplierTable
     */
    private $supplierTable;
    
    private $groups;
    private $suppliers;
    
    
    public function init() {
        $this->setAttribute('method', 'get');
        
        $this->groups = $this->materialGroupTable->getMaterialGroupsIdNameAssocc();
        $this->suppliers = $this->supplierTable->getSuppliersIdNameAssocc();

        $name = new \Zend\Form\Element\Text('name');
        $name->setAttribute('class', 'input-medium search-query');
        $name->setLabel('Material-Name');
        $this->add($name);

        $group = new \Zend\Form\Element\Select('group');
        $group->setLabel('Material-Gruppe');
        $group->setValueOptions($this->groups);
        $group->setEmptyOption('bitte wählen');
        $this->add($group);

        $supplier = new \Zend\Form\Element\Select('supplier');
        $supplier->setLabel('Lieferant');
        $supplier->setValueOptions($this->suppliers);
        $supplier->setEmptyOption('bitte wählen');
        $this->add($supplier);
        
        $search = new \Zend\Form\Element\Submit('submit');
        $search->setAttributes(array(
            'value' => 'suchen',
            'class' => 'btn',
        ));
        $this->add($search);
    }

    public function __construct() {
        parent::__construct();
    }
    
    public function getInputFilterSpecification() {
        return array(
            'name' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'group' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'Digits'),
                ),
            ),
            'supplier' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'Digits'),
                ),
            ),
        );
    }
    
    public function setMaterialGroupTable(\AmandiaES\Table\MaterialGroupTable $materialGroupTable) {
        $this->materialGroupTable = $materialGroupTable;
    }

    public function setSupplierTable(\AmandiaES\Table\SupplierTable $supplierTable) {
        $this->supplierTable = $supplierTable;
    }




}

?>
