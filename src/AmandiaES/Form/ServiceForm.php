<?php
namespace AmandiaES\Form;


/**
 * ServiceForm
 *
 * @author allapow
 */
class ServiceForm extends \Zend\Form\Form {
    
    /**
     *
     * @var \AmandiaES\Table\ServiceGroupTable
     */
    private $serviceGroupTable;
        
    public function init() {
//        parent::init();
        $this->setAttribute('method', 'post');

        $serviceFieldset = new \AmandiaES\Form\Fieldset\ServiceFieldset();
        $serviceFieldset->setServiceGroups($this->serviceGroupTable->getServiceGroupsIdNameAssocc());
        $serviceFieldset->init();
        $this->add($serviceFieldset);
        
        $submit = new \Zend\Form\Element\Submit('submit');
        $submit->setAttributes(array(
            'value' => 'speichern',
            'class' => 'btn',
        ));
        $this->add($submit);
    }

    public function __construct() {
        parent::__construct();
    }
    
    public function setServiceGroupTable(\AmandiaES\Table\ServiceGroupTable $serviceGroupTable) {
        $this->serviceGroupTable = $serviceGroupTable;
    }

}

?>
