<?php
namespace AmandiaES\Form;

use Zend\Form\Fieldset;

/**
 * OrderForm
 *
 * @author allapow
 */
class OrderForm extends \Zend\Form\Form {
    
    /**
     *
     * @var \AmandiaES\Table\CostCenterTable
     */
    private $costCenterTable;
    
    /**
     *
     * @var \AmandiaES\Table\ShipmentTable
     */
    private $shipmentTable;
    
    
    
    public function init() {
//        parent::init();
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal'); // Bootstrap 2.3.2

        $orderFieldset = new \AmandiaES\Form\Fieldset\OrderFieldset();
        $orderFieldset->setCostCenters($this->costCenterTable->getCostCentersIdNameAssocc());
        $orderFieldset->setShipments($this->shipmentTable->getShipmentsIdNameAssocc());
        $orderFieldset->init();
        $this->add($orderFieldset);
        
        $submit = new \Zend\Form\Element\Button('submit');
        $submit->label = 'Details speichern';
        $submit->setAttributes(array(
            'type' => 'submit',
            'class' => 'small',
        ));
        $this->add($submit);
        
        $start = new \Zend\Form\Element\Button('start');
        $start->label = 'Auftrag starten';
        $start->setAttributes(array(
            'type' => 'submit',
            'class' => 'small',
        ));
        $this->add($start);
        
        $finish = new \Zend\Form\Element\Button('finish');
        $finish->label = 'Auftrag fertig';
        $finish->setAttributes(array(
            'type' => 'submit',
            'class' => 'small',
        ));
        $this->add($finish);
        
        $cancel = new \Zend\Form\Element\Button('cancel');
        $cancel->label = 'Auftrag abbrechen';
        $cancel->setAttributes(array(
            'type' => 'submit',
            'class' => 'small',
        ));
        $this->add($cancel);
        
        $copy = new \Zend\Form\Element\Button('copy'); // den gibts wohl immer
        $copy->label = 'Auftrag kopieren';
        $copy->setAttributes(array(
            'type' => 'submit',
            'class' => 'small',
        ));
        $this->add($copy);
    }

    public function __construct() {
        parent::__construct();
    }

    public function setCostCenterTable(\AmandiaES\Table\CostCenterTable $costCenterTable) {
        $this->costCenterTable = $costCenterTable;
    }
    
    public function setShipmentTable(\AmandiaES\Table\ShipmentTable $shipmentTable) {
        $this->shipmentTable = $shipmentTable;
    }
}

?>
