<?php
namespace AmandiaES\Form;


/**
 * ProductTextForm
 *
 * @author allapow
 */
class ProductTextForm extends \Zend\Form\Form {
    
    public function init() {
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal'); // Bootstrap 2.3.2
        
        $this->setInputFilter(new Filter\ProductTextFilter());
        
        $textDisplayName = new \Zend\Form\Element\Text('text_display_name');
        $textDisplayName->setLabel('display Name');
        $this->add($textDisplayName);
        
        $textShort = new \Zend\Form\Element\Textarea('text_short');
        $textShort->setAttribute('class', 'ckeditor');
        $textShort->setLabel('kurze Beschreibung');
        $this->add($textShort);
        
        $textLong = new \Zend\Form\Element\Textarea('text_long');
        $textLong->setAttribute('class', 'ckeditor');
        $textLong->setLabel('lange Beschreibung');
        $this->add($textLong);

//        $productId = new \Zend\Form\Element\Hidden('product_id');
//        $this->add($productId);
        
        $submit = new \Zend\Form\Element\Button('submit_product_text');
        $submit->label = 'Texte speichern';
        $submit->setAttributes(array(
            'type' => 'submit',
            'class' => 'small',
        ));
        $this->add($submit);
    }

    public function __construct() {
        parent::__construct();
    }
}

?>
