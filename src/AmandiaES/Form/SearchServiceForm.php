<?php
namespace AmandiaES\Form;

use Zend\Form\Fieldset;

/**
 * SearchServiceForm
 *
 * @author allapow
 */
class SearchServiceForm extends \Zend\Form\Form implements \Zend\InputFilter\InputFilterProviderInterface {
    
    /**
     *
     * @var \AmandiaES\Table\ServiceGroupTable
     */
    private $serviceGroupTable;
    
    
    public function init() {
        $this->setAttribute('method', 'get');

        $name = new \Zend\Form\Element\Text('name');
        $name->setAttribute('class', 'input-medium search-query');
        $name->setLabel('Servicename');
        $this->add($name);

        $group = new \Zend\Form\Element\Select('group');
        $group->setLabel('Service-Gruppe');
        $group->setValueOptions($this->serviceGroupTable->getServiceGroupsIdNameAssocc());
        $group->setEmptyOption('bitte wählen');
        $this->add($group);
        
        $serviceNr = new \Zend\Form\Element\Text('service_nr');
        $serviceNr->setAttribute('class', 'input-medium search-query');
        $serviceNr->setLabel('Service-Nr.');
        $this->add($serviceNr);
        
        $search = new \Zend\Form\Element\Submit('submit');
        $search->setAttributes(array(
            'value' => 'suchen',
            'class' => 'btn',
        ));
        $this->add($search);
    }

    public function __construct() {
        parent::__construct();
    }
    
    public function getInputFilterSpecification() {
        return array(
            'name' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'group' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'Digits'),
                ),
            ),
            'service_nr' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
        );
    }
    
    public function setServiceGroupTable(\AmandiaES\Table\ServiceGroupTable $serviceGroupTable) {
        $this->serviceGroupTable = $serviceGroupTable;
    }

}

?>
