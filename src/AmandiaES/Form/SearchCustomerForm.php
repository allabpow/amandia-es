<?php
namespace AmandiaES\Form;


/**
 * CustomerForm
 *
 * @author allapow
 */
class SearchCustomerForm extends \Zend\Form\Form implements \Zend\InputFilter\InputFilterProviderInterface {
    
    /**
     *
     * @var \AmandiaES\Table\CustomerGroupTable 
     */
    private $customerGroupTable;
    
    private $customerGroups;
    
    public function init() {
//        parent::init();
        $this->setAttribute('method', 'get');
        
        $this->customerGroups = $this->customerGroupTable->getCustomerGroupsIdNameAssocc();

        $customerNr = new \Zend\Form\Element\Text('customer_nr');
        $customerNr->setLabel('Kunden-Nr.');
        $this->add($customerNr);

        $customerGroup = new \Zend\Form\Element\Select('group');
        $customerGroup->setLabel('Gruppe');
        $customerGroup->setValueOptions($this->customerGroups);
        $customerGroup->setEmptyOption('bitte wählen');
        $this->add($customerGroup);
        
        $vorname = new \Zend\Form\Element\Text('name_first');
        $vorname->setLabel('Vorname');
        $this->add($vorname);
        
        $name = new \Zend\Form\Element\Text('name_last');
        $name->setLabel('Nachname');
        $this->add($name);
        
        $str = new \Zend\Form\Element\Text('str');
        $str->setLabel('Straße');
        $this->add($str);
        
        $zip = new \Zend\Form\Element\Text('zip');
        $zip->setLabel('PLZ');
        $this->add($zip);
        
        $city = new \Zend\Form\Element\Text('city');
        $city->setLabel('Stadt');
        $this->add($city);
        
        $submit = new \Zend\Form\Element\Submit('submit');
        $submit->setAttributes(array(
            'value' => 'suchen',
            'class' => 'btn',
        ));
        $this->add($submit);
    }

    public function __construct() {
        parent::__construct();
    }
    
    public function getInputFilterSpecification() {
        return array(
            'customer_nr' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'group' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'Digits'),
                ),
            ),
            'name_first' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'name_last' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'str' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'str_nr' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'zip' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'city' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
        );
    }
    
    public function setCustomerGroupTable(\AmandiaES\Table\CustomerGroupTable $customerGroupTable) {
        $this->customerGroupTable = $customerGroupTable;
    }

}

?>
