<?php
namespace AmandiaES\Form;


/**
 * ArticleForm
 *
 * @author allapow
 */
class ArticleForm extends \Zend\Form\Form {
    
    /**
     *
     * @var \AmandiaES\Table\SupplierTable
     */
    private $supplierTable;
    
    /**
     *
     * @var \AmandiaES\Table\ArticleGroupTable
     */
    private $articleGroupTable;
    
    /**
     *
     * @var \AmandiaES\Table\SymbolGeomTable
     */
    private $symbolGeomTable;
    
    /**
     *
     * @var \AmandiaES\Table\SymbolWeightTable
     */
    private $symbolWeightTable;
    
    
    
    public function init() {
//        parent::init();
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal'); // Bootstrap 2.3.2

        $articleFieldset = new \AmandiaES\Form\Fieldset\ArticleFieldset();
        $articleFieldset->setArticleGroups($this->articleGroupTable->getArticleGroupsIdNameAssocc());
        $articleFieldset->setSuppliers($this->supplierTable->getSuppliersIdNameAssocc());
        $articleFieldset->setMeasureSymbolGeoms($this->symbolGeomTable->getSymbolIdNameAssocc());
        $articleFieldset->setMeasureSymbolWeights($this->symbolWeightTable->getSymbolIdNameAssocc());
        $articleFieldset->init();
        $this->add($articleFieldset);
        
        $submit = new \Zend\Form\Element\Submit('submit');
        $submit->setAttributes(array(
            'value' => 'speichern',
            'class' => 'btn',
        ));
        $this->add($submit);
    }

    public function __construct() {
        parent::__construct();
    }
    
    public function setArticleGroupTable(\AmandiaES\Table\ArticleGroupTable $articleGroupTable) {
        $this->articleGroupTable = $articleGroupTable;
    }

    public function setSupplierTable(\AmandiaES\Table\SupplierTable $supplierTable) {
        $this->supplierTable = $supplierTable;
    }
    
    public function setSymbolGeomTable(\AmandiaES\Table\SymbolGeomTable $symbolGeomTable) {
        $this->symbolGeomTable = $symbolGeomTable;
    }

    public function setSymbolWeightTable(\AmandiaES\Table\SymbolWeightTable $symbolWeightTable) {
        $this->symbolWeightTable = $symbolWeightTable;
    }

}

?>
