<?php
namespace AmandiaES\Form;


/**
 * FactoryArticleForm
 *
 * @author allapow
 */
class FactoryArticleForm extends \Zend\Form\Form {
    
    /**
     *
     * @var \AmandiaES\Table\FactoryArticleGroupTable
     */
    private $factoryArticleGroupTable;
    
    /**
     *
     * @var \AmandiaES\Table\SymbolGeomTable
     */
    private $symbolGeomTable;
    
    /**
     *
     * @var \AmandiaES\Table\SymbolWeightTable
     */
    private $symbolWeightTable;
    
    
    
    public function init() {
//        parent::init();
        $this->setAttribute('method', 'post');

        $fArticleFieldset = new \AmandiaES\Form\Fieldset\FactoryArticleFieldset();
        $fArticleFieldset->setFactoryArticleGroups($this->factoryArticleGroupTable->getFactoryArticleGroupsIdNameAssocc());
        $fArticleFieldset->setMeasureSymbolGeoms($this->symbolGeomTable->getSymbolIdNameAssocc());
        $fArticleFieldset->setMeasureSymbolWeights($this->symbolWeightTable->getSymbolIdNameAssocc());
        $fArticleFieldset->init();
        $this->add($fArticleFieldset);
        
        $submit = new \Zend\Form\Element\Button('submit');
        $submit->label = 'B-Artikel Details speichern';
        $submit->setAttributes(array(
            'type' => 'submit',
            'class' => 'small',
        ));
        $this->add($submit);
    }

    public function __construct() {
        parent::__construct();
    }
    
    public function setFactoryArticleGroupTable(\AmandiaES\Table\FactoryArticleGroupTable $factoryArticleGroupTable) {
        $this->factoryArticleGroupTable = $factoryArticleGroupTable;
    }
    
    public function setSymbolGeomTable(\AmandiaES\Table\SymbolGeomTable $symbolGeomTable) {
        $this->symbolGeomTable = $symbolGeomTable;
    }

    public function setSymbolWeightTable(\AmandiaES\Table\SymbolWeightTable $symbolWeightTable) {
        $this->symbolWeightTable = $symbolWeightTable;
    }

}

?>
