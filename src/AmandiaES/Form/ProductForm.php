<?php
namespace AmandiaES\Form;

use Zend\Form\Fieldset;

/**
 * ProductForm
 *
 * @author allapow
 */
class ProductForm extends \Zend\Form\Form {
    
    /**
     *
     * @var \AmandiaES\Table\SupplierTable
     */
//    private $supplierTable;
    
    /**
     *
     * @var \AmandiaES\Table\ProductGroupTable
     */
    private $productGroupTable;
    
    /**
     *
     * @var \AmandiaES\Table\ShipmentTable
     */
//    private $shipmentTable;
    
    public function init() {
//        parent::init();
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal'); // Bootstrap 2.3.2

        $productFieldset = new \AmandiaES\Form\Fieldset\ProductFieldset();
//        $productFieldset->setSuppliers($this->supplierTable->getSuppliersIdNameAssocc());
//        $productFieldset->setMaterials($this->materialTable->getMaterialsIdNameAssocc());
        $productFieldset->setProductGroups($this->productGroupTable->getProductGroupsIdNameAssocc());
//        $productFieldset->setShipments($this->shipmentTable->getShipmentsIdNameAssocc());
        $productFieldset->init();
        $this->add($productFieldset);
        
        $submit = new \Zend\Form\Element\Button('submit_product');
        $submit->label = 'Produkt Details speichern';
        $submit->setAttributes(array(
            'type' => 'submit',
            'class' => 'small',
        ));
        $this->add($submit);
    }

    public function __construct() {
        parent::__construct();
    }

//    public function setSupplierTable(\AmandiaES\Table\SupplierTable $supplierTable) {
//        $this->supplierTable = $supplierTable;
//    }

    public function setProductGroupTable(\AmandiaES\Table\ProductGroupTable $productGroupTable) {
        $this->productGroupTable = $productGroupTable;
    }
    
//    public function setShipmentTable(\AmandiaES\Table\ShipmentTable $shipmentTable) {
//        $this->shipmentTable = $shipmentTable;
//    }


}

?>
