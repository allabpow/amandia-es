<?php

namespace AmandiaES\Form;

/**
 * SearchFactoryArticleForm
 *
 * @author allapow
 */
class SearchFactoryArticleForm extends \Zend\Form\Form implements \Zend\InputFilter\InputFilterProviderInterface {

    /**
     *
     * @var \AmandiaES\Table\FactoryArticleGroupTable
     */
    private $factoryArticleGroupTable;
    private $groups;

    public function init() {
        $this->setAttribute('method', 'get');

        $this->groups = $this->factoryArticleGroupTable->getFactoryArticleGroupsIdNameAssocc();

        $name = new \Zend\Form\Element\Text('name');
        $name->setAttribute('class', 'input-medium search-query');
        $name->setLabel('B-Artikel-Name');
        $this->add($name);

        $fArticleNr = new \Zend\Form\Element\Text('factory_article_nr');
        $fArticleNr->setAttribute('class', 'input-medium search-query');
        $fArticleNr->setLabel('B-Artikel-Nr.');
        $this->add($fArticleNr);

        $group = new \Zend\Form\Element\Select('group');
        $group->setLabel('B-Artikel-Gruppe');
        $group->setValueOptions($this->groups);
        $group->setEmptyOption('bitte wählen');
        $this->add($group);

        $search = new \Zend\Form\Element\Submit('submit');
        $search->setAttributes(array(
            'value' => 'suchen',
            'class' => 'btn',
        ));
        $this->add($search);
    }

    public function __construct() {
        parent::__construct();
    }

    public function getInputFilterSpecification() {
        return array(
            'name' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'factory_article_nr' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'group' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'Digits'),
                ),
            ),
        );
    }

    public function setFactoryArticleGroupTable(\AmandiaES\Table\FactoryArticleGroupTable $factoryArticleGroupTable) {
        $this->factoryArticleGroupTable = $factoryArticleGroupTable;
    }

}

?>
