<?php

namespace AmandiaES\Form\Fieldset;

use \Zend\Form\Fieldset;

class FactoryArticleFieldset extends Fieldset implements \Zend\InputFilter\InputFilterProviderInterface {

    /**
     *
     * @var array B-Artikel Gruppen from db.aes_factory_article_group
     */
    private $factoryArticleGroups;

    /**
     *
     * @var array Symbol Geometrie aus db.aes_symbol_geom
     */
    private $measureSymbolGeoms;

    /**
     *
     * @var array Symbol Gewicht from db.aes_symbol_weight
     */
    private $measureSymbolWeights;
    

    public function init() {

        $orderId = new \Zend\Form\Element\Hidden('id');
        $this->add($orderId);
        
        $articleNr = new \Zend\Form\Element\Text('factory_article_nr');
        $articleNr->setLabel('B-Artikel-Nr.');
        $this->add($articleNr);
        
        $name = new \Zend\Form\Element\Text('name');
        $name->setAttribute('id', 'name');
        $name->setLabel('Artikel Name');
        $this->add($name);

        $group = new \Zend\Form\Element\Select('group');
        $group->setLabel('B-Artikel Gruppe');
        $group->setValueOptions($this->factoryArticleGroups);
        $group->setEmptyOption('bitte wählen');
        $this->add($group);

        $measureScaleGeom = new \Zend\Form\Element\Text('measure_scale_geom');
        $measureScaleGeom->setLabel('Geometrie - Größe');
        $this->add($measureScaleGeom);

        $measureSymbolGeom = new \Zend\Form\Element\Select('measure_symbol_geom');
        $measureSymbolGeom->setLabel('Geometrie - Symbol');
        $measureSymbolGeom->setValueOptions($this->measureSymbolGeoms);
        $measureSymbolGeom->setEmptyOption('bitte wählen');
        $this->add($measureSymbolGeom);

        $measureScaleWeight = new \Zend\Form\Element\Text('measure_scale_weight');
        $measureScaleWeight->setLabel('Gewicht - Größe');
        $this->add($measureScaleWeight);

        $measureSymbolWeight = new \Zend\Form\Element\Select('measure_symbol_weight');
        $measureSymbolWeight->setLabel('Gewicht - Symbol');
        $measureSymbolWeight->setValueOptions($this->measureSymbolWeights);
        $measureSymbolWeight->setEmptyOption('bitte wählen');
        $this->add($measureSymbolWeight);
        
        $priceSell = new \Zend\Form\Element\Text('price_sell');
        $priceSell->setLabel('Preis Verkauf');
        $this->add($priceSell);
        
        $description = new \Zend\Form\Element\Textarea('description');
        $description->setLabel('Beschreibung');
        $this->add($description);
    }
    
    public function __construct($name = 'factory_article') {
        parent::__construct($name);
    }

    
    
    public function getInputFilterSpecification() {
        return array(
            'id' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new \AmandiaES\Validator\Int(),
                ),
            ),
            'factory_article_nr' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                    ),
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 6,
                            'max' => 45,
                        ),
                    ),
                ),
            ),
            'name' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                    ),
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 45,
                        ),
                    ),
                ),
            ),
            'group' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                    ),
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array_keys($this->factoryArticleGroups),
                        ),
                    ),
                ),
            ),
            'measure_scale_geom' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                    ),
                    new \AmandiaES\Validator\Float(),
                ),
            ),
            'measure_symbol_geom' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array_keys($this->measureSymbolGeoms),
                        ),
                    ),
                ),
            ),
            'measure_scale_weight' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                    ),
                    new \AmandiaES\Validator\Float(),
                ),
            ),
            'measure_symbol_weight' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array_keys($this->measureSymbolWeights),
                        ),
                    ),
                ),
            ),
            'price_sell' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new \AmandiaES\Validator\Float(),
                ),
            ),
            'description' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 255,
                        ),
                    ),
                ),
            ),
        );
    }
    
    public function setFactoryArticleGroups(array $factoryArticleGroups) {
        $this->factoryArticleGroups = $factoryArticleGroups;
    }

    public function setMeasureSymbolGeoms($measureSymbolGeoms) {
        $this->measureSymbolGeoms = $measureSymbolGeoms;
    }

    public function setMeasureSymbolWeights($measureSymbolWeights) {
        $this->measureSymbolWeights = $measureSymbolWeights;
    }

}

?>
