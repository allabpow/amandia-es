<?php

namespace AmandiaES\Form\Fieldset;

use \Zend\Form\Fieldset;

class MaterialFieldset extends Fieldset implements \Zend\InputFilter\InputFilterProviderInterface {

    /**
     *
     * @var array Artikel Gruppen from db.aes_material_group
     */
    private $materialGroups;

    /**
     *
     * @var array Supplier from db.aes_supplier
     */
    private $suppliers;

    /**
     *
     * @var array Symbol Geometrie aus db.aes_symbol_geom
     */
    private $measureSymbolGeoms;

    /**
     *
     * @var array Symbol Gewicht from db.aes_symbol_weight
     */
    private $measureSymbolWeights;

    public function init() {

        $materialId = new \Zend\Form\Element\Hidden('id');
        $this->add($materialId);

        $name = new \Zend\Form\Element\Text('name');
        $name->setLabel('Material-Name');
        $this->add($name);

        $group = new \Zend\Form\Element\Select('group');
        $group->setLabel('Material Gruppe');
        $group->setValueOptions($this->materialGroups);
        $group->setEmptyOption('bitte wählen');
        $this->add($group);

        $supplier = new \Zend\Form\Element\Select('supplier');
        $supplier->setLabel('Lieferant');
        $supplier->setValueOptions($this->suppliers);
        $supplier->setEmptyOption('bitte wählen');
        $this->add($supplier);

        $measureScaleGeom = new \Zend\Form\Element\Text('measure_scale_geom');
        $measureScaleGeom->setLabel('Geometrie - Größe');
        $this->add($measureScaleGeom);

        $measureSymbolGeom = new \Zend\Form\Element\Select('measure_symbol_geom');
        $measureSymbolGeom->setLabel('Geometrie - Symbol');
        $measureSymbolGeom->setValueOptions($this->measureSymbolGeoms);
        $measureSymbolGeom->setEmptyOption('bitte wählen');
        $this->add($measureSymbolGeom);

        $measureScaleWeight = new \Zend\Form\Element\Text('measure_scale_weight');
        $measureScaleWeight->setLabel('Gewicht - Größe');
        $this->add($measureScaleWeight);

        $measureSymbolWeight = new \Zend\Form\Element\Select('measure_symbol_weight');
        $measureSymbolWeight->setLabel('Gewicht - Symbol');
        $measureSymbolWeight->setValueOptions($this->measureSymbolWeights);
        $measureSymbolWeight->setEmptyOption('bitte wählen');
        $this->add($measureSymbolWeight);

        $priceBuy = new \Zend\Form\Element\Text('price_buy');
        $priceBuy->setLabel('Preis Einkauf');
        $this->add($priceBuy);

        $priceSell = new \Zend\Form\Element\Text('price_sell');
        $priceSell->setLabel('Preis Verkauf');
        $this->add($priceSell);

        $description = new \Zend\Form\Element\Textarea('description');
        $description->setLabel('Beschreibung');
        $this->add($description);
    }

    public function __construct($name = 'material') {
        parent::__construct($name);
    }

    public function getInputFilterSpecification() {
        return array(
            'id' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new \AmandiaES\Validator\Int(),
                ),
            ),
            'name' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Dieses Feld darf nicht leer bleiben!',
                            ),
                        ),
                    ),
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 45,
                            'messages' => array(
                                \Zend\Validator\StringLength::TOO_LONG => 'Maximal %max% Zeichen!',
                            ),
                        ),
                    ),
                ),
            ),
            'group' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Dieses Feld darf nicht leer bleiben!',
                            ),
                        ),
                    ),
                    array(
                        'name' => 'InArray', // ...there is no automatic InArray validator in \Zend\Form\Element\Select()
                        'options' => array(
                            'haystack' => array_keys($this->materialGroups),
                        ),
                    ),
                ),
            ),
            'supplier' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array_keys($this->suppliers),
                        ),
                    ),
                ),
            ),
            'measure_scale_geom' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                    ),
                    new \AmandiaES\Validator\Float(),
                ),
            ),
            'measure_symbol_geom' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array_keys($this->measureSymbolGeoms),
                        ),
                    ),
                ),
            ),
            'measure_scale_weight' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                    ),
                    new \AmandiaES\Validator\Float(),
                ),
            ),
            'measure_symbol_weight' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array_keys($this->measureSymbolWeights),
                        ),
                    ),
                ),
            ),
            'price_buy' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new \AmandiaES\Validator\Float(),
                ),
            ),
            'price_sell' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new \AmandiaES\Validator\Float(),
                ),
            ),
            'description' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 255,
                        ),
                    ),
                ),
            ),
        );
    }

    public function setMaterialGroups($materialGroups) {
        $this->materialGroups = $materialGroups;
    }

    public function setSuppliers($suppliers) {
        $this->suppliers = $suppliers;
    }

    public function setMeasureSymbolGeoms($measureSymbolGeoms) {
        $this->measureSymbolGeoms = $measureSymbolGeoms;
    }

    public function setMeasureSymbolWeights($measureSymbolWeights) {
        $this->measureSymbolWeights = $measureSymbolWeights;
    }

}

?>
