<?php

namespace AmandiaES\Form\Fieldset;

use \Zend\Form\Fieldset;

class ArticleFieldset extends Fieldset implements \Zend\InputFilter\InputFilterProviderInterface {

    /**
     *
     * @var array Artikel Gruppen from db.aes_article_group
     */
    private $articleGroups;

    /**
     *
     * @var array Supplier from db.aes_supplier
     */
    private $suppliers;

    /**
     *
     * @var array Symbol Geometrie aus db.aes_symbol_geom
     */
    private $measureSymbolGeoms;

    /**
     *
     * @var array Symbol Gewicht from db.aes_symbol_weight
     */
    private $measureSymbolWeights;
    

    public function init() {

        $orderId = new \Zend\Form\Element\Hidden('id');
        $this->add($orderId);
        
        $articleNr = new \Zend\Form\Element\Text('article_nr');
        $articleNr->setLabel('Artikel-Nr.');
        $this->add($articleNr);
        
        $articleNrExtern = new \Zend\Form\Element\Text('article_nr_extern');
        $articleNrExtern->setLabel('Artikel-Nr. extern');
        $this->add($articleNrExtern);
        
        $name = new \Zend\Form\Element\Text('name');
        $name->setAttribute('id', 'name');
        $name->setLabel('Artikel Name');
        $this->add($name);

        $group = new \Zend\Form\Element\Select('group');
        $group->setLabel('Artikel Gruppe');
        $group->setValueOptions($this->articleGroups);
        $group->setEmptyOption('bitte wählen');
        $this->add($group);

        $supplier = new \Zend\Form\Element\Select('supplier');
        $supplier->setLabel('Lieferant');
        $supplier->setValueOptions($this->suppliers);
        $supplier->setEmptyOption('bitte wählen');
        $this->add($supplier);

        $measureScaleGeom = new \Zend\Form\Element\Text('measure_scale_geom');
        $measureScaleGeom->setLabel('Geometrie - Größe');
        $this->add($measureScaleGeom);

        $measureSymbolGeom = new \Zend\Form\Element\Select('measure_symbol_geom');
        $measureSymbolGeom->setLabel('Geometrie - Symbol');
        $measureSymbolGeom->setValueOptions($this->measureSymbolGeoms);
        $measureSymbolGeom->setEmptyOption('bitte wählen');
        $this->add($measureSymbolGeom);

        $measureScaleWeight = new \Zend\Form\Element\Text('measure_scale_weight');
        $measureScaleWeight->setLabel('Gewicht - Größe');
        $this->add($measureScaleWeight);

        $measureSymbolWeight = new \Zend\Form\Element\Select('measure_symbol_weight');
        $measureSymbolWeight->setLabel('Gewicht - Symbol');
        $measureSymbolWeight->setValueOptions($this->measureSymbolWeights);
        $measureSymbolWeight->setEmptyOption('bitte wählen');
        $this->add($measureSymbolWeight);
        
        $priceBuy = new \Zend\Form\Element\Text('price_buy');
        $priceBuy->setLabel('Preis Einkauf');
        $this->add($priceBuy);
        
        $priceSell = new \Zend\Form\Element\Text('price_sell');
        $priceSell->setLabel('Preis Verkauf');
        $this->add($priceSell);
        
        $description = new \Zend\Form\Element\Textarea('description');
        $description->setLabel('Beschreibung');
        $this->add($description);
    }
    
    public function __construct($name = 'article') {
        parent::__construct($name);
    }

    
    
    public function getInputFilterSpecification() {
        return array(
            'id' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new \AmandiaES\Validator\Int(),
                ),
            ),
            'article_nr' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                    ),
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 6,
                            'max' => 45,
                        ),
                    ),
                ),
            ),
            'article_nr_extern' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 6,
                            'max' => 45,
                        ),
                    ),
                ),
            ),
            'name' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                    ),
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 45,
                        ),
                    ),
                ),
            ),
            'group' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                    ),
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array_keys($this->articleGroups),
                        ),
                    ),
                ),
            ),
            'supplier' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array_keys($this->suppliers),
                        ),
                    ),
                ),
            ),
            'measure_scale_geom' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                    ),
                    new \AmandiaES\Validator\Float(),
                ),
            ),
            'measure_symbol_geom' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array_keys($this->measureSymbolGeoms),
                        ),
                    ),
                ),
            ),
            'measure_scale_weight' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                    ),
                    new \AmandiaES\Validator\Float(),
                ),
            ),
            'measure_symbol_weight' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array_keys($this->measureSymbolWeights),
                        ),
                    ),
                ),
            ),
            'price_buy' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new \AmandiaES\Validator\Float(),
                ),
            ),
            'price_sell' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new \AmandiaES\Validator\Float(),
                ),
            ),
            'description' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 255,
                        ),
                    ),
                ),
            ),
        );
    }
    
    public function setArticleGroups($articleGroups) {
        $this->articleGroups = $articleGroups;
    }

    public function setSuppliers($suppliers) {
        $this->suppliers = $suppliers;
    }

    public function setMeasureSymbolGeoms($measureSymbolGeoms) {
        $this->measureSymbolGeoms = $measureSymbolGeoms;
    }

    public function setMeasureSymbolWeights($measureSymbolWeights) {
        $this->measureSymbolWeights = $measureSymbolWeights;
    }
    
}

?>
