<?php

namespace AmandiaES\Form\Fieldset;

use \Zend\Form\Fieldset;

class OrderFieldset extends Fieldset implements \Zend\InputFilter\InputFilterProviderInterface {

    /**
     *
     * @var array Kostenstelle from db.aes_cost_center
     */
    private $costCenters;

    /**
     *
     * @var array Versandarten from db.aes_shipment
     */
    private $shipments;
    

    public function init() {

        $orderId = new \Zend\Form\Element\Hidden('id');
        $this->add($orderId);
        
        $orderNr = new \Zend\Form\Element\Text('order_nr');
        $orderNr->setLabel('Auftrags-Nr.'); // Anfrage-Nr. ?
        $this->add($orderNr);

        $userCreate = new \Zend\Form\Element\Hidden('user_create'); // logged in user
        $this->add($userCreate);

        $userLastEditor = new \Zend\Form\Element\Hidden('user_last_editor'); // logged in user
        $this->add($userLastEditor);
        
        $costCenter = new \Zend\Form\Element\Select('cost_center');
        $costCenter->setLabel('Kostenstelle');
        $costCenter->setValueOptions($this->costCenters);
        $costCenter->setEmptyOption('bitte wählen');
        $this->add($costCenter);

        $shipment = new \Zend\Form\Element\Select('shipment');
        $shipment->setLabel('Versand');
        $shipment->setValueOptions($this->shipments);
        $this->add($shipment);
        
        $descrip = new \Zend\Form\Element\Textarea('description');
        $descrip->setLabel('Beschreibung');
        $this->add($descrip);
        
    }
    
    public function __construct($name = 'order') {
        parent::__construct($name);
    }

    
    
    public function getInputFilterSpecification() {
        return array(
            'id' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new \AmandiaES\Validator\Int(),
                ),
            ),
            'order_nr' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Dieses Feld darf nicht leer bleiben!',
                            ),
                        ),
                    ),
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 6,
                            'max' => 30,
                            'messages' => array(
                                \Zend\Validator\StringLength::TOO_SHORT => 'Mindestens %min% Zeichen!',
                                \Zend\Validator\StringLength::TOO_LONG => 'Maximal %max% Zeichen!',
                            ),
                        ),
                    ),
                ),
            ),
//            'customer_id' => array(
//                'required' => FALSE,
//                'filters' => array(
//                    array('name' => 'StripTags'),
//                    array('name' => 'StringTrim'),
//                    array('name' => 'Digits'),
//                ),
//            ),
            'user_create' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                    ),
                    new \AmandiaES\Validator\Int(), // besser ein InArray mit allen employee_id's
                ),
            ),
            'user_last_editor' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                    ),
                    new \AmandiaES\Validator\Int(), // besser ein InArray mit allen employee_id's
                ),
            ),
            'cost_center' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array_keys($this->costCenters),
                        ),
                    ),
                ),
            ),
            'shipment' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array_keys($this->shipments),
                        ),
                    ),
                ),
            ),
            'description' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 255,
                            'messages' => array(
                                \Zend\Validator\StringLength::TOO_LONG => 'Maximal %max% Zeichen!',
                            ),
                        ),
                    ),
                ),
            ),
        );
    }
    
    /**
     * Sets the cost center for \Zend\Form\Element\Select Field cost_center
     * @param array $costCenter array(1 => 'first cost center', 2 => 'second cost center')
     */
    public function setCostCenters(array $costCenter) {
        $this->costCenters = $costCenter;
    }
    
    public function setShipments($shipments) {
        $this->shipments = $shipments;
    }

    
}

?>
