<?php

namespace AmandiaES\Form\Fieldset;

use \Zend\Form\Fieldset;

class ServiceFieldset extends Fieldset implements \Zend\InputFilter\InputFilterProviderInterface {

    /**
     *
     * @var array Artikel Gruppen from db.aes_article_group
     */
    private $serviceGroups;
    

    public function init() {

        $orderId = new \Zend\Form\Element\Hidden('id');
        $this->add($orderId);
        
        $serviceNr = new \Zend\Form\Element\Text('service_nr');
        $serviceNr->setLabel('Service-Nr.');
        $this->add($serviceNr);
        
        $name = new \Zend\Form\Element\Text('name');
        $name->setAttribute('id', 'name'); // wozu das nochmal ...kam aus Artikel
        $name->setLabel('Service Name');
        $this->add($name);

        $group = new \Zend\Form\Element\Select('group');
        $group->setLabel('Service Gruppe');
        $group->setValueOptions($this->serviceGroups);
        $group->setEmptyOption('bitte wählen');
        $this->add($group);
        
        $priceBuy = new \Zend\Form\Element\Text('price_buy');
        $priceBuy->setLabel('Preis Einkauf');
        $this->add($priceBuy);
        
        $priceSell = new \Zend\Form\Element\Text('price_sell');
        $priceSell->setLabel('Preis Verkauf');
        $this->add($priceSell);
        
        $description = new \Zend\Form\Element\Textarea('description');
        $description->setLabel('Beschreibung');
        $this->add($description);
    }
    
    public function __construct($name = 'service') {
        parent::__construct($name);
    }

    
    
    public function getInputFilterSpecification() {
        return array(
            'id' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new \AmandiaES\Validator\Int(),
                ),
            ),
            'service_nr' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                    ),
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 6,
                            'max' => 45,
                        ),
                    ),
                ),
            ),
            'name' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                    ),
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 45,
                        ),
                    ),
                ),
            ),
            'group' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                    ),
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array_keys($this->serviceGroups),
                        ),
                    ),
                ),
            ),
            'price_buy' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new \AmandiaES\Validator\Float(),
                ),
            ),
            'price_sell' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new \AmandiaES\Validator\Float(),
                ),
            ),
            'description' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 255,
                        ),
                    ),
                ),
            ),
        );
    }
    
    public function setServiceGroups(array $serviceGroups) {
        $this->serviceGroups = $serviceGroups;
    }

}

?>
