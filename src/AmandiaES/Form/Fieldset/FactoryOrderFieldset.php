<?php

namespace AmandiaES\Form\Fieldset;

use \Zend\Form\Fieldset;

class FactoryOrderFieldset extends Fieldset implements \Zend\InputFilter\InputFilterProviderInterface {

    private $locations;

    public function init() {

        $foId = new \Zend\Form\Element\Hidden('id');
        $this->add($foId);

        $name = new \Zend\Form\Element\Text('factory_order_nr');
        $name->setLabel('BA-Nr.');
        $this->add($name);

        $location = new \Zend\Form\Element\Select('location');
        $location->setLabel('Standort');
        $location->setValueOptions($this->locations);
        $location->setEmptyOption('bitte wählen');
        $this->add($location);

        $comment = new \Zend\Form\Element\Textarea('comment');
        $comment->setLabel('Kommentar');
        $this->add($comment);
    }

    public function __construct($name = 'factory_order') {
        parent::__construct($name);
    }

    public function getInputFilterSpecification() {
        return array(
            'id' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new \AmandiaES\Validator\Int(),
                ),
            ),
            'factory_order_nr' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                    ),
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 45,
                        ),
                    ),
                ),
            ),
            'location' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'Digits'),
                ),
                'validators' => array(
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array_keys($this->locations),
                        ),
                    ),
                ),
            ),
            'comment' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 255,
                        ),
                    ),
                ),
            ),
        );
    }

    public function setLocations($locations) {
        $this->locations = $locations;
    }

}

?>
