<?php

namespace AmandiaES\Form\Fieldset;

use \Zend\Form\Fieldset;

class CustomerFieldset extends Fieldset implements \Zend\InputFilter\InputFilterProviderInterface {

    private $customerGroups;
    private $countries;

    public function init() {
//        parent::init();

        $customerId = new \Zend\Form\Element\Hidden('id');
        $this->add($customerId);
        
        $customerNr = new \Zend\Form\Element\Text('customer_nr');
        $customerNr->setLabel('Kunden-Nr.');
        $this->add($customerNr);

        $customerGroup = new \Zend\Form\Element\Select('group');
        $customerGroup->setLabel('Gruppe');
        $customerGroup->setValueOptions($this->customerGroups);
        $customerGroup->setValue(1);
        $this->add($customerGroup);
        
        $vorname = new \Zend\Form\Element\Text('name_first');
        $vorname->setLabel('Vorname');
        $this->add($vorname);
        
        $name = new \Zend\Form\Element\Text('name_last');
        $name->setLabel('Nachname');
        $this->add($name);
        
        $nameCompany = new \Zend\Form\Element\Text('name_company');
        $nameCompany->setLabel('Firmenname');
        $this->add($nameCompany);
        
        $str = new \Zend\Form\Element\Text('str');
        $str->setLabel('Straße');
        $this->add($str);
        
        $strNr = new \Zend\Form\Element\Text('str_nr');
        $strNr->setLabel('Hausnummer');
        $this->add($strNr);
        
        $zip = new \Zend\Form\Element\Text('zip');
        $zip->setLabel('PLZ');
        $this->add($zip);
        
        $city = new \Zend\Form\Element\Text('city');
        $city->setLabel('Stadt');
        $this->add($city);

        $country = new \Zend\Form\Element\Select('country');
        $country->setLabel('Land');
        $country->setValueOptions($this->countries);
        $country->setValue(47);
        $this->add($country);
        
        // ############################################################
        
        $vorname2ed = new \Zend\Form\Element\Text('sec_name_first');
        $vorname2ed->setLabel('Vorname');
        $this->add($vorname2ed);
        
        $name2ed = new \Zend\Form\Element\Text('sec_name_last');
        $name2ed->setLabel('Nachname');
        $this->add($name2ed);
        
        $nameCompany2ed = new \Zend\Form\Element\Text('sec_name_company');
        $nameCompany2ed->setLabel('Firmenname');
        $this->add($nameCompany2ed);
        
        $str2ed = new \Zend\Form\Element\Text('sec_str');
        $str2ed->setLabel('Straße');
        $this->add($str2ed);
        
        $strNr2ed = new \Zend\Form\Element\Text('sec_str_nr');
        $strNr2ed->setLabel('Hausnummer');
        $this->add($strNr2ed);
        
        $zip2ed = new \Zend\Form\Element\Text('sec_zip');
        $zip2ed->setLabel('PLZ');
        $this->add($zip2ed);
        
        $city2ed = new \Zend\Form\Element\Text('sec_city');
        $city2ed->setLabel('Stadt');
        $this->add($city2ed);

        $country2ed = new \Zend\Form\Element\Select('sec_country');
        $country2ed->setLabel('Land');
        $country2ed->setValueOptions($this->countries);
        $country2ed->setValue(47);
        $this->add($country2ed);
    }
    
    public function __construct($name = 'customer') {
        parent::__construct($name);
    }

    
    
    public function getInputFilterSpecification() {
        return array(
            'id' => array(
                'required' => FALSE, // leer bei erster Eingabe
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new \AmandiaES\Validator\Int(),
                ),
            ),
            'customer_nr' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                    ),
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 45,
                        ),
                    ),
                ),
            ),
            'group' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'Digits'),
                ),
                'validators' => array(
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array_keys($this->customerGroups),
                        ),
                    ),
                ),
            ),
            'name_first' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 45,
                        ),
                    ),
                ),
            ),
            'name_last' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 45,
                        ),
                    ),
                ),
            ),
            'name_company' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 100,
                        ),
                    ),
                ),
            ),
            'str' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 100,
                        ),
                    ),
                ),
            ),
            'str_nr' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 10,
                        ),
                    ),
                ),
            ),
            'zip' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 10,
                        ),
                    ),
                ),
            ),
            'city' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 80,
                        ),
                    ),
                ),
            ),
            'country' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array_keys($this->countries),
                        ),
                    ),
                ),
            ),
            //########################################
            'sec_name_first' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 45,
                        ),
                    ),
                ),
            ),
            'sec_name_last' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 45,
                        ),
                    ),
                ),
            ),
            'sec_name_company' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 100,
                        ),
                    ),
                ),
            ),
            'sec_str' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 100,
                        ),
                    ),
                ),
            ),
            'sec_str_nr' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 10,
                        ),
                    ),
                ),
            ),
            'sec_zip' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 10,
                        ),
                    ),
                ),
            ),
            'sec_city' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 80,
                        ),
                    ),
                ),
            ),
            'sec_country' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array_keys($this->countries),
                        ),
                    ),
                ),
            ),
        );
    }
    
    public function setCustomerGroups(array $customerGroups) {
        $this->customerGroups = $customerGroups;
    }

    public function setCountries(array $countries) {
        $this->countries = $countries;
    }

    
}

?>
