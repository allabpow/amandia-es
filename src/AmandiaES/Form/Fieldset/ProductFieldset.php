<?php

namespace AmandiaES\Form\Fieldset;

use \Zend\Form\Fieldset;

class ProductFieldset extends Fieldset implements \Zend\InputFilter\InputFilterProviderInterface {

    /**
     *
     * @var array Suppliers from db.aes_supplier
     */
//    private $suppliers;

    /**
     *
     * @var array Produktgruppen from db.aes_product_group
     */
    private $productGroups;

    /**
     *
     * @var array Versandarten from db.aes_shipment
     */
//    private $shipments;


    public function init() {

        $prodId = new \Zend\Form\Element\Hidden('id');
        $this->add($prodId);
        
        $prodNr = new \Zend\Form\Element\Text('product_nr');
        $prodNr->setLabel('Produkt-Nr.');
        $this->add($prodNr);
        
        $prodName = new \Zend\Form\Element\Text('name');
        $prodName->setLabel('Produktname');
        $this->add($prodName);

        $group = new \Zend\Form\Element\Select('group');
        $group->setLabel('Produktgruppe');
        $group->setValueOptions($this->productGroups);
        $group->setEmptyOption('bitte wählen');
        $this->add($group);

        $preis = new \Zend\Form\Element\Text('price');
        $preis->setLabel('Preis zus.');
        $this->add($preis);

        $descrip = new \Zend\Form\Element\Textarea('description');
        $descrip->setLabel('Beschreibung (intern)');
        $this->add($descrip);

//        $shipment = new \Zend\Form\Element\Select('shipment');
//        $shipment->setLabel('Versandart');
//        $shipment->setValueOptions($this->shipments);
//        $this->add($shipment);
    }
    
    public function __construct($name = 'product') {
        parent::__construct($name);
    }

    
    
    public function getInputFilterSpecification() {
        return array(
            'id' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Digits',
                    ),
                ),
            ),
            'product_nr' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                    ),
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 45,
                        ),
                    ),
                ),
            ),
            'name' => array(
                'required' => TRUE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                    ),
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 45,
                        ),
                    ),
                ),
            ),
            'group' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array_keys($this->productGroups),
                        ),
                    ),
                ),
            ),
            'price' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'break_chain_on_failure' => TRUE,
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Dieses Feld darf nicht leer bleiben!',
                            ),
                        ),
                    ),
                    new \AmandiaES\Validator\Float(),
                ),
            ),
//            'supplier' => array(
//                'required' => FALSE,
//                'filters' => array(
//                    array('name' => 'StripTags'),
//                    array('name' => 'StringTrim'),
//                ),
//                'validators' => array(
//                    array(
//                        'name' => 'InArray',
//                        'options' => array(
//                            'haystack' => array_keys($this->suppliers),
//                        ),
//                    ),
//                ),
//            ),
            'description' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => 255,
                            'messages' => array(
                                \Zend\Validator\StringLength::TOO_LONG => 'Maximal %max% Zeichen!',
                            ),
                        ),
                    ),
                ),
            ),
//            'shipment' => array(
//                'required' => FALSE,
//                'filters' => array(
//                    array('name' => 'StripTags'),
//                    array('name' => 'StringTrim'),
//                ),
//                'validators' => array(
//                    array(
//                        'name' => 'InArray',
//                        'options' => array(
//                            'haystack' => array_keys($this->shipments),
//                        ),
//                    ),
//                ),
//            ),
        );
    }

//    public function setSuppliers($suppliers) {
//        $this->suppliers = $suppliers;
//    }

    public function setProductGroups($productGroups) {
        $this->productGroups = $productGroups;
    }
    
//    public function setShipments($shipments) {
//        $this->shipments = $shipments;
//    }


}

?>
