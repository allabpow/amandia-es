<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Form\Filter;

/**
 * Description of ProductTextFilter
 *
 * @author bitkorn
 */
class ProductTextFilter extends \Zend\InputFilter\InputFilter {

    function __construct() {
        $textDisplayName = new \Zend\InputFilter\Input('text_display_name');
        $textDisplayName->setRequired(FALSE);
        $textDisplayName->getFilterChain()->attachByName('StringTrim');
        $textDisplayName->getValidatorChain()->attachByName('StringLength', array(
            'max' => 500, // database=65500
            'messages' => array(
                \Zend\Validator\StringLength::TOO_LONG => 'max. %max% Zeichen',
            ),
        ));
        $this->add($textDisplayName);

        $textShort = new \Zend\InputFilter\Input('text_short');
        $textShort->setRequired(FALSE);
        $textShort->getFilterChain()->attachByName('StringTrim');
        $textShort->getValidatorChain()->attachByName('StringLength', array(
            'max' => 65500,
            'messages' => array(
                \Zend\Validator\StringLength::TOO_LONG => 'max. %max% Zeichen',
            ),
        ));
        $this->add($textShort);

        $textLong = new \Zend\InputFilter\Input('text_long');
        $textLong->setRequired(FALSE);
        $textLong->getFilterChain()->attachByName('StringTrim');
        $textLong->getValidatorChain()->attachByName('StringLength', array(
            'max' => 65500,
            'messages' => array(
                \Zend\Validator\StringLength::TOO_LONG => 'max. %max% Zeichen',
            ),
        ));
        $this->add($textLong);
    }

}
