<?php

namespace AmandiaES\Form;


/**
 * SearchFactoryOrderForm
 *
 * @author allapow
 */
class SearchFactoryOrderForm extends \Zend\Form\Form implements \Zend\InputFilter\InputFilterProviderInterface {

    /**
     *
     * @var \AmandiaES\Table\CostCenterTable
     */
    private $costCenterTable;
    
    private $costCenters;

    public function init() {
//        parent::init();
        $this->setAttribute('method', 'get');

        $this->costCenters = $this->costCenterTable->getCostCentersIdNameAssocc();

        $orderNr = new \Zend\Form\Element\Text('factory_article_nr');
        $orderNr->setAttribute('class', 'input-medium search-query');
        $orderNr->setLabel('B-Artikel-Nr.');
        $this->add($orderNr);

        $employeeId = new \Zend\Form\Element\Text('employee_id');
        $employeeId->setAttribute('class', 'input-medium search-query');
        $employeeId->setLabel('Mitarbeiter-ID');
        $this->add($employeeId);

        $costCenter = new \Zend\Form\Element\Select('cost_center');
        $costCenter->setLabel('Kostenstelle ???');
        $costCenter->setValueOptions($this->costCenters);
        $costCenter->setEmptyOption('bitte wählen');
        $this->add($costCenter);

        $datetimeCreate = new \Zend\Form\Element\Text('datetime_creation');
        $datetimeCreate->setAttribute('class', 'input-medium search-query datefield');
        $datetimeCreate->setLabel('Datum erstellt');
        $this->add($datetimeCreate);

        $datetimeFinish = new \Zend\Form\Element\Text('datetime_finish');
        $datetimeFinish->setAttribute('class', 'input-medium search-query datefield');
        $datetimeFinish->setLabel('Datum erledigt');
        $this->add($datetimeFinish);

        $finished = new \Zend\Form\Element\Radio('finished');
        $finished->setValueOptions(array(
            1 => 'nur offene Aufträge',
            2 => 'nur fertige Aufträge',
            3 => 'alle Aufträge',
        ));
        $this->add($finished);

        $search = new \Zend\Form\Element\Submit('submit');
        $search->setAttributes(array(
            'value' => 'suchen',
            'class' => 'btn',
        ));
        $this->add($search);
    }

    public function __construct() {
        parent::__construct();
    }

    public function getInputFilterSpecification() {
        return array(
            'factory_article_nr' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'employee_id' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'Digits'),
                ),
            ),
            'cost_center' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'Digits'),
                ),
            ),
            'datetime_creation' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'datetime_finish' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'finished' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'Digits'),
                ),
            ),
        );
    }

    public function setCostCenterTable(\AmandiaES\Table\CostCenterTable $costCenterTable) {
        $this->costCenterTable = $costCenterTable;
    }

}

?>
