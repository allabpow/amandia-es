<?php
namespace AmandiaES\Form;


/**
 * MaterialForm
 *
 * @author allapow
 */
class MaterialForm extends \Zend\Form\Form {
    
    /**
     *
     * @var \AmandiaES\Table\SupplierTable
     */
    private $supplierTable;
    
    /**
     *
     * @var \AmandiaES\Table\MaterialGroupTable
     */
    private $materialGroupTable;
    
    /**
     *
     * @var \AmandiaES\Table\SymbolGeomTable
     */
    private $symbolGeomTable;
    
    /**
     *
     * @var \AmandiaES\Table\SymbolWeightTable
     */
    private $symbolWeightTable;
    
    
    
    public function init() {
//        parent::init();
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal'); // Bootstrap 2.3.2

        $materialFieldset = new \AmandiaES\Form\Fieldset\MaterialFieldset();
        $materialFieldset->setMaterialGroups($this->materialGroupTable->getMaterialGroupsIdNameAssocc());
        $materialFieldset->setSuppliers($this->supplierTable->getSuppliersIdNameAssocc());
        $materialFieldset->setMeasureSymbolGeoms($this->symbolGeomTable->getSymbolIdNameAssocc());
        $materialFieldset->setMeasureSymbolWeights($this->symbolWeightTable->getSymbolIdNameAssocc());
        $materialFieldset->init();
        $this->add($materialFieldset);
        
        $submit = new \Zend\Form\Element\Submit('submit');
        $submit->setAttributes(array(
            'value' => 'speichern',
            'class' => 'btn',
        ));
        $this->add($submit);
    }

    public function __construct() {
        parent::__construct();
    }
    
    public function setMaterialGroupTable(\AmandiaES\Table\MaterialGroupTable $materialGroupTable) {
        $this->materialGroupTable = $materialGroupTable;
    }

    public function setSupplierTable(\AmandiaES\Table\SupplierTable $supplierTable) {
        $this->supplierTable = $supplierTable;
    }
    
    public function setSymbolGeomTable(\AmandiaES\Table\SymbolGeomTable $symbolGeomTable) {
        $this->symbolGeomTable = $symbolGeomTable;
    }

    public function setSymbolWeightTable(\AmandiaES\Table\SymbolWeightTable $symbolWeightTable) {
        $this->symbolWeightTable = $symbolWeightTable;
    }



    
}

?>
