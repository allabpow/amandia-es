<?php
namespace AmandiaES\Form;

use Zend\Form\Fieldset;

/**
 * SearchArticleForm
 *
 * @author allapow
 */
class SearchArticleForm extends \Zend\Form\Form implements \Zend\InputFilter\InputFilterProviderInterface {
    
    /**
     *
     * @var \AmandiaES\Table\ArticleGroupTable
     */
    private $articleGroupTable;
    
    /**
     *
     * @var \AmandiaES\Table\SupplierTable
     */
    private $supplierTable;
    
    private $groups;
    private $suppliers;
    
    
    public function init() {
        $this->setAttribute('method', 'get');
        
        $this->groups = $this->articleGroupTable->getArticleGroupsIdNameAssocc();
        $this->suppliers = $this->supplierTable->getSuppliersIdNameAssocc();

        $name = new \Zend\Form\Element\Text('name');
        $name->setAttribute('class', 'input-medium search-query');
        $name->setLabel('Produktname');
        $this->add($name);

        $group = new \Zend\Form\Element\Select('group');
        $group->setLabel('Artikel-Gruppe');
        $group->setValueOptions($this->groups);
        $group->setEmptyOption('bitte wählen');
        $this->add($group);

        $supplier = new \Zend\Form\Element\Select('supplier');
        $supplier->setLabel('Lieferant');
        $supplier->setValueOptions($this->suppliers);
        $supplier->setEmptyOption('bitte wählen');
        $this->add($supplier);
        
        $articleNr = new \Zend\Form\Element\Text('article_nr');
        $articleNr->setAttribute('class', 'input-medium search-query');
        $articleNr->setLabel('Artikel-Nr.');
        $this->add($articleNr);
        
        $articleNrExt = new \Zend\Form\Element\Text('article_nr_extern');
        $articleNrExt->setAttribute('class', 'input-medium search-query');
        $articleNrExt->setLabel('Artikel-Nr. extern');
        $this->add($articleNrExt);
        
        $search = new \Zend\Form\Element\Submit('submit');
        $search->setAttributes(array(
            'value' => 'suchen',
            'class' => 'btn',
        ));
        $this->add($search);
    }

    public function __construct() {
        parent::__construct();
    }
    
    public function getInputFilterSpecification() {
        return array(
            'name' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'group' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'Digits'),
                ),
            ),
            'supplier' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'Digits'),
                ),
            ),
            'article_nr' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'article_nr_extern' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
        );
    }
    
    public function setArticleGroupTable(\AmandiaES\Table\ArticleGroupTable $articleGroupTable) {
        $this->articleGroupTable = $articleGroupTable;
    }

    public function setSupplierTable(\AmandiaES\Table\SupplierTable $supplierTable) {
        $this->supplierTable = $supplierTable;
    }




}

?>
