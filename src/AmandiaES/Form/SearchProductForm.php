<?php
namespace AmandiaES\Form;

use Zend\Form\Fieldset;

/**
 * SearchProductForm
 *
 * @author allapow
 */
class SearchProductForm extends \Zend\Form\Form implements \Zend\InputFilter\InputFilterProviderInterface {
    
    /**
     *
     * @var \AmandiaES\Table\ProductGroupTable
     */
    private $productGroupTable;
    
    public function init() {
        $this->setAttribute('method', 'get');

        $name = new \Zend\Form\Element\Text('name');
        $name->setAttribute('class', 'input-medium search-query');
//        $name->setAttribute('id', 'name'); // for AJAX call
        $name->setLabel('Produktname');
        $this->add($name);

        $group = new \Zend\Form\Element\Select('group');
//        $group->setAttribute('id', 'group'); // for AJAX call
        $group->setLabel('Prod.-Gruppe');
        $group->setValueOptions($this->productGroupTable->getProductGroupsIdNameAssocc());
        $group->setEmptyOption('bitte wählen');
        $this->add($group);
        
        $search = new \Zend\Form\Element\Submit('submit');
        $search->setAttributes(array(
            'value' => 'suchen',
            'class' => 'btn',
        ));
        $this->add($search);
    }

    public function __construct() {
        parent::__construct();
    }
    
    public function getInputFilterSpecification() {
        return array(
            'name' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'group' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'Digits'),
                ),
            ),
            'supplier' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'Digits'),
                ),
            ),
            'in_shop' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'Digits'),
                ),
            ),
        );
    }
    
    public function setProductGroupTable(\AmandiaES\Table\ProductGroupTable $productGroupTable) {
        $this->productGroupTable = $productGroupTable;
    }

}

?>
