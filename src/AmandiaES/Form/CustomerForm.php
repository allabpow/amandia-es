<?php
namespace AmandiaES\Form;


/**
 * CustomerForm
 *
 * @author allapow
 */
class CustomerForm extends \Zend\Form\Form {
    
    /**
     *
     * @var \AmandiaES\Table\CustomerGroupTable 
     */
    private $customerGroupTable;
    
    /**
     *
     * @var \AmandiaES\Table\CountryTable 
     */
    private $countryTable;
    
    public function init() {
        parent::init();
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal'); // Bootstrap 2.3.2

        $customerFieldset = new \AmandiaES\Form\Fieldset\CustomerFieldset();
        $customerFieldset->setCustomerGroups($this->customerGroupTable->getCustomerGroupsIdNameAssocc());
        $customerFieldset->setCountries($this->countryTable->getCountriesIdNameAssocc());
        $customerFieldset->init();
        $this->add($customerFieldset);
        
        $submit = new \Zend\Form\Element\Submit('submit');
        $submit->setAttributes(array(
            'value' => 'speichern',
            'class' => 'btn',
        ));
        $this->add($submit);
    }

    public function __construct() {
        parent::__construct();
    }
    
    public function setCustomerGroupTable(\AmandiaES\Table\CustomerGroupTable $customerGroupTable) {
        $this->customerGroupTable = $customerGroupTable;
    }

    public function setCountryTable(\AmandiaES\Table\CountryTable $countryTable) {
        $this->countryTable = $countryTable;
    }

}

?>
