<?php
namespace AmandiaES\Form;

use Zend\Form\Fieldset;

/**
 * SearchOrderForm
 *
 * @author allapow
 */
class SearchOrderForm extends \Zend\Form\Form implements \Zend\InputFilter\InputFilterProviderInterface {
    
    /**
     *
     * @var \AmandiaES\Table\CostCenterTable
     */
    private $costCenterTable;
    
    private $costCenters;
    
    
    public function init() {
//        parent::init();
        $this->setAttribute('method', 'get');
        
        $this->costCenters = $this->costCenterTable->getCostCentersIdNameAssocc();

        $orderNr = new \Zend\Form\Element\Text('order_nr');
        $orderNr->setAttribute('class', 'input-medium search-query');
        $orderNr->setLabel('Auftrags-Nr.');
        $this->add($orderNr);

        $custNr = new \Zend\Form\Element\Text('customer_nr');
        $custNr->setAttribute('class', 'input-medium search-query');
        $custNr->setLabel('Kunden-Nr.');
        $this->add($custNr);

        $userCreate = new \Zend\Form\Element\Text('user_create');
        $userCreate->setAttribute('class', 'input-medium search-query');
        $userCreate->setLabel('Mitarbeiter-ID');
        $this->add($userCreate);

        $costCenter = new \Zend\Form\Element\Select('cost_center');
        $costCenter->setLabel('Kostenstelle');
        $costCenter->setValueOptions($this->costCenters);
        $costCenter->setEmptyOption('bitte wählen');
        $this->add($costCenter);
        
        $datetimeFrom = new \Zend\Form\Element\Text('datetime_from');
        $datetimeFrom->setAttribute('class', 'input-medium search-query datefield');
        $datetimeFrom->setLabel('Datum von');
        $this->add($datetimeFrom);
        
        $datetimeTo = new \Zend\Form\Element\Text('datetime_to');
        $datetimeTo->setAttribute('class', 'input-medium search-query datefield');
        $datetimeTo->setLabel('Datum bis');
        $this->add($datetimeTo);
        
        $finished = new \Zend\Form\Element\Radio('finished');
        $finished->setValueOptions(array(
            1 => 'nur offene Aufträge',
            2 => 'nur fertige Aufträge',
            3 => 'alle Aufträge',
        ));
        $this->add($finished);
        
        $search = new \Zend\Form\Element\Submit('submit');
        $search->setAttributes(array(
            'value' => 'suchen',
            'class' => 'btn',
        ));
        $this->add($search);
    }

    public function __construct() {
        parent::__construct();
    }

    public function getInputFilterSpecification() {
        return array(
            'order_nr' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'customer_nr' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'user_create' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'Digits'),
                ),
            ),
            'cost_center' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'Digits'),
                ),
            ),
            'datetime_from' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'datetime_to' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            'finished' => array(
                'required' => FALSE,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'Digits'),
                ),
            ),
        );
    }
    
    public function setCostCenterTable(\AmandiaES\Table\CostCenterTable $costCenterTable) {
        $this->costCenterTable = $costCenterTable;
    }



}

?>
