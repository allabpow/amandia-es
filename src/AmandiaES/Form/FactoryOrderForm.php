<?php
namespace AmandiaES\Form;


/**
 * ArticleForm
 *
 * @author allapow
 */
class FactoryOrderForm extends \Zend\Form\Form {
        
    /**
     *
     * @var \AmandiaES\Table\LocationTable 
     */
    private $locationTable;
    
    public function init() {
//        parent::init();
        $this->setAttribute('method', 'post');

        $foFieldset = new \AmandiaES\Form\Fieldset\FactoryOrderFieldset();
        $foFieldset->setLocations($this->locationTable->getLocationsIdNameAssocc());
        $foFieldset->init();
        $this->add($foFieldset);
        
        $submit = new \Zend\Form\Element\Button('submit');
        $submit->setLabel('speichern');
        $submit->setAttributes(array(
            'type' => 'submit',
            'class' => 'small',
        ));
        $this->add($submit);
        
        $finish = new \Zend\Form\Element\Button('finish');
        $finish->setLabel('fertig/abschließen');
        $finish->setAttributes(array(
            'type' => 'submit',
            'class' => 'small',
        ));
        $this->add($finish);
        
        $cancel = new \Zend\Form\Element\Button('cancel');
        $cancel->setLabel('abbrechen');
        $cancel->setAttributes(array(
            'type' => 'submit',
            'class' => 'small',
        ));
        $this->add($cancel);
    }

    public function __construct() {
        parent::__construct();
    }
    
    public function setLocationTable(\AmandiaES\Table\LocationTable $locationTable) {
        $this->locationTable = $locationTable;
    }

}

?>
