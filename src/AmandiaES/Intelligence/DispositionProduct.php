<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */
namespace AmandiaES\Intelligence;
/**
 * DispositionProduct
 * ...wegen der CustomOrder gibt es dann noch DispositionCustomOrder
 *
 * @author bitkorn
 */
class DispositionProduct {
    
    /**
     *
     * @var \AmandiaES\Table\ProductTable
     */
    private $productTable;

    /**
     *
     * @var \AmandiaES\Table\ArticleTable 
     */
    private $articleTable;

    /**
     *
     * @var \AmandiaES\Table\MaterialTable 
     */
    private $materialTable;

    /**
     *
     * @var \AmandiaES\Table\FactoryArticleTable
     */
    private $factoryArticleTable;
    
}
