<?php
/**
 * Description of CostCenterName
 *
 * @author allapow
 */

namespace AmandiaES\View\Helper;

use Zend\View\Helper\AbstractHelper;

class CostCenterName extends AbstractHelper {
    
    private $costcenters;
    
    public function __invoke($id) {
        return $this->costcenters[$id];
    }
    
    public function setCostcenters($costcenters) {
        $this->costcenters = $costcenters;
    }

}

?>
