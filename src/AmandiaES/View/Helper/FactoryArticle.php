<?php

namespace AmandiaES\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Description of FactoryArticle
 *
 * @author allapow
 */
class FactoryArticle extends AbstractHelper {

    /**
     *
     * @var \AmandiaES\Table\FactoryArticleTable 
     */
    private $factoryArticleTable;

    /**
     *
     * @var \AmandiaES\Table\FactoryArticleGroupTable 
     */
    private $factoryArticleGroupTable;

    public function __invoke($id, $key) {
        $factoryArticle = $this->factoryArticleTable->getFactoryArticleById($id);
        switch ($key) {
            case 'groupName':
                $factoryArticleGroups = $this->factoryArticleGroupTable->getFactoryArticleGroupsIdNameAssocc();
                return $factoryArticleGroups[$factoryArticle['group']];
            case 'nr':
                return $factoryArticle['factory_article_nr'];
            case 'name':
                return $factoryArticle['name'];
        }
    }

    public function setFactoryArticleTable(\AmandiaES\Table\FactoryArticleTable $factoryArticleTable) {
        $this->factoryArticleTable = $factoryArticleTable;
    }

    public function setFactoryArticleGroupTable(\AmandiaES\Table\FactoryArticleGroupTable $factoryArticleGroupTable) {
        $this->factoryArticleGroupTable = $factoryArticleGroupTable;
    }

}

?>
