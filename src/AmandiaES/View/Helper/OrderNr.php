<?php
/**
 * Description of ProductName
 *
 * @author allapow
 */

namespace AmandiaES\View\Helper;

use Zend\View\Helper\AbstractHelper;

class OrderNr extends AbstractHelper {
    
    /**
     *
     * @var \AmandiaES\Table\OrderTable 
     */
    private $orderTable;
    
    public function __invoke($id) {
        $id = (int)$id;
        $order = $this->orderTable->getOrderById($id);
        if(isset($order['order_nr']) && !empty($order['order_nr'])) {
            return $order['order_nr'];
        }
        return 'Order.-ID: ' . $id;
    }
    
    public function setOrderTable(\AmandiaES\Table\OrderTable $orderTable) {
        $this->orderTable = $orderTable;
    }

}

?>
