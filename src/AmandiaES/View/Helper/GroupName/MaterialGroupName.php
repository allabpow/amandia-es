<?php
/**
 * Description of MaterialGroupName
 *
 * @author allapow
 */

namespace AmandiaES\View\Helper\GroupName;

use Zend\View\Helper\AbstractHelper;

class MaterialGroupName extends AbstractHelper {
    
    private $materialGroups;
    
    public function __invoke($id) {
        return $this->materialGroups[$id];
    }
    
    public function setMaterialGroups($materialGroups) {
        $this->materialGroups = $materialGroups;
    }

}

?>
