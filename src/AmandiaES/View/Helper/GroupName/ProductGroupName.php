<?php
/**
 * Description of ProductGroupName
 *
 * @author allapow
 */

namespace AmandiaES\View\Helper\GroupName;

use Zend\View\Helper\AbstractHelper;

class ProductGroupName extends AbstractHelper {
    
    private $productGroups;
    
    public function __invoke($id) {
        return $this->productGroups[$id];
    }
    
    public function setProductGroups($productGroups) {
        $this->productGroups = $productGroups;
    }

}

?>
