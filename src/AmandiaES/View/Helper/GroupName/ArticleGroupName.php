<?php
/**
 * Description of ArticleGroupName
 *
 * @author allapow
 */

namespace AmandiaES\View\Helper\GroupName;

use Zend\View\Helper\AbstractHelper;

class ArticleGroupName extends AbstractHelper {
    
    private $articleGroups;
    
    public function __invoke($id) {
        return $this->articleGroups[$id];
    }
    
    public function setArticleGroups($articleGroups) {
        $this->articleGroups = $articleGroups;
    }


}

?>
