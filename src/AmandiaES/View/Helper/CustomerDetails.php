<?php

/**
 * Description of CustomerNr
 *
 * @author allapow
 */

namespace AmandiaES\View\Helper;

use Zend\View\Helper\AbstractHelper;

class CustomerDetails extends AbstractHelper {

    /**
     *
     * @var \AmandiaES\Table\CustomerTable 
     */
    private $customerTable;

    /**
     *
     * @var \AmandiaES\Table\CustomerGroupTable 
     */
    private $customerGroupTable;

    /**
     *
     * @var \AmandiaES\Table\CountryTable 
     */
    private $countryTable;

    public function __invoke($id, $extend = 0) {
        $html = '';
        $customer = $this->customerTable->getCustomerById($id);
        if ($customer) {
            $customerGroups = $this->customerGroupTable->getCustomerGroupsIdNameAssocc();
            $countries = $this->countryTable->getCountriesIdNameAssocc();
            $html .= '<table cellspacing="0" cellpadding="0" class="tight"><tbody>';
            $html .= '<tr><td>Kd.-Nr. (Gruppe)</td><td>' . $customer['customer_nr'] . ' (' . $customerGroups[$customer['group']] . ')</td></tr>';
            $html .= '<tr><td>Name</td><td>' . $customer['name_first'] . ' ' . $customer['name_last'] . '</td></tr>';
            $html .= '<tr><td>Firma</td><td>' . $customer['name_company'] . '</td></tr>';
            if (1 == $extend) {
                $html .= '<tr><td>Straße - Nr.</td><td>' . $customer['str'] . ' ' . $customer['str_nr'] . '</td></tr>';
                $html .= '<tr><td>PLZ - Stadt</td><td>' . $customer['zip'] . ' ' . $customer['city'] . '</td></tr>';
                $html .= '<tr><td>Land</td><td>';
                if (isset($customer['country'])) {
                    $html .= $countries[$customer['country']];
                }
                $html .= '</td></tr>';
                $html .= '<tr><td>Telefon</td><td>' . $customer['tel'] . '</td></tr>';
                $html .= '<tr><td>Mobile</td><td>' . $customer['tel_mobile'] . '</td></tr>';
                $html .= '<tr><td>Email</td><td><a href="mailto:' . $customer['email'] . '">' . $customer['email'] . '</a></td></tr>';
//            $html .= '<tr><td></td><td>' . $customer[''] . '</td></tr>';
                if (2 == $extend) {
                    $html .= '<tr><td colspan="2"></td></tr>';
                    $html .= '<tr><td>Name</td><td>' . $customer['sec_name_first'] . ' ' . $customer['sec_name_last'] . '</td></tr>';
                    $html .= '<tr><td>Firma</td><td>' . $customer['sec_name_company'] . '</td></tr>';
                    $html .= '<tr><td>Straße - Nr.</td><td>' . $customer['sec_str'] . ' ' . $customer['sec_str_nr'] . '</td></tr>';
                    $html .= '<tr><td>PLZ - Stadt</td><td>' . $customer['sec_zip'] . ' ' . $customer['sec_city'] . '</td></tr>';
                    $html .= '<tr><td>Land</td><td>';
                    if (isset($customer['sec_country'])) {
                        $html .= $countries[$customer['sec_country']];
                    }
                    $html .= '</td></tr>';
                    $html .= '<tr><td>Fax</td><td>' . $customer['tel_fax'] . '</td></tr>';
//                $html .= '<tr><td></td><td>' . $customer[''] . '</td></tr>';
                }
            }
        }
        $html .= '</tbody></table>';
        return $html;
    }

    public function setCustomerTable(\AmandiaES\Table\CustomerTable $customerTable) {
        $this->customerTable = $customerTable;
    }

    public function setCustomerGroupTable(\AmandiaES\Table\CustomerGroupTable $customerGroupTable) {
        $this->customerGroupTable = $customerGroupTable;
    }

    public function setCountryTable(\AmandiaES\Table\CountryTable $countryTable) {
        $this->countryTable = $countryTable;
    }

}

?>
