<?php
/**
 * Description of ListMaterials
 *
 * @author allapow
 */

namespace AmandiaES\View\Helper;

use Zend\View\Helper\AbstractHelper;

class SymbolGeom extends AbstractHelper {
    
    private $symbolsGeom;
    
    public function __invoke($id) {
        return $this->symbolsGeom[$id];
    }
    
    public function setSymbolsGeom($symbolsGeom) {
        $this->symbolsGeom = $symbolsGeom;
    }
    
}

?>
