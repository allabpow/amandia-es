<?php
/**
 * Description of SupplierName
 *
 * @author allapow
 */

namespace AmandiaES\View\Helper;

use Zend\View\Helper\AbstractHelper;

class LocationName extends AbstractHelper {
    
    private $locations;
    
    public function __invoke($id) {
        return isset($this->locations[$id]) ? $this->locations[$id] : 'undifined';
    }
    
    public function setLocations($locations) {
        $this->locations = $locations;
    }


}

?>
