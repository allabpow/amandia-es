<?php
/**
 * Description of ShopTranslate
 *
 * @author allapow
 */

namespace AmandiaES\View\Helper;

use Zend\View\Helper\AbstractHelper;

class AesTranslate extends AbstractHelper {
    
    /**
     *
     * @var \AmandiaES\Service\Translator 
     */
    private $translator;
    
    /**
     * 
     * @param string $key
     * @return string
     */
    public function __invoke($key) {
        return $this->translator->translate($key);
    }
    
    public function setTranslator(\AmandiaES\Service\Translator $translator) {
        $this->translator = $translator;
    }

}

?>
