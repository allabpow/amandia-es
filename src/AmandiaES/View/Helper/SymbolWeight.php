<?php
/**
 * Description of ListMaterials
 *
 * @author allapow
 */

namespace AmandiaES\View\Helper;

use Zend\View\Helper\AbstractHelper;

class SymbolWeight extends AbstractHelper {
    
    private $symbolsWeight;
    
    public function __invoke($id) {
        return $this->symbolsWeight[$id];
    }
    
    public function setSymbolsWeight($symbolsWeight) {
        $this->symbolsWeight = $symbolsWeight;
    }


}

?>
