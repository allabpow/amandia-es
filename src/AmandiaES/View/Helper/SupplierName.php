<?php
/**
 * Description of SupplierName
 *
 * @author allapow
 */

namespace AmandiaES\View\Helper;

use Zend\View\Helper\AbstractHelper;

class SupplierName extends AbstractHelper {
    
    private $suppliers;
    
    public function __invoke($id) {
        return $this->suppliers[$id];
    }
    
    public function setSuppliers($suppliers) {
        $this->suppliers = $suppliers;
    }


}

?>
