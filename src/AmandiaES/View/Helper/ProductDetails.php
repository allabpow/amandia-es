<?php
/**
 * Description of ProductDetails
 *
 * @author allapow
 */

namespace AmandiaES\View\Helper;

use Zend\View\Helper\AbstractHelper;

class ProductDetails extends AbstractHelper {
    
    /**
     *
     * @var \AmandiaES\Table\ProductTable 
     */
    private $productTable;
    
    public function __invoke($id, $key) {
        $product = $this->productTable->getProductById($id);
        if(!$product) {
            return FALSE;
        }
        switch($key) {
            case 'nr':
                return $product['product_nr'];
            case 'name':
                return $product['name'];
        }
    }
    
    public function setProductTable(\AmandiaES\Table\ProductTable $productTable) {
        $this->productTable = $productTable;
    }


}

?>
