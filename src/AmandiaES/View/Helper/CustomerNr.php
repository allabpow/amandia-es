<?php
/**
 * Description of CustomerNr
 *
 * @author allapow
 */

namespace AmandiaES\View\Helper;

use Zend\View\Helper\AbstractHelper;

class CustomerNr extends AbstractHelper {
    
    /**
     *
     * @var \AmandiaES\Table\CustomerTable 
     */
    private $customerTable;
    
    public function __invoke($id) {
        $customer = $this->customerTable->getCustomerById($id);
        if(isset($customer['customer_nr'])) {
            return $customer['customer_nr'];
        }
        return 'no customer';
    }
    
    public function setCustomerTable(\AmandiaES\Table\CustomerTable $customerTable) {
        $this->customerTable = $customerTable;
    }

}

?>
