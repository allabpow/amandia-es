<?php
/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */


namespace AmandiaES\Table;

use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\TableGateway\AbstractTableGateway;

/**
 * Description of CostCenterTable
 *
 * @author allapow
 */
class CostCenterTable extends AbstractTableGateway implements AdapterAwareInterface {

    protected $table = 'aes_cost_center';

    public function setDbAdapter(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet();
        $this->initialize();
    }

    /**
     * 
     * @param string $group
     * @return array empty or full Array with Key=id Value=name
     */
    public function getCostCentersIdNameAssocc($order = 'id') {
        $select = $this->sql->select();
        $select->order($order);
        $resultset = $this->executeSelect($select);
        $resultArray = $resultset->toArray();
        $returnArray = array();
        foreach ($resultArray as $costCenter) {
            $returnArray[$costCenter['id']] = $costCenter['name'];
        }
        return $returnArray;
    }
    
}

?>
