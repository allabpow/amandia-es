<?php
/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */


namespace AmandiaES\Table;

use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\TableGateway\AbstractTableGateway;

/**
 * Description of SupplierTable
 *
 * @author allapow
 */
class SupplierTable extends AbstractTableGateway implements AdapterAwareInterface {

    protected $table = 'aes_supplier';

    public function setDbAdapter(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet();
        $this->initialize();
    }

    /**
     * 
     * @param string $order
     */
    public function getSuppliers($order = 'id') {
        $select = $this->sql->select();
        $select->order($order);
        $hResultset = $this->executeSelect($select);
        return $hResultset->toArray();
    }

    /**
     * 
     * @param string $order
     */
    public function getSuppliersIdNameAssocc($order = 'id') {
        $select = $this->sql->select();
        $select->columns(array('id', 'name'));
        $select->order($order);
        $resultset = $this->executeSelect($select);
        $resultArr = $resultset->toArray();
        $returnArray = array();
        foreach ($resultArr as $row) {
            $returnArray[$row['id']] = $row['name'];
        }
        return $returnArray;
    }
    
}

?>
