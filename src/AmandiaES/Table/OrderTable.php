<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Table;

use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\TableGateway\AbstractTableGateway;

/**
 * Description of OrderTable
 *
 * @author allapow
 */
class OrderTable extends AbstractTableGateway implements AdapterAwareInterface {

    /**
     *
     * @var \Zend\Db\Adapter\Adapter 
     */
    protected $adapter;
    protected $table = 'aes_order';

    public function setDbAdapter(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet();
        $this->initialize();
    }

    /**
     * 
     * @param int $id
     * @return array|bool Result row as Array or FALSE if no result exist.
     */
    public function getOrderById($id) {
        $select = $this->sql->select();
        $select->where(array(
            'id' => (int) $id,
        ));
        $resultset = $this->executeSelect($select);
        $resultArray = $resultset->toArray();
        if (isset($resultArray[0])) {
            return $resultArray[0];
        }
        return FALSE;
    }

    public function getOrder($id) {
        $select = $this->sql->select();
        $select->where(array(
            'id' => (int) $id,
        ));
        $resultset = $this->executeSelect($select);
        $resultArray = $resultset->toArray();
        if (isset($resultArray[0])) {
            return array('order' => $resultArray[0]);
        }
        return FALSE;
    }

    /**
     * 
     * @param array $data
     * @return int|mixed
     */
    public function saveOrder(array $data) {
        $dataArray = array(
            'order_nr' => $data['order_nr'],
            'user_create' => $data['user_create'],
            'user_last_editor' => $data['user_last_editor'],
            'cost_center' => $data['cost_center'],
            'shipment' => $data['shipment'],
            'description' => $data['description'],
        );
        if (isset($data['id']) && $data['id']) {
            $update = $this->sql->update();
            $dataArray['datetime_last_change'] = date('Y-m-d H:i:s');
            $update->set($dataArray);
            $update->where(array(
                'id' => $data['id'],
            ));
            $result = $this->updateWith($update);
            if ($result == 1 || $result == 0) {
                return (int) $data['id'];
            }
            return FALSE;
        } else {
            $insert = $this->sql->insert();
            $dataArray['datetime_creation'] = date('Y-m-d H:i:s');
            $insert->values($dataArray);
            $result = $this->executeInsert($insert);
            if ($result) {
                $select = $this->sql->select();
                $select->columns(array(new \Zend\Db\Sql\Expression("LAST_INSERT_ID() as id")));
                $selResult = $this->selectWith($select);
                $selResArray = $selResult->toArray();
                if (isset($selResArray[0]['id'])) {
                    return (int) $selResArray[0]['id'];
                }
                return FALSE;
            }
        }
    }

    public function searchOrder($logger, array $dataSearch, $order = 'order_nr', $onlySelect = FALSE) {
//        $logger->log(\Zend\Log\Logger::DEBUG, 'drin');
        $select = $this->sql->select();
        if (!empty($dataSearch['order_nr'])) {
            $likeOrderNr = new \Zend\Db\Sql\Predicate\Like('order_nr');
            $likeOrderNr->setLike('%' . $dataSearch['order_nr'] . '%');
            $select->where($likeOrderNr);
        }
        if (!empty($dataSearch['customer_nr'])) {
//            $likeCustomerNr = new \Zend\Db\Sql\Predicate\Like('customer_nr');
//            $likeCustomerNr->setLike('%' . $dataSearch['customer_nr'] . '%');
//            $select->where($likeCustomerNr);
            // SQL subquery
            // WHERE customer_id = SELECT id FROM aes_customer WHERE customer_nr LIKE %customer_nr%
            $sqlSub = new \Zend\Db\Sql\Sql($this->adapter);
            $subSelect = $sqlSub->select('aes_customer');
            $likeCustomerNr = new \Zend\Db\Sql\Predicate\Like('customer_nr');
            $likeCustomerNr->setLike('%' . $dataSearch['customer_nr'] . '%');
            $subSelect->columns(array('id'))->where($likeCustomerNr);
//            $subSelect->where($likeCustomerNr);
            $inCustomerId = new \Zend\Db\Sql\Predicate\In('customer_id');
//            $inCustomerId->setValueSet($subSelect);
            $select->where($inCustomerId->setValueSet($subSelect));
        }
        if (!empty($dataSearch['user_create'])) {
            $select->where(array(
                'user_create' => $dataSearch['user_create'],
            ));
        }
        if (!empty($dataSearch['cost_center'])) {
            $select->where(array(
                'cost_center' => $dataSearch['cost_center'],
            ));
        }
        if (!empty($dataSearch['datetime_from'])) {
            $select->where->greaterThan('datetime_creation', $dataSearch['datetime_from']);
        }
        if (!empty($dataSearch['datetime_to'])) {
            $select->where->lessThan('datetime_creation', $dataSearch['datetime_to']);
        }
        if (!empty($dataSearch['finished'])) {
            if ($dataSearch['finished'] == 1) {
                $select->where->isNull('datetime_finish');
            } elseif ($dataSearch['finished'] == 2) {
                $select->where->isNotNull('datetime_finish');
            }
        }

        $select->order($order);

        if (!$onlySelect) {
            $resultset = $this->executeSelect($select);
            $resultArray = $resultset->toArray();
            if (isset($resultArray[0])) {
                return $resultArray;
            }
        } else {
            return $select;
        }
        return FALSE;
    }

    /**
     * 
     * @param int $orderId
     * @param int $productId
     * @param string $operator "minus" | "plus"
     * @return boolean
     */
    public function updateProducts($orderId, $productId, $operator) {
        $order = $this->getOrderById($orderId);
        $productsArr = json_decode($order['products'], TRUE);
        switch ($operator) {
            case 'minus':
                if (array_key_exists($productId, $productsArr)) {
                    $productsArr[$productId] -= 1;
                } else {
                    $productsArr[$productId] = 0; // spart woanders Code
                }
                break;
            case 'plus':
                if (array_key_exists($productId, $productsArr)) {
                    $productsArr[$productId] += 1;
                } else {
                    $productsArr[$productId] = 1;
                }
                break;
            default :
                return FALSE;
        }
        $withoutEmptyVals = array();
        foreach ($productsArr as $key => $val) {
            if ($val > 0) {
                $withoutEmptyVals[$key] = $val;
            }
        }
        $productsJson = json_encode($withoutEmptyVals);
        $update = $this->sql->update();
        $update->set(array(
            'products' => $productsJson,
            'datetime_last_change' => date('Y-m-d H:i:s'),
        ));
        $update->where(array(
            'id' => $orderId,
        ));
        $result = $this->updateWith($update);
        if ($result == 1) {
            return TRUE;
        }
        return FALSE;
    }

    public function updateField($orderId, $fieldName, $fieldValue) {
        $update = $this->sql->update();
        $update->set(array(
            $fieldName => $fieldValue,
            'datetime_last_change' => date('Y-m-d H:i:s'),
        ));
        $update->where(array(
            'id' => $orderId,
        ));
        $result = $this->updateWith($update);
        if ($result == 1 || $result == 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function countProduct($productId) {
        $select = $this->sql->select();
        $select->columns(array(
            new \Zend\Db\Sql\Expression('COUNT(id) as cfo')
        ));
        $select->where(array(
            'order_id' => $orderId
        ));
    }
}

?>
