<?php
/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */


namespace AmandiaES\Table;

use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\TableGateway\AbstractTableGateway;

/**
 * Description of ServiceTable
 *
 * @author allapow
 */
class ServiceTable extends AbstractTableGateway implements AdapterAwareInterface {

    protected $table = 'aes_service';

    public function setDbAdapter(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet();
        $this->initialize();
    }

    /**
     * 
     * @param string $group 
     */
    public function getServicesByGroup($group = 1) {
        $select = $this->sql->select();
        if($group) {
            $select->where(array(
                'group' => $group,
            ));
        }
        $select->order('name');
        $resultset = $this->executeSelect($select);
        return $resultset->toArray();
    }
    
    public function getSelectServicesAll($group = 1, $order = 'id') {
        $select = $this->sql->select();
        $select->where(array(
            'group' => $group,
        ));
        $select->order($order);
        return $select;
    }
    
    /**
     * 
     * @param array $ids array(2,4,6)
     * @return array
     */
    public function getServicesByIds(array $ids) {
        if(count($ids) < 1) {
            return array();
        }
        $select = $this->sql->select();
        $predicateIn = new \Zend\Db\Sql\Predicate\In('id',$ids);
        $select->where($predicateIn);
        $select->order('name');
        $resultset = $this->executeSelect($select);
        return $resultset->toArray();
    }
    
    public function getServiceById($id) {
        $select = $this->sql->select();
        $select->where(array(
            'id' => (int)$id,
        ));
        $resultset = $this->executeSelect($select);
        $resultArray = $resultset->toArray();
        if(isset($resultArray[0])) {
            return $resultArray[0];
        }
        return FALSE;
    }
    
    public function saveService($data) {
        $dataArray = array(
            'service_nr' => $data['service_nr'],
            'name' => $data['name'],
            'group' => $data['group'],
            'price_buy' => $data['price_buy'],
            'price_sell' => $data['price_sell'],
            'description' => $data['description'],
        );
        if(isset($data['id']) && $data['id']) {
            $update = $this->sql->update();
            $update->set($dataArray);
            $update->where(array(
                'id' => $data['id'],
            ));
            $result = $this->updateWith($update);
            if($result == 1 || $result == 0) {
                return (int)$data['id'];
            }
            return $result;
        } else {
            $insert = $this->sql->insert();
            $insert->values($dataArray);
            $result = $this->executeInsert($insert);
            if($result) {
                $select = $this->sql->select();
                $select->columns(array(new \Zend\Db\Sql\Expression("LAST_INSERT_ID() as id")));
                $selResult = $this->selectWith($select);
                $selResArray = $selResult->toArray();
                if(isset($selResArray[0]['id'])) {
                    return (int)$selResArray[0]['id'];
                }
                return $selResArray;
            }
        }
    }
    
    public function searchService($logger, array $dataSearch, $order = 'service_nr DESC', $onlySelect = FALSE) {
//        $logger->log(\Zend\Log\Logger::DEBUG, 'drin');
        $select = $this->sql->select();
        if(!empty($dataSearch['name'])) {
            $likeArticleName = new \Zend\Db\Sql\Predicate\Like('name');
            $likeArticleName->setLike('%' . $dataSearch['name'] . '%');
            $select->where($likeArticleName);
        }
        if(!empty($dataSearch['group'])) {
            $select->where(array(
                'group' => $dataSearch['group'],
            ));
        }
        if(!empty($dataSearch['service_nr'])) {
            $likeServiceNr = new \Zend\Db\Sql\Predicate\Like('service_nr');
            $likeServiceNr->setLike('%' . $dataSearch['service_nr'] . '%');
            $select->where($likeServiceNr);
        }
        
        $select->order($order);
        
        if(!$onlySelect) {
            $resultset = $this->executeSelect($select);
            $resultArray = $resultset->toArray();
            if(isset($resultArray[0])) {
                return $resultArray;
            }
        } else {
            return $select;
        }
        return FALSE;
    }
}

?>
