<?php
/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */


namespace AmandiaES\Table;

use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\TableGateway\AbstractTableGateway;

/**
 * 
 *
 * @author allapow
 */
class DispositionFactoryArticleTable extends AbstractTableGateway implements AdapterAwareInterface {

    protected $table = 'aes_disposition_factory_article';

    public function setDbAdapter(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet();
        $this->initialize();
    }

    /**
     * 
     * @param array $productIds 
     */
    public function getStockDispositionEntries(array $productIds) {
        if(!$productIds) {
            return false;
        }
        $select = $this->sql->select();
        $inPredicate = new \Zend\Db\Sql\Predicate\In();
        $inPredicate->setIdentifier('product_id');
        $inPredicate->setValueSet($productIds);
        $select->where($inPredicate);
        $select->order('product_id');
        $select->order('id');
        $resultset = $this->executeSelect($select);
        return $resultset->toArray();
    }
    
}

?>
