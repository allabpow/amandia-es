<?php
/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */


namespace AmandiaES\Table;

use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\TableGateway\AbstractTableGateway;

/**
 * Description of ShipmentTable
 *
 * @author allapow
 */
class ShipmentTable extends AbstractTableGateway implements AdapterAwareInterface {

    protected $table = 'aes_shipment';

    public function setDbAdapter(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet();
        $this->initialize();
    }

    /**
     * 
     * @param string $order
     */
    public function getShipments($order = 'name') {
        $select = $this->sql->select();
        $select->order($order);
        $hResultset = $this->executeSelect($select);
        return $hResultset->toArray();
    }

    /**
     * 
     * @param int $id
     */
    public function getShipmentById($id) {
        $select = $this->sql->select();
        $select->where(array(
            'id' => $id
        ));
        $hResultset = $this->executeSelect($select);
        $resArr = $hResultset->toArray();
        if(!empty($resArr[0])) {
            return $resArr[0];
        }
        return FALSE;
    }

    /**
     * 
     * @param string $order
     */
    public function getShipmentsIdNameAssocc($order = 'group name') {
        $select = $this->sql->select();
        $select->columns(array('id', 'name', 'price'));
        $select->order($order);
        $resultset = $this->executeSelect($select);
        $resultArr = $resultset->toArray();
        $returnArray = array();
        foreach ($resultArr as $row) {
            $returnArray[$row['id']] = $row['name'] . ' - ' . $row['price'];
        }
        return $returnArray;
    }
    
}

?>
