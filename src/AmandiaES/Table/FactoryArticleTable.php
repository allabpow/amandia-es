<?php
/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */


namespace AmandiaES\Table;

use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\TableGateway\AbstractTableGateway;

/**
 * Description of ArticleTable
 *
 * @author allapow
 */
class FactoryArticleTable extends AbstractTableGateway implements AdapterAwareInterface {

    protected $table = 'aes_factory_article';

    public function setDbAdapter(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet();
        $this->initialize();
    }

    /**
     * 
     * @param string $group 
     */
    public function getFactoryArticlesByGroup($group = 1) {
        $select = $this->sql->select();
        if($group) {
            $select->where(array(
                'group' => $group,
            ));
        }
        $select->order('name');
        $resultset = $this->executeSelect($select);
        return $resultset->toArray();
    }
    
    public function getSelectFactoryArticlesAll($group = 1, $order = 'id') {
        $select = $this->sql->select();
        $select->where(array(
            'group' => $group,
        ));
        $select->order($order);
        return $select;
    }
    
    /**
     * 
     * @param array $ids array(2,4,6)
     * @return array
     */
    public function getFactoryArticlesByIds(array $ids) {
        if(count($ids) < 1) {
            return array();
        }
        $select = $this->sql->select();
        $predicateIn = new \Zend\Db\Sql\Predicate\In('id',$ids);
        $select->where($predicateIn);
        $select->order('name');
        $resultset = $this->executeSelect($select);
        return $resultset->toArray();
    }
    
    public function getFactoryArticleById($id) {
        $select = $this->sql->select();
        $select->where(array(
            'id' => (int)$id,
        ));
        $resultset = $this->executeSelect($select);
        $resultArray = $resultset->toArray();
        if(isset($resultArray[0])) {
            return $resultArray[0];
        }
        return FALSE;
    }
    
    public function saveFactoryArticle($dataFA) {
        $dataArray = array(
            'factory_article_nr' => $dataFA['factory_article_nr'],
            'name' => $dataFA['name'],
            'group' => $dataFA['group'],
            'measure_symbol_geom' => $dataFA['measure_symbol_geom'],
            'measure_scale_geom' => $dataFA['measure_scale_geom'],
            'measure_symbol_weight' => $dataFA['measure_symbol_weight'],
            'measure_scale_weight' => $dataFA['measure_scale_weight'],
            'price_sell' => $dataFA['price_sell'],
            'description' => $dataFA['description'],
        );
        if(isset($dataFA['id']) && $dataFA['id']) {
            $update = $this->sql->update();
            $update->set($dataArray);
            $update->where(array(
                'id' => $dataFA['id'],
            ));
            $result = $this->updateWith($update);
            if($result == 1 || $result == 0) {
                return (int)$dataFA['id'];
            }
            return FALSE;
        } else {
            $insert = $this->sql->insert();
            $insert->values($dataArray);
            $result = $this->executeInsert($insert);
            if($result) {
                $select = $this->sql->select();
                $select->columns(array(new \Zend\Db\Sql\Expression("LAST_INSERT_ID() as id")));
                $selResult = $this->selectWith($select);
                $selResArray = $selResult->toArray();
                if(isset($selResArray[0]['id'])) {
                    return (int)$selResArray[0]['id'];
                }
                return FALSE;
            }
        }
    }
    
    public function searchFactoryArticle($logger, array $dataSearch, $order = 'factory_article_nr DESC', $onlySelect = FALSE) {
//        $logger->log(\Zend\Log\Logger::DEBUG, 'drin');
        $select = $this->sql->select();
        if(!empty($dataSearch['name'])) {
            $likeArticleName = new \Zend\Db\Sql\Predicate\Like('name');
            $likeArticleName->setLike('%' . $dataSearch['name'] . '%');
            $select->where($likeArticleName);
        }
        if(!empty($dataSearch['group'])) {
            $select->where(array(
                'group' => $dataSearch['group'],
            ));
        }
        if(!empty($dataSearch['factory_article_nr'])) {
            $likeArticleNr = new \Zend\Db\Sql\Predicate\Like('factory_article_nr');
            $likeArticleNr->setLike('%' . $dataSearch['factory_article_nr'] . '%');
            $select->where($likeArticleNr);
        }
        
        $select->order($order);
        
        if(!$onlySelect) {
            $resultset = $this->executeSelect($select);
            $resultArray = $resultset->toArray();
            if(isset($resultArray[0])) {
                return $resultArray;
            }
        } else {
            return $select;
        }
        return FALSE;
    }
    
    public function updateArticles($factoryArticleId, $articleId, $operator) {
        $factoryArticle = $this->getFactoryArticleById($factoryArticleId);
        $articlesArr = json_decode($factoryArticle['articles'], TRUE);
        switch ($operator) {
            case 'minus':
                if(array_key_exists($articleId, $articlesArr)) {
                    $articlesArr[$articleId] -= 1;
                } else {
                    $articlesArr[$articleId] = 0; // spart woanders Code
                }
                break;
            case 'plus':
                if(array_key_exists($articleId, $articlesArr)) {
                    $articlesArr[$articleId] += 1;
                } else {
                    $articlesArr[$articleId] = 1;
                }
                break;
            default :
                return FALSE;
        }
        $withoutEmptyVals = array();
        foreach ($articlesArr as $key => $val) {
            if ($val > 0) {
                $withoutEmptyVals[$key] = $val;
            }
        }
        $articlesJson = json_encode($withoutEmptyVals);
        $update = $this->sql->update();
        $update->set(array(
            'articles' => $articlesJson,
        ));
        $update->where(array(
            'id' => $factoryArticleId,
        ));
        $result = $this->updateWith($update);
        if ($result == 1) {
            return TRUE;
        }
        return FALSE;
    }
    
    public function updateMaterials($factoryArticleId, $materialId, $amount) {
        $factoryArticle = $this->getFactoryArticleById($factoryArticleId);
        $materialsArr = json_decode($factoryArticle['materials'], TRUE);
        $materialsArr[$materialId] = $amount;
        $withoutEmptyVals = array();
        foreach ($materialsArr as $key => $val) {
            if ($val > 0) {
                $withoutEmptyVals[$key] = $val;
            }
        }
        $materialsJson = json_encode($withoutEmptyVals);
        $update = $this->sql->update();
        $update->set(array(
            'materials' => $materialsJson,
        ));
        $update->where(array(
            'id' => $factoryArticleId,
        ));
        $result = $this->updateWith($update);
        if ($result == 1) {
            return TRUE;
        }
        return FALSE;
    }
    
    public function updateServices($factoryArticleId, $serviceId, $amount) {
        $factoryArticle = $this->getFactoryArticleById($factoryArticleId);
        if(!$factoryArticle) {
            return FALSE;
        }
        $servicesArr = json_decode($factoryArticle['services'], TRUE);
        $servicesArr[$serviceId] = $amount;
        $withoutEmptyVals = array();
        foreach ($servicesArr as $key => $val) {
            if ($val > 0) {
                $withoutEmptyVals[$key] = $val;
            }
        }
        $servicesJson = json_encode($withoutEmptyVals);
        $update = $this->sql->update();
        $update->set(array(
            'services' => $servicesJson,
        ));
        $update->where(array(
            'id' => $factoryArticleId,
        ));
        $result = $this->updateWith($update);
        if ($result == 1) {
            return TRUE;
        }
        return FALSE;
    }
}

?>
