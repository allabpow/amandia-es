<?php
/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */


namespace AmandiaES\Table\Language;

use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\TableGateway\AbstractTableGateway;

/**
 * Lang ist user in Controller for e.g. Product-Sell-Text
 * 
 * NOT used from Service AmandiaES\Service\Translator
 * 
 * Weil es hier NUR um Produktbeschreibungen geht sollte es vielleicht von der
 * App-Translation tabellentechnisch abgekoppelt werden!?
 *
 * @author allapow
 */
class Lang extends AbstractTableGateway implements AdapterAwareInterface {

    protected $table = 'aes_lang_de';

    public function setDbAdapter(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet();
        $this->initialize();
    }

    private function setLang($locale) {
        $this->table = 'aes_lang_' . $locale;
    }
    
    /**
     * 
     * @param array $keys array('key_one','key_two')
     * @return array
     */
    public function getLangValues($locale, array $keys) {
        $this->setLang($locale);
        $select = $this->sql->select();
        $predicateIn = new \Zend\Db\Sql\Predicate\In('key',$keys);
        $select->where($predicateIn);
        $select->order('key');
        $resultset = $this->executeSelect($select);
        return $resultset->toArray();
    }
    
    public function getLangValue($locale, $key) {
        $this->setLang($locale);
        $select = $this->sql->select();
        $select->where(array(
            'key' => $key,
        ));
        $resultset = $this->executeSelect($select);
        $resultArray = $resultset->toArray();
        if(isset($resultArray[0])) {
            return $resultArray[0]['value'];
        }
        return FALSE;
    }
    
//    public function getsKeyValueAssocc($locale, $order = 'key') {
//        $this->setLang($locale);
//        $select = $this->sql->select();
//        $select->columns(array('key', 'value'));
//        $select->order($order);
//        $resultset = $this->executeSelect($select);
//        $resultArr = $resultset->toArray();
//        $returnArray = array();
//        foreach ($resultArr as $row) {
//            $returnArray[$row['key']] = $row['value'];
//        }
//        return $returnArray;
//    }
//    
//    public function getProductFormKeyValueAssoc($locale, $productId) {
//        $this->setLang($locale);
//        $inArray = array($productId . 'text_display_name', $productId . 'text_short', $productId . 'text_long');
//        $select = $this->sql->select();
//        $predicateIn = new \Zend\Db\Sql\Predicate\In('key',$inArray);
//        $select->where($predicateIn);
//        $result = $this->selectWith($select);
//        $resultArr = $result->toArray();
//        $returnArray = array();
//        foreach ($resultArr as $row) {
//            $returnArray[$row['key']] = $row['value'];
//        }
//        return $returnArray;
//    }
    
    public function keyExist($locale, $key) {
        $this->setLang($locale);
        $result = $this->select(array('key' => $key));
        return $result->count();
    }
    
    /**
     * 
     * @param string $locale
     * @param string|int $pre Pre pice Key
     * @param string|int $post Post pice key
     * @param string $value
     * @return int
     */
    public function saveWithPreGluePostKey($locale, $pre, $post, $value) {
        $this->setLang($locale);
        $key = $pre . $post;
        if($this->keyExist($locale, $key) > 0) {
            $result = $this->update(array('value' => $value), array('key' => $key));
            if($result == 1 || $result == 0) {
                return true;
            }
        } else {
            $result = $this->insert(array('key' => $key, 'value' => $value));
            if($result == 1) {
                return true;
            }
        }
        return FALSE;
    }
    
    public function saveKeyValue($locale, $key, $value) {
        $this->setLang($locale);
        if($this->keyExist($locale, $key) > 0) {
            return $this->update(array('value' => $value), array('key' => $key));
        } else {
            return $this->insert(array('key' => $key, 'value' => $value));
        }
    }
}

?>
