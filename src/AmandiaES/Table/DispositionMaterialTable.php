<?php
/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */


namespace AmandiaES\Table;

use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\TableGateway\AbstractTableGateway;

/**
 * Description of StockDispositionMaterialTable
 *
 * @author allapow
 */
class DispositionMaterialTable extends AbstractTableGateway implements AdapterAwareInterface {

    protected $table = 'aes_stock_disposition_material';

    public function setDbAdapter(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet();
        $this->initialize();
    }

    /**
     * 
     * @param array $materialIds 
     */
    public function getStockDispositionEntries(array $materialIds) {
        if(!$materialIds) {
            return false;
        }
        $select = $this->sql->select();
        $inPredicate = new \Zend\Db\Sql\Predicate\In();
        $inPredicate->setIdentifier('material_id');
        $inPredicate->setValueSet($materialIds);
        $select->where($inPredicate);
        $select->order('material_id');
        $select->order('id');
        $resultset = $this->executeSelect($select);
        return $resultset->toArray();
    }
    
}

?>
