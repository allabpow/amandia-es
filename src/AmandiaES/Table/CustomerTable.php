<?php


namespace AmandiaES\Table;

use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\TableGateway\AbstractTableGateway;

/**
 * Description of CustomerTable
 *
 * @author allapow
 */
class CustomerTable extends AbstractTableGateway implements AdapterAwareInterface {

    protected $table = 'aes_customer';

    public function setDbAdapter(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet();
        $this->initialize();
    }

    /**
     * 
     * @param string $order
     */
    public function getCustomers($order = 'id') {
        $select = $this->sql->select();
        $select->order($order);
        $hResultset = $this->executeSelect($select);
        return $hResultset->toArray();
    }
    
    public function getCustomerById($id) {
        $resultArr = $this->select(array('id' => $id))->toArray();
        if(isset($resultArr[0])) {
            return $resultArr[0];
        }
        return FALSE;
    }
    
    public function getCustomerByCustomerNr($customerNr) {
        $resultArr = $this->select(array('customer_nr' => $customerNr))->toArray();
        if(isset($resultArr[0])) {
            return $resultArr[0];
        }
    }
    
    public function searchCustomerByCustomerNr($customerNr) {
        $select = $this->sql->select();
        $likeNr = new \Zend\Db\Sql\Predicate\Like('customer_nr');
        $likeNr->setLike('%' . $customerNr . '%');
        $select->where($likeNr);
        $resultArr = $this->selectWith($select)->toArray();
        if(count($resultArr) > 0) {
            return $resultArr;
        }
        return FALSE;
    }
    
    public function getSelectCustomersAll($group = 1, $order = 'id') {
        $select = $this->sql->select();
        $select->where(array(
            'group' => $group,
        ));
        $select->order($order);
        return $select;
    }
    
    public function saveCustomer($data, \Zend\Log\Logger $logger) {
        if(!isset($data['customer'])) {
            return FALSE;
        }
        $dataCust = $data['customer'];
        $dataArray = array(
            'customer_nr' => $dataCust['customer_nr'],
            'group' => $dataCust['group'],
            'name_first' => $dataCust['name_first'],
            'name_last' => $dataCust['name_last'],
            'name_company' => $dataCust['name_company'],
            'str' => $dataCust['str'],
            'str_nr' => $dataCust['str_nr'],
            'zip' => $dataCust['zip'],
            'city' => $dataCust['city'],
            'country' => $dataCust['country'],
            'sec_name_first' => $dataCust['sec_name_first'],
            'sec_name_last' => $dataCust['sec_name_last'],
            'sec_name_company' => $dataCust['sec_name_company'],
            'sec_str' => $dataCust['sec_str'],
            'sec_str_nr' => $dataCust['sec_str_nr'],
            'sec_zip' => $dataCust['sec_zip'],
            'sec_city' => $dataCust['sec_city'],
            'sec_country' => $dataCust['sec_country'],
        );
        if(isset($dataCust['id']) && $dataCust['id']) {
            $update = $this->sql->update();
            $update->set($dataArray);
            $update->where(array(
                'id' => $dataCust['id'],
            ));
//            $logger->log(\Zend\Log\Logger::DEBUG, $update->getSqlString());
            $result = $this->executeUpdate($update);
            if($result == 1 || $result == 0) {
                return (int)$dataCust['id'];
            }
            return FALSE;
        } else {
            $insert = $this->sql->insert();
            $insert->values($dataArray);
            $result = $this->executeInsert($insert);
            if($result) {
                $select = $this->sql->select();
                $select->columns(array(new \Zend\Db\Sql\Expression("LAST_INSERT_ID() as id")));
                $selResult = $this->selectWith($select);
                $selResArray = $selResult->toArray();
                if(isset($selResArray[0]['id'])) {
                    return (int)$selResArray[0]['id'];
                }
                return FALSE;
            }
        }
    }
    
    public function searchCustomer($logger, array $dataSearch, $order = 'customer_nr DESC', $onlySelect = FALSE) {
//        $logger->log(\Zend\Log\Logger::DEBUG, 'drin');
        $select = $this->sql->select();
        if(!empty($dataSearch['customer_nr'])) {
            $likeCustNr = new \Zend\Db\Sql\Predicate\Like('customer_nr');
            $likeCustNr->setLike('%' . $dataSearch['customer_nr'] . '%');
            $select->where($likeCustNr);
        }
        if (!empty($dataSearch['group'])) {
            $select->where(array(
                'group' => $dataSearch['group'],
            ));
        }
        if(!empty($dataSearch['name_first'])) {
            $likeNameFirst = new \Zend\Db\Sql\Predicate\Like('name_first');
            $likeNameFirst->setLike('%' . $dataSearch['name_first'] . '%');
            $select->where($likeNameFirst);
        }
        if(!empty($dataSearch['name_last'])) {
            $likeNameLast = new \Zend\Db\Sql\Predicate\Like('name_last');
            $likeNameLast->setLike('%' . $dataSearch['name_last'] . '%');
            $select->where($likeNameLast);
        }
        if(!empty($dataSearch['str'])) {
            $likeStr = new \Zend\Db\Sql\Predicate\Like('str');
            $likeStr->setLike('%' . $dataSearch['str'] . '%');
            $select->where($likeStr);
        }
        if(!empty($dataSearch['zip'])) {
            $likeZip = new \Zend\Db\Sql\Predicate\Like('zip');
            $likeZip->setLike('%' . $dataSearch['zip'] . '%');
            $select->where($likeZip);
        }
        if(!empty($dataSearch['city'])) {
            $likeCity = new \Zend\Db\Sql\Predicate\Like('city');
            $likeCity->setLike('%' . $dataSearch['city'] . '%');
            $select->where($likeCity);
        }
        
        $select->order($order);
        
        if(!$onlySelect) {
            $resultset = $this->executeSelect($select);
            $resultArray = $resultset->toArray();
            if(isset($resultArray[0])) {
                return $resultArray;
            }
        } else {
            return $select;
        }
        return FALSE;
    }
}

?>
