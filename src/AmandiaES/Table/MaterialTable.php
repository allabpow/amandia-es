<?php
/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */


namespace AmandiaES\Table;

use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\TableGateway\AbstractTableGateway;

/**
 * Description of MaterialTable
 *
 * @author allapow
 */
class MaterialTable extends AbstractTableGateway implements AdapterAwareInterface {

    protected $table = 'aes_material';

    public function setDbAdapter(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet();
        $this->initialize();
    }
    
    /**
     * 
     * @param string $group 
     */
    public function getMaterialsByGroup($group = 1) {
        $select = $this->sql->select();
        if($group) {
            $select->where(array(
                'group' => $group,
            ));
        }
        $select->order('name');
        $resultset = $this->executeSelect($select);
        return $resultset->toArray();
    }
    
    public function getSelectMaterialsAll($group = 1, $order = 'id') {
        $select = $this->sql->select();
        $select->where(array(
            'group' => $group,
        ));
        $select->order($order);
        return $select;
    }

    /**
     * z.B. fuer 'n Auswahlfeld
     * @param string $order
     */
    public function getMaterialsIdNameAssocc($order = 'id') {
        $select = $this->sql->select();
        $select->columns(array('id', 'name'));
        $select->order($order);
        $resultset = $this->executeSelect($select);
        $resultArr = $resultset->toArray();
        $returnArray = array();
        foreach ($resultArr as $row) {
            $returnArray[$row['id']] = $row['name'];
        }
        return $returnArray;
    }
    
    /**
     * 
     * @param array $ids array(2,4,6)
     * @return array
     */
    public function getMaterialsByIds(array $ids) {
        if(count($ids) < 1) {
            return array();
        }
        $select = $this->sql->select();
        $predicateIn = new \Zend\Db\Sql\Predicate\In('id',$ids);
        $select->where($predicateIn);
        $select->order('name');
        $resultset = $this->executeSelect($select);
        return $resultset->toArray();
    }

    public function getMaterialById($id) {
        $select = $this->sql->select();
        $select->where(array(
            'id' => (int)$id,
        ));
        $resultset = $this->executeSelect($select);
        $resultArray = $resultset->toArray();
        if(isset($resultArray[0])) {
            return array('material' => $resultArray[0]);
        }
        return FALSE;
    }
    
    public function saveMaterial($data) {
        $dataArray = array(
                'name' => $data['material']['name'],
                'group' => $data['material']['group'],
                'supplier' => $data['material']['supplier'],
                'measure_symbol_geom' => $data['material']['measure_symbol_geom'],
                'measure_scale_geom' => $data['material']['measure_scale_geom'],
                'measure_symbol_weight' => $data['material']['measure_symbol_weight'],
                'measure_scale_weight' => $data['material']['measure_scale_weight'],
                'price_buy' => $data['material']['price_buy'],
                'price_sell' => $data['material']['price_sell'],
                'description' => $data['material']['description'],
        );
        if(isset($data['material']['id']) && $data['material']['id']) {
            $update = $this->sql->update();
            $update->set($dataArray);
            $update->where(array(
                'id' => $data['material']['id'],
            ));
            $result = $this->updateWith($update);
            if($result == 1 || $result == 0) {
                return (int)$data['material']['id'];
            }
            return $result;
        } else {
            $insert = $this->sql->insert();
            $insert->values($dataArray);
            $result = $this->executeInsert($insert);
            if($result) {
                $select = $this->sql->select();
                $select->columns(array(new \Zend\Db\Sql\Expression("LAST_INSERT_ID() as id")));
                $selResult = $this->selectWith($select);
                $selResArray = $selResult->toArray();
                if(isset($selResArray[0]['id'])) {
                    return (int)$selResArray[0]['id'];
                }
                return $selResArray;
            }
        }
    }
    
    public function searchMaterial($logger, array $dataSearch, $order = 'name DESC', $onlySelect = FALSE) {
//        $logger->log(\Zend\Log\Logger::DEBUG, 'drin');
        $select = $this->sql->select();
        if(!empty($dataSearch['name'])) {
            $likeArticleName = new \Zend\Db\Sql\Predicate\Like('name');
            $likeArticleName->setLike('%' . $dataSearch['name'] . '%');
            $select->where($likeArticleName);
        }
        if(!empty($dataSearch['group'])) {
            $select->where(array(
                'group' => $dataSearch['group'],
            ));
        }
        if(!empty($dataSearch['supplier'])) {
            $select->where(array(
                'supplier' => $dataSearch['supplier'],
            ));
        }
        if(!empty($dataSearch['article_nr'])) {
            $likeArticleNr = new \Zend\Db\Sql\Predicate\Like('article_nr');
            $likeArticleNr->setLike('%' . $dataSearch['article_nr'] . '%');
            $select->where($likeArticleNr);
        }
        if(!empty($dataSearch['article_nr_extern'])) {
            $likeArticleNrExt = new \Zend\Db\Sql\Predicate\Like('article_nr_extern');
            $likeArticleNrExt->setLike('%' . $dataSearch['article_nr_extern'] . '%');
            $select->where($likeArticleNrExt);
        }
        
        $select->order($order);
        
        if(!$onlySelect) {
            $resultset = $this->executeSelect($select);
            $resultArray = $resultset->toArray();
            if(isset($resultArray[0])) {
                return $resultArray;
            }
        } else {
            return $select;
        }
        return FALSE;
    }
}

?>
