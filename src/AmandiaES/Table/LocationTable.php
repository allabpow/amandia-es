<?php
/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */


namespace AmandiaES\Table;

use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\TableGateway\AbstractTableGateway;

/**
 * Description of LocationTable
 *
 * @author allapow
 */
class LocationTable extends AbstractTableGateway implements AdapterAwareInterface {

    protected $table = 'aes_location';

    public function setDbAdapter(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet();
        $this->initialize();
    }

    public function getLocationById($id) {
        $select = $this->sql->select();
        $select->where(array(
            'id' => $id,
        ));
        $resultset = $this->executeSelect($select);
        $resultArray = $resultset->toArray();
        if(isset($resultArray[0])) {
            return $resultArray[0];
        }
        return FALSE;
    }
    
    
    public function getLocationsIdNameAssocc($order = 'name') {
        $select = $this->sql->select();
        $select->columns(array('id', 'name'));
        $select->order($order);
        $resultset = $this->executeSelect($select);
        $resultArr = $resultset->toArray();
        $returnArray = array();
        foreach ($resultArr as $row) {
            $returnArray[$row['id']] = $row['name'];
        }
        return $returnArray;
    }
    
}

?>
