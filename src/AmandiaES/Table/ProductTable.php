<?php
/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */


namespace AmandiaES\Table;

use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\TableGateway\AbstractTableGateway;

/**
 * Description of ProductTable
 *
 * @author allapow
 */
class ProductTable extends AbstractTableGateway implements AdapterAwareInterface {

    protected $table = 'aes_product';

    public function setDbAdapter(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet();
        $this->initialize();
    }

    /**
     * 
     * @param string $order
     */
    public function getProducts($order = 'id') {
        $select = $this->sql->select();
        $select->order($order);
        $hResultset = $this->executeSelect($select);
        return $hResultset->toArray();
    }
    
    /**
     * optimal for product form
     * @param int $id
     * @return array|boolean
     */
    public function getProduct($id) {
        $select = $this->sql->select();
        $select->where(array('id' => $id));
        $hResultset = $this->executeSelect($select);
        $resArr = $hResultset->toArray();
        if(isset($resArr[0])) {
            return array('product' => $resArr[0]);
        }
        return false;
    }
    
    /**
     * 
     * @param type $id
     * @return array|false
     */
    public function getProductById($id) {
        $select = $this->sql->select();
        $select->where(array('id' => $id));
        $hResultset = $this->executeSelect($select);
        $resArr = $hResultset->toArray();
        if(isset($resArr[0])) {
            return $resArr[0];
        }
        return false;
    }
    
    public function getSelectProductsAll($group = 1, $order = 'id') {
        $select = $this->sql->select();
        $select->where(array(
            'group' => $group,
        ));
        $select->order($order);
        return $select;
    }
    
    /**
     * 
     * @param array $data
     * @return int|mixed
     */
    public function saveProduct(array $data) {
        $productData = $data['product'];
        $dataArray = array(
            'product_nr' => $productData['product_nr'],
            'name' => $productData['name'],
            'group' => $productData['group'],
            'description' => $productData['description'],
            'price' => $productData['price'],
//            'supplier' => $productData['supplier'],
//            'materials' => $productData['materials'],
//            'articles' => $productData['articles'],
//            'shipment' => $productData['shipment'],
        );
        if(isset($productData['id']) && $productData['id']) {
            // set keys for text
            $dataArray['text_display_name'] = $productData['id'] . 'text_display_name';
            $dataArray['text_short'] = $productData['id'] . 'text_short';
            $dataArray['text_long'] = $productData['id'] . 'text_long';
            $update = $this->sql->update();
            $update->set($dataArray);
            $update->where(array(
                'id' => $productData['id'],
            ));
            $result = $this->updateWith($update);
            if($result == 1 || $result == 0) {
                return (int)$productData['id'];
            }
            return FALSE;
        } else {
            $insert = $this->sql->insert();
            $insert->values($dataArray);
            $result = $this->executeInsert($insert);
            if($result) {
                $select = $this->sql->select();
                $select->columns(array(new \Zend\Db\Sql\Expression("LAST_INSERT_ID() as id")));
                $selResult = $this->selectWith($select);
                $selResArray = $selResult->toArray();
                if(isset($selResArray[0]['id'])) {
                    return (int)$selResArray[0]['id'];
                }
                return FALSE;
            }
        }
    }
    
    public function textKeysAvailable($id) {
        $resultArr = $this->select(array('id' => $id))->toArray();
        if(isset($resultArr[0]['text_display_name']) && isset($resultArr[0]['text_short']) && isset($resultArr[0]['text_short'])) {
            return TRUE;
        }
        return FALSE;
    }
    
    /**
     * 
     * @param array $ids array(2,4,6)
     * @return array
     */
    public function getProductsByIds(array $ids) {
        $select = $this->sql->select();
        $predicateIn = new \Zend\Db\Sql\Predicate\In('id',$ids);
        $select->where($predicateIn);
        $select->order('name');
        $resultset = $this->executeSelect($select);
        return $resultset->toArray();
    }
    
    /**
     * 
     * @param type $logger
     * @param array $data
     * @param type $order
     * @param type $onlySelect
     * @param type $limitCount count per page
     * @param type $page
     * @return array|boolean
     */
    public function searchProduct($logger, array $dataSearch, $order = 'name', $onlySelect = FALSE, $limitCount = 0, $page = 1, $count = FALSE) {
//        $logger->log(\Zend\Log\Logger::DEBUG, 'drin');
        $select = $this->sql->select();
        if(!empty($dataSearch['name'])) {
            $likeProdName = new \Zend\Db\Sql\Predicate\Like('name');
            $likeProdName->setLike('%' . $dataSearch['name'] . '%');
            $select->where($likeProdName);
        }
        if(!empty($dataSearch['group'])) {
            $select->where(array(
                'group' => $dataSearch['group'],
            ));
        }
        if(!empty($dataSearch['supplier'])) {
            $select->where(array(
                'supplier' => $dataSearch['supplier'],
            ));
        }
        if(!empty($dataSearch['in_shop'])) {
            if($dataSearch['in_shop'] == 2) { // in shop
                $select->where(array(
                    'in_shop' => 2
                ));
            } elseif ($dataSearch['in_shop'] == 3) { // not in shop
                $select->where(array(
                    'in_shop' => 1
                ));
            }
        }
        
        $select->order($order);
        if(!$count) {
            if($limitCount > 0) {
                $select->limit($limitCount);
            }
            if($page > 1 && $limitCount > 0) {
                $select->offset($page * $limitCount);
            }
        } else {
            $select->columns(array(new \Zend\Db\Sql\Expression('COUNT(id) as productCount')));
            $resultset = $this->executeSelect($select);
            $resultArray = $resultset->toArray();
            return $resultArray[0]['productCount'];
        }
        
        if(!$onlySelect) {
            $resultset = $this->executeSelect($select);
            $resultArray = $resultset->toArray();
            if(isset($resultArray[0])) {
//                $resultArray['count'] = count($resultArray);
                return $resultArray;
            }
        } else {
            return $select;
        }
        return FALSE;
    }
    
    /**
     * 
     * @param int $productId
     * @param int $articleId
     * @param string $operator minus|plus
     * @return boolean
     */
    public function updateArticles($productId, $articleId, $operator) {
        $product = $this->getProductById($productId);
        $articlesArr = json_decode($product['articles'], TRUE);
        if(!is_array($articlesArr)) {
            $articlesArr = array();
        }
        switch ($operator) {
            case 'minus':
                if(array_key_exists($articleId, $articlesArr)) {
                    $articlesArr[$articleId] -= 1;
                } else {
                    $articlesArr[$articleId] = 0; // spart woanders Code
                }
                break;
            case 'plus':
                if(array_key_exists($articleId, $articlesArr)) {
                    $articlesArr[$articleId] += 1;
                } else {
                    $articlesArr[$articleId] = 1;
                }
                break;
            default :
                return FALSE;
        }
        $withoutEmptyVals = array();
        foreach ($articlesArr as $key => $val) {
            if ($val > 0) {
                $withoutEmptyVals[$key] = $val;
            }
        }
        $articlesJson = json_encode($withoutEmptyVals);
        $update = $this->sql->update();
        $update->set(array(
            'articles' => $articlesJson,
        ));
        $update->where(array(
            'id' => $productId,
        ));
        $result = $this->updateWith($update);
        if ($result == 1) {
            return TRUE;
        }
        return FALSE;
    }
    
    /**
     * 
     * @param int $productId
     * @param int $materialId
     * @param float $amount
     * @return boolean
     */
    public function updateMaterials($productId, $materialId, $amount) {
        $product = $this->getProductById($productId);
        $materialsArr = json_decode($product['materials'], TRUE);
        $materialsArr[$materialId] = $amount;
        $withoutEmptyVals = array();
        foreach ($materialsArr as $key => $val) {
            if ($val > 0) {
                $withoutEmptyVals[$key] = $val;
            }
        }
        $materialsJson = json_encode($withoutEmptyVals);
        $update = $this->sql->update();
        $update->set(array(
            'materials' => $materialsJson,
        ));
        $update->where(array(
            'id' => $productId,
        ));
        $result = $this->updateWith($update);
        if ($result == 1) {
            return TRUE;
        }
        return FALSE;
    }
    
    public function updateServices($productId, $serviceId, $amount) {
        $product = $this->getProductById($productId);
        if(!$product) {
            return FALSE;
        }
        $servicesArr = json_decode($product['services'], TRUE);
        $servicesArr[$serviceId] = $amount;
        $withoutEmptyVals = array();
        foreach ($servicesArr as $key => $val) {
            if ($val > 0) {
                $withoutEmptyVals[$key] = $val;
            }
        }
        $servicesJson = json_encode($withoutEmptyVals);
        $update = $this->sql->update();
        $update->set(array(
            'services' => $servicesJson,
        ));
        $update->where(array(
            'id' => $productId,
        ));
        $result = $this->updateWith($update);
        if ($result == 1) {
            return TRUE;
        }
        return FALSE;
    }
    
    public function updateFactoryArticles($productId, $fArticleId, $operator) {
        $product = $this->getProductById($productId);
        $fArticlesArr = json_decode($product['factory_articles'], TRUE);
        if(!is_array($fArticlesArr)) {
            $fArticlesArr = array();
        }
        switch ($operator) {
            case 'minus':
                if(array_key_exists($fArticleId, $fArticlesArr)) {
                    $fArticlesArr[$fArticleId] -= 1;
                } else {
                    $fArticlesArr[$fArticleId] = 0; // spart woanders Code
                }
                break;
            case 'plus':
                if(array_key_exists($fArticleId, $fArticlesArr)) {
                    $fArticlesArr[$fArticleId] += 1;
                } else {
                    $fArticlesArr[$fArticleId] = 1;
                }
                break;
            default :
                return FALSE;
        }
        $withoutEmptyVals = array();
        foreach ($fArticlesArr as $key => $val) {
            if ($val > 0) {
                $withoutEmptyVals[$key] = $val;
            }
        }
        $fArticlesJson = json_encode($withoutEmptyVals);
        $update = $this->sql->update();
        $update->set(array(
            'factory_articles' => $fArticlesJson,
        ));
        $update->where(array(
            'id' => $productId,
        ));
        $result = $this->updateWith($update);
        if ($result == 1) {
            return TRUE;
        }
        return FALSE;
    }
}

?>
