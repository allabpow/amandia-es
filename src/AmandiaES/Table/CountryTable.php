<?php
/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */


namespace AmandiaES\Table;

use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\TableGateway\AbstractTableGateway;

/**
 * Description of CountryTable
 *
 * @author allapow
 */
class CountryTable extends AbstractTableGateway implements AdapterAwareInterface {

    protected $table = 'aes_country';

    public function setDbAdapter(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet();
        $this->initialize();
    }

    /**
     * 
     * @param string $group 
     */
    public function getCountryById($id = 47) {
        $select = $this->sql->select();
        $select->where(array(
            'id' => $id,
        ));
        $result = $this->executeSelect($select);
        $resultArr = $result->toArray();
        if(isset($resultArr[0])) {
            return $resultArr[0];
        }
        return FALSE;
    }
    
    public function getCountriesIdNameAssocc($order = 'name') {
        $select = $this->sql->select();
        $select->columns(array('id', 'name', 'iso_2'));
        $select->order($order);
        $resultset = $this->executeSelect($select);
        $resultArr = $resultset->toArray();
        $returnArray = array();
        foreach ($resultArr as $row) {
            $returnArray[$row['id']] = ($row['name'] . ' ' . $row['iso_2']);
        }
        return $returnArray;
    }
}

?>
