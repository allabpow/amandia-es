<?php
/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */


namespace AmandiaES\Table;

use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\TableGateway\AbstractTableGateway;

/**
 * Description of ProductTable
 *
 * @author allapow
 */
class ConfigTable extends AbstractTableGateway implements AdapterAwareInterface {

    protected $table = 'aes_config';

    public function setDbAdapter(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet();
        $this->initialize();
    }

    /**
     * 
     * @param string $group 
     */
    public function getConfig($group = '') {
        $select = $this->sql->select();
        if($group) {
            $select->where(array(
                'group' => $group,
            ));
        }
        $select->order('key');
        $resultset = $this->executeSelect($select);
        return $resultset->toArray();
    }
    
    public function getConfigValue($key,$type = 'int') {
        $valueColumn = '';
        switch ($type) {
            case 'int':
                $valueColumn = 'value_int';
                break;
            case 'dec':
                $valueColumn = 'value_dec';
                break;
            default :
                return FALSE;
        }
        $select = $this->sql->select();
        $select->columns(array($valueColumn));
        $select->where(array(
            'key' => $key,
        ));
        
        $resultset = $this->executeSelect($select);
        if($resultset->count() > 0) {
            $resArr = $resultset->toArray();
            return $resArr[0][$valueColumn];
        }
    }
    
}

?>
