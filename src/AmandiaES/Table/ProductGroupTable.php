<?php
/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */


namespace AmandiaES\Table;

use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\TableGateway\AbstractTableGateway;

/**
 * Description of ProductGroupTable
 *
 * @author allapow
 */
class ProductGroupTable extends AbstractTableGateway implements AdapterAwareInterface {

    protected $table = 'aes_product_group';

    public function setDbAdapter(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet();
        $this->initialize();
    }

    /**
     * 
     * @param string $group 
     */
    public function getGroupById($id = 1) {
        $select = $this->sql->select();
        $select->where(array(
            'id' => $id,
        ));
        $result = $this->executeSelect($select);
        $resultArr = $result->toArray();
        if(isset($resultArr[0])) {
            return $resultArr[0];
        }
        return FALSE;
    }
    
    
    public function getProductGroupsIdNameAssocc($order = 'id') {
        $select = $this->sql->select();
        $select->columns(array('id', 'name'));
        $select->order($order);
        $resultset = $this->executeSelect($select);
        $resultArr = $resultset->toArray();
        $returnArray = array();
        foreach ($resultArr as $row) {
            $returnArray[$row['id']] = $row['name'];
        }
        return $returnArray;
    }
}

?>
