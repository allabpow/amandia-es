<?php
/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */


namespace AmandiaES\Table;

use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\TableGateway\AbstractTableGateway;

/**
 * Description of FactoryOrderTable
 *
 * @author allapow
 */
class FactoryOrderTable extends AbstractTableGateway implements AdapterAwareInterface {

    protected $table = 'aes_factory_order';

    public function setDbAdapter(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet();
        $this->initialize();
    }

    public function getFactoryOrderById($id) {
        $select = $this->sql->select();
        $select->where(array(
            'id' => $id,
        ));
        $resultset = $this->executeSelect($select);
        $resultArray = $resultset->toArray();
        if(isset($resultArray[0])) {
            return $resultArray[0];
        }
        return FALSE;
    }
    
    /**
     * 
     * @param type $factoryArticleId
     * @param type $order
     * @return array|false
     */
    public function getFactoryOrdersForFactoryArticle($factoryArticleId, $order = 'id') {
        $select = $this->sql->select();
        $select->where(array(
            'factory_article_id' => $factoryArticleId,
        ));
        $select->order($order);
        $resultset = $this->executeSelect($select);
        $resultArray = $resultset->toArray();
        if(isset($resultArray[0])) {
            return $resultArray;
        }
        return FALSE;
    }
    
    /**
     * 
     * @param int $factoryArticleId
     * @param int $factoryArticleCount
     * @return boolean
     */
    public function createFactoryOrder($factoryArticleId, $factoryArticleCount, $userId) {
        $userId = (int)$userId;
        if(!$userId) {
            return FALSE;
        }
        $insert = $this->sql->insert();
        $insert->values(array(
            'factory_article_id' => $factoryArticleId,
            'factory_article_count' => $factoryArticleCount,
            'employee_id' => $userId,
            'datetime_creation' => date('Y-m-d H:i:s'),
        ));
        $result = $this->insertWith($insert);
        if($result == 1) {
            return TRUE;
        }
        return FALSE;
    }
    
    /**
     * 
     * @param type $data
     * @return type
     */
    public function updateFactoryOrder($data) {
        $factoryData = $data['factory_order'];
        $dataArray = array(
            'factory_order_nr' => $factoryData['factory_order_nr'],
            'location' => $factoryData['location'],
            'comment' => $factoryData['comment'],
            'datetime_last_change' => date('Y-m-d H:i:s'),
        );
        $update = $this->sql->update();
        $update->set($dataArray);
        $update->where(array(
            'id' => $factoryData['id'],
        ));
        $result = $this->updateWith($update);
        if($result == 1 || $result == 0) {
            return $factoryData['id'];
        }
        return FALSE;
    }
    
    public function searchFactoryOrder($logger, array $dataSearch, $order = 'factory_article_id factory_order_nr', $onlySelect = FALSE) {
//        $logger->log(\Zend\Log\Logger::DEBUG, 'drin');
        $select = $this->sql->select();
        if(!empty($dataSearch['factory_article_nr'])) {
            //sub SELECT
            $sqlSub = new \Zend\Db\Sql\Sql($this->adapter);
            $subSelectFA = $sqlSub->select('aes_factory_article');
            $likeFANr = new \Zend\Db\Sql\Predicate\Like('factory_article_nr');
            $likeFANr->setLike('%' . $dataSearch['factory_article_nr'] . '%');
            $subSelectFA->columns(array('id'));
            $subSelectFA->where($likeFANr);
            
            $inFAId = new \Zend\Db\Sql\Predicate\In('factory_article_id');
            $inFAId->setValueSet($subSelectFA);
            
            $select->where($inFAId);
        }
        if(!empty($dataSearch['employee_id'])) {
            // wenn verschiedene employee_id's pro Factory-Order
            // dann fuer jede factory-Order eigene row in der db
            // dann hier ein DISTINCT oder so
            $select->where(array(
                'employee_id' => $dataSearch['employee_id'],
            ));
        }
        if(!empty($dataSearch['cost_center'])) {
            $select->where(array(
                'cost_center' => $dataSearch['cost_center'],
            ));
        }
        if(!empty($dataSearch['datetime_creation'])) {
            $select->where->greaterThan('datetime_creation', $dataSearch['datetime_creation']);
        }
        if(!empty($dataSearch['datetime_finish'])) {
            $select->where->greaterThan('datetime_finish', $dataSearch['datetime_finish']);
        }
        if(!empty($dataSearch['finished'])) {
            if($dataSearch['finished'] == 1) {
                $select->where->isNull('datetime_finish');
            } elseif ($dataSearch['finished'] == 2) {
                $select->where->isNotNull('datetime_finish');
            }
        }
        
        $select->order($order);
        
        if(!$onlySelect) {
            $resultset = $this->executeSelect($select);
            $resultArray = $resultset->toArray();
            if(isset($resultArray[0])) {
                return $resultArray;
            }
        } else {
            return $select;
        }
        return FALSE;
    }
}

?>
