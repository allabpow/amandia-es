<?php
/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */


namespace AmandiaES\Table;

use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\TableGateway\AbstractTableGateway;

/**
 * Description of StockMaterialTable
 *
 * @author allapow
 */
class StockMaterialTable extends AbstractTableGateway implements AdapterAwareInterface {

    protected $table = 'aes_stock_material';

    public function setDbAdapter(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet();
        $this->initialize();
    }

    /**
     * 
     * @param array $materialIds 
     */
    public function getStockEntries(array $materialIds) {
        if(!$materialIds) {
            return false;
        }
        $select = $this->sql->select();
        $inPredicate = new \Zend\Db\Sql\Predicate\In();
        $inPredicate->setIdentifier('material_id');
        $inPredicate->setValueSet($materialIds);
        $select->where($inPredicate);
        $select->order('material_id');
        $select->order('id');
        $resultset = $this->executeSelect($select);
        return $resultset->toArray();
    }
    
}

?>
