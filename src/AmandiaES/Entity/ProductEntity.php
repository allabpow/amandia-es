<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Entity;

/**
 * Description of ProductEntity
 *
 * @author bitkorn
 */
class ProductEntity {
    

    /**
     *
     * @var \AmandiaES\Table\ProductTable
     */
    private $productTable;

    /**
     *
     * @var \AmandiaES\Table\ProductGroupTable
     */
    private $productGroupTable;

    /**
     *
     * @var \AmandiaES\Table\StockProductTable
     */
    private $stockProductTable;

    /**
     *
     * @var \AmandiaES\Table\DispositionProductTable
     */
    private $dispositionProductTable;

    /**
     *
     * @var \AmandiaES\Table\SupplierTable 
     */
    private $supplierTable;
    
    
 
    public function setProductTable(\AmandiaES\Table\ProductTable $productTable) {
        $this->productTable = $productTable;
    }

    public function setProductGroupTable(\AmandiaES\Table\ProductGroupTable $productGroupTable) {
        $this->productGroupTable = $productGroupTable;
    }

    public function setStockProductTable(\AmandiaES\Table\StockProductTable $stockProductTable) {
        $this->stockProductTable = $stockProductTable;
    }

    public function setDispositionProductTable(\AmandiaES\Table\DispositionProductTable $dispositionProductTable) {
        $this->dispositionProductTable = $dispositionProductTable;
    }

    public function setSupplierTable(\AmandiaES\Table\SupplierTable $supplierTable) {
        $this->supplierTable = $supplierTable;
    }

}
