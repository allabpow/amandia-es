<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Entity;

/**
 * Description of OrderEntity
 *
 * @author bitkorn
 */
class Amandia {
    
    /**
     *
     * @var \BitkornUser\Service\AccessService
     */
    private $accessService;

    /**
     *
     * @var \AmandiaES\Table\OrderTable 
     */
    private $orderTable;

    /**
     *
     * @var \AmandiaES\Table\FactoryOrderTable 
     */
    private $factoryOrderTable;

    /**
     *
     * @var \AmandiaES\Table\CostCenterTable 
     */
    private $costCenterTable;

    /**
     *
     * @var \AmandiaES\Table\ProductTable
     */
    private $productTable;

    /**
     *
     * @var \AmandiaES\Table\ProductGroupTable
     */
    private $productGroupTable;

    /**
     *
     * @var \AmandiaES\Table\StockProductTable
     */
    private $stockProductTable;

    /**
     *
     * @var \AmandiaES\Table\DispositionProductTable
     */
    private $dispositionProductTable;

    /**
     *
     * @var \AmandiaES\Table\ArticleTable 
     */
    private $articleTable;

    /**
     *
     * @var \AmandiaES\Table\MaterialTable 
     */
    private $materialTable;

    /**
     *
     * @var \AmandiaES\Table\SupplierTable 
     */
    private $supplierTable;
    
}
