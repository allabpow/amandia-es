<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */
namespace AmandiaES\Service\Shop;

/**
 * Description of CalculateProductPrice
 *
 * @author bitkorn
 */
class Calculator implements \Zend\Db\Adapter\AdapterAwareInterface {
    
    /**
     *
     * @var array product row from db
     */
    private $product;
    
    /**
     *
     * @var \AmandiaES\Table\ArticleTable
     */
    private $articleTable;
    
    /**
     *
     * @var \AmandiaES\Table\MaterialTable
     */
    private $materialTable;
    
    /**
     *
     * @var \AmandiaES\Table\ServiceTable
     */
    private $serviceTable;
    
    /**
     *
     * @var \AmandiaES\Table\FactoryArticleTable
     */
    private $factoryArticleTable;
    
    /**
     *
     * @var \AmandiaES\Table\ShipmentTable
     */
    private $shipmentTable;
    
    public function setDbAdapter(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet();
    }
    
    public function setProduct(array $product) {
        $this->product = $product;
        return $this;
    }

    /**
     * 
     * @return real
     */
    public function calculateProductPriceSell() {
        if(!isset($this->product)) {
            return FALSE;
        }
        $totalPrice = $this->product['price'];
        $articles = json_decode($this->product['articles'], TRUE);
        if($articles) {
            foreach ($articles as $articleId => $articleCount) {
                $article = $this->articleTable->getArticleById($articleId);
                $totalPrice += ($article['price_sell'] * $articleCount);
            }
        }
        $materials = json_decode($this->product['materials'], TRUE);
        if($materials) {
            foreach ($materials as $materialId => $materialAmount) {
                $material = $this->materialTable->getMaterialById($materialId);
                $totalPrice += ($material['material']['price_sell'] * $materialAmount);
            }
        }
        $services = json_decode($this->product['services'], TRUE);
        if($services) {
            foreach ($services as $serviceId => $serviceAmount) {
                $service = $this->serviceTable->getServiceById($serviceId);
                $totalPrice += ($service['price_sell'] * $serviceAmount);
            }
        }
        $factoryArticles = json_decode($this->product['factory_articles'], TRUE);
        if($factoryArticles) {
            foreach ($factoryArticles as $articleId => $articleAmount) {
                $factoryArticle = $this->factoryArticleTable->getFactoryArticleById($articleId);
                $totalPrice += ($factoryArticle['price_sell'] * $articleAmount);
            }
        }
        $shipment = $this->shipmentTable->getShipmentById($this->product['shipment']);
        $totalPrice += $shipment['price'];
        return $totalPrice;
    }
    
    public function setArticleTable(\AmandiaES\Table\ArticleTable $articleTable) {
        $this->articleTable = $articleTable;
    }

    public function setMaterialTable(\AmandiaES\Table\MaterialTable $materialTable) {
        $this->materialTable = $materialTable;
    }

    public function setServiceTable(\AmandiaES\Table\ServiceTable $serviceTable) {
        $this->serviceTable = $serviceTable;
    }
    
    public function setFactoryArticleTable(\AmandiaES\Table\FactoryArticleTable $factoryArticleTable) {
        $this->factoryArticleTable = $factoryArticleTable;
    }
    
    public function setShipmentTable(\AmandiaES\Table\ShipmentTable $shipmentTable) {
        $this->shipmentTable = $shipmentTable;
    }


}
