<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Service;

/**
 * Vorlagen holen fuellen und HTML return
 * 
 * oder "nur" diverse Feldinhalte holen
 * z.B. Absender, Anschrift, head, footer, top-left, top-right, bottom, content mit table etc.
 * ...abhaengig vom Dokument Typ
 *
 * @author bitkorn
 */
class DraftService {
    
    
    /**
     *
     * @var \Zend\Log\Logger
     */
    private $logger;

    /**
     * Full path and filename from the HTML draft
     * 
     * @var string
     */
    private $draftHtmlFilename;
    
}
