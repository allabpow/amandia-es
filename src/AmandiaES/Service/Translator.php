<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Service;

/**
 * Translator is used e.g. in ViewHelper AmandiaES\View\Helper\AesTranslate
 *
 * @author bitkorn
 */
class Translator extends \Zend\Db\TableGateway\AbstractTableGateway implements \Zend\Db\Adapter\AdapterAwareInterface {

    /**
     *
     * @var \Zend\Db\Adapter\Adapter
     */
    protected $adapter;
    protected $table = 'aes_lang_en';
    private $tableDefault = 'aes_lang_de'; // MasterTable wo alle keys eine value haben muessen
    private $lang;

    public function setDbAdapter(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet();
        $this->initialize();
    }

    public function initLang() {
        if (!isset($_SESSION['language'])) {
            $langArr = explode(',', htmlspecialchars($_SERVER['HTTP_ACCEPT_LANGUAGE']));
            $minusPos = strpos($langArr[0], '-');
            if($minusPos) {
                $this->lang = substr($langArr[0], 0, $minusPos);
            } else {
                $this->lang = $langArr[0];
            }
        } else {
            $this->lang = htmlspecialchars($_SESSION['language']);
        }
        if ($this->lang) {
            $this->table = 'aes_lang_' . $this->lang;
            return;
        }
    }

    public function translate($key) {
        $select = $this->sql->select();
        $select->where(array(
            'key' => $key
        ));
        $result = $this->selectWith($select);
        $resArr = $result->toArray();
        if (!empty($resArr[0]['value'])) {
            return $resArr[0]['value'];
        }
        return 'untranslated text';
//        $sql = new \Zend\Db\Sql\Sql($this->adapter);
//        $selectDefault = new \Zend\Db\Sql\Select($this->tableDefault);
//        $selectDefault->where(array(
//            'key' => $key
//        ));
    }

}
