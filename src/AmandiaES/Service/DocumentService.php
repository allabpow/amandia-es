<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Service;

require_once 'tcpdf.php';
require_once 'tcpdf_barcodes_2d.php';

/**
 * HTML Dateien als PDF ausgeben | ausgeben & speichern
 * mit TCPDF
 * 
 * oder nach Dokument Typ definierte Felder fuellen
 * 
 * vielleicht eine Dokument base Class mit diversen Unterklassen mit entsprechenden Methoden zum Felder fuellen
 * ...diese Sub-Klassen dann hier einfuegen und generate()
 * ...was haette die Base-Klasse (muesste sie haben)?
 *
 * @author bitkorn
 */
class DocumentService {
    
    /**
     *
     * @var \Zend\Log\Logger
     */
    private $logger;
    
    /**
     *
     * @var string
     */
    private $documentFolder;
    
    private $format = 'A4';
    
}
