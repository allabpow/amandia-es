<?php

/*
 * Entwickler:          Torsten Brieskorn
 * Entwickler Email:    mail@bitkorn.de
 * Entwickler Webseite: www.bitkorn.de
 */

namespace AmandiaES\Service;

/**
 * Create a folder or look if it writable
 * 
 * docroot/YYYY/MM/pre01/pre02/pre03
 *
 * @author bitkorn
 */
class FolderService {

    const fileNamePattern = '/^[a-zA-Z0-9]+[a-zA-Z0-9_-]{0,20}[a-zA-Z0-9]+$/';

    private $documentRoot;

    /**
     *
     * @var \Zend\Log\Logger
     */
    private $logger;

    /**
     * full path of the folder
     * computed
     * @var string 
     */
    private $folderName;

    /**
     * DateTime for the full path
     * 
     * @var int Unix timestamp
     */
    private $datetime;
    private $pre01;
    private $pre02;
    private $pre03;

    /**
     * 
     * @param string $documentRoot
     * @param int $datetime Unix timestamp
     * @param string $pre01 e.g. customer ID
     * @param string $pre02 e.g. document type (order, offer, contract etc.)
     * @param string $pre03 e.g. nr from document type (order ID, etc.)
     * @param \Zend\Log\Logger $logger
     */
    function __construct(\Zend\Log\Logger $logger, $documentRoot, $datetime, $pre01 = '', $pre02 = '', $pre03 = '') {
        $this->logger = $logger;
        $this->documentRoot = $documentRoot;
        $this->datetime = $datetime;
        $this->pre01 = $pre01;
        $this->pre02 = $pre02;
        $this->pre03 = $pre03;
    }

    public function computeFolder() {
        if (!$this->documentRoot) {
            throw new \Exception(' bad method call');
        }
        $this->folderName = realpath($this->documentRoot);
        if (!$this->folderName || !file_exists($this->folderName) || !is_dir($this->folderName) || !is_writable($this->folderName)) {
            $this->logger->log(\Zend\Log\Logger::ERR, 'problem with document_root: ' . $this->documentRoot);
            return FALSE;
        }
        // year
        $this->folderName .= DIRECTORY_SEPARATOR . date('Y', $this->datetime);
        if (!is_dir($this->folderName)) {
            if (!mkdir($this->folderName, 0755)) {
                $this->logger->log(\Zend\Log\Logger::ERR, 'problem with mkdir: ' . $this->folderName);
                return FALSE;
            }
        } elseif (!is_writable($this->folderName)) {
            if (!chmod($this->folderName, 0755)) {
                $this->logger->log(\Zend\Log\Logger::ERR, 'problem with chmod: ' . $this->folderName);
                return FALSE;
            }
        }
        // month
        $this->folderName .= DIRECTORY_SEPARATOR . date('m', $this->datetime);
        if (!is_dir($this->folderName)) {
            if (!mkdir($this->folderName, 0755)) {
                $this->logger->log(\Zend\Log\Logger::ERR, 'problem with mkdir: ' . $this->folderName);
                return FALSE;
            }
        } elseif (!is_writable($this->folderName)) {
            if (!chmod($this->folderName, 0755)) {
                $this->logger->log(\Zend\Log\Logger::ERR, 'problem with chmod: ' . $this->folderName);
                return FALSE;
            }
        }
        // pre01
        if(!$this->pre01) {
            return $this->folderName;
        }
        $pre1Matches = array();
        preg_match(self::fileNamePattern, $this->pre01, $pre1Matches);
//        $this->logger->log(\Zend\Log\Logger::DEBUG, print_r($pre1Matches, TRUE));
        if(!$pre1Matches) {
            $this->logger->log(\Zend\Log\Logger::ERR, 'PCRE problem with path piece: ' . $this->pre01);
            return FALSE;
        }
        $this->folderName .= DIRECTORY_SEPARATOR . implode('', $pre1Matches);
        if (!is_dir($this->folderName)) {
            if (!mkdir($this->folderName, 0755)) {
                $this->logger->log(\Zend\Log\Logger::ERR, 'problem with mkdir: ' . $this->folderName);
                return FALSE;
            }
        } elseif (!is_writable($this->folderName)) {
            if (!chmod($this->folderName, 0755)) {
                $this->logger->log(\Zend\Log\Logger::ERR, 'problem with chmod: ' . $this->folderName);
                return FALSE;
            }
        }
        // pre02
        if(!$this->pre02) {
            return $this->folderName;
        }
        $pre2Matches = array();
        preg_match(self::fileNamePattern, $this->pre02, $pre2Matches);
//        $this->logger->log(\Zend\Log\Logger::DEBUG, print_r($pre2Matches, TRUE));
        if(!$pre2Matches) {
            $this->logger->log(\Zend\Log\Logger::ERR, 'PCRE problem with path piece: ' . $this->pre02);
            return FALSE;
        }
        $this->folderName .= DIRECTORY_SEPARATOR . implode('', $pre2Matches);
        if (!is_dir($this->folderName)) {
            if (!mkdir($this->folderName, 0755)) {
                $this->logger->log(\Zend\Log\Logger::ERR, 'problem with mkdir: ' . $this->folderName);
                return FALSE;
            }
        } elseif (!is_writable($this->folderName)) {
            if (!chmod($this->folderName, 0755)) {
                $this->logger->log(\Zend\Log\Logger::ERR, 'problem with chmod: ' . $this->folderName);
                return FALSE;
            }
        }
        // pre03
        if(!$this->pre03) {
            return $this->folderName;
        }
        $pre3Matches = array();
        preg_match(self::fileNamePattern, $this->pre03, $pre3Matches);
//        $this->logger->log(\Zend\Log\Logger::DEBUG, print_r($pre3Matches, TRUE));
        if(!$pre3Matches) {
            $this->logger->log(\Zend\Log\Logger::ERR, 'PCRE problem with path piece: ' . $this->pre03);
            return FALSE;
        }
        $this->folderName .= DIRECTORY_SEPARATOR . implode('', $pre3Matches);
        if (!is_dir($this->folderName)) {
            if (!mkdir($this->folderName, 0755)) {
                $this->logger->log(\Zend\Log\Logger::ERR, 'problem with mkdir: ' . $this->folderName);
                return FALSE;
            }
        } elseif (!is_writable($this->folderName)) {
            if (!chmod($this->folderName, 0755)) {
                $this->logger->log(\Zend\Log\Logger::ERR, 'problem with chmod: ' . $this->folderName);
                return FALSE;
            }
        }
        return $this->folderName;
    }

}
