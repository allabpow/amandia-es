<?php

array(
'product_show' => array(
    'type' => 'Segment',
    'options' => array(
        'route' => '/products[/:id]',
        'constraints' => array(
            'id' => '[0-9]',
        ),
        'defaults' => array(
            '__NAMESPACE__' => 'Modulname\Controller',
            'controller' => 'Product',
            'action' => 'index',
        ),
    ),
),
);
