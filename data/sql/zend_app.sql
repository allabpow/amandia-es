-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 13. Jun 2015 um 15:19
-- Server Version: 5.6.19-0ubuntu0.14.04.1
-- PHP-Version: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `zend_app`
--

DELIMITER $$
--
-- Prozeduren
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `first_test`(inParam INT(11))
BEGIN

DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SELECT 'wegen Fehler ROLLBACK';
		ROLLBACK;
	END;

END$$

--
-- Funktionen
--
CREATE DEFINER=`root`@`localhost` FUNCTION `select_and_return`(inParam INT(11)) RETURNS int(11)
BEGIN

DECLARE testvar INT DEFAULT 1;

RETURN 1;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `acms_content`
--

CREATE TABLE IF NOT EXISTS `acms_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` int(11) NOT NULL DEFAULT '0',
  `alias` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `title` tinytext NOT NULL,
  `content` mediumtext NOT NULL,
  `menu_id` int(11) NOT NULL DEFAULT '0',
  `menu_name` varchar(45) NOT NULL COMMENT 'name for link in menu',
  `menu_title` tinytext NOT NULL COMMENT 'title for title in menu',
  `parent_content_id` int(11) NOT NULL DEFAULT '0',
  `menu_depth` int(11) NOT NULL DEFAULT '1' COMMENT 'parent_content_id.menu_depth + 1',
  `meta_title` tinytext,
  `meta_keywords` text,
  `meta_description` text,
  `datetime_creation` datetime DEFAULT NULL,
  `datetime_edited` datetime DEFAULT NULL,
  `startseite` int(1) DEFAULT '0',
  `active` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias_UNIQUE` (`alias`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Daten für Tabelle `acms_content`
--

INSERT INTO `acms_content` (`id`, `category`, `alias`, `name`, `title`, `content`, `menu_id`, `menu_name`, `menu_title`, `parent_content_id`, `menu_depth`, `meta_title`, `meta_keywords`, `meta_description`, `datetime_creation`, `datetime_edited`, `startseite`, `active`) VALUES
(1, 1, 'root', 'Root-1', '', '', 1, 'root', '', 0, 1, NULL, NULL, NULL, NULL, NULL, 0, 0),
(3, 1, 'start', 'Startseite', 'Willkommen bei Bitkorn', '<p>hallo lieber Besucher</p>', 1, 'home', 'zur Startseite', 1, 2, NULL, NULL, NULL, NULL, NULL, 1, 1),
(4, 1, 'ueber-mich', 'Über mich', 'Über Torsten', '<p>Eswar einmal ein Junge</p>', 1, 'Torsten', 'etwas über Torsten', 1, 2, NULL, NULL, NULL, NULL, NULL, 0, 1),
(5, 1, 'sein-mopped', 'Torstens Mopped', '', '<p>Sein Mopped war ne 1000er Suzi</p>', 1, 'Seine Suzi', 'die fette Suzi', 4, 3, NULL, NULL, NULL, NULL, NULL, 0, 1),
(6, 1, 'chrome-des-moppeds', 'des Moppeds Chrope', '', '<p>Der Chrome des Moppeds glänzt wie ein...</p>', 1, 'Suzis Chrome', 'der Suzi ihr Chrome', 5, 4, NULL, NULL, NULL, NULL, NULL, 0, 1),
(7, 1, 'dies-das', 'Dies und das', '', '<p>dies und auch das</p>', 1, 'Dies Das', 'dies und das', 3, 3, NULL, NULL, NULL, NULL, NULL, 0, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `acms_content_category`
--

CREATE TABLE IF NOT EXISTS `acms_content_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(45) NOT NULL COMMENT 'dient als route',
  `name` varchar(45) DEFAULT NULL,
  `meta_title` tinytext,
  `meta_description` text,
  `meta_keywords` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Daten für Tabelle `acms_content_category`
--

INSERT INTO `acms_content_category` (`id`, `alias`, `name`, `meta_title`, `meta_description`, `meta_keywords`) VALUES
(1, 'default', 'Default', NULL, NULL, NULL),
(2, 'sinnfrei', 'Stumpfsinn', NULL, NULL, NULL),
(3, 'technisch', 'Technik', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_article`
--

CREATE TABLE IF NOT EXISTS `aes_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_nr` varchar(45) NOT NULL,
  `article_nr_extern` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `group` int(11) NOT NULL DEFAULT '1',
  `supplier` int(11) NOT NULL DEFAULT '1',
  `measure_scale_geom` decimal(10,2) DEFAULT NULL COMMENT 'quantity of measure_identifier',
  `measure_symbol_geom` int(11) DEFAULT NULL COMMENT 'm, cm, kg, ccm',
  `measure_scale_weight` decimal(10,2) DEFAULT NULL COMMENT 'based on measure_default_size',
  `measure_symbol_weight` int(11) DEFAULT NULL COMMENT 'measure identifier only for weight_value, may differ from measure_identifier',
  `price_buy` decimal(10,2) DEFAULT NULL,
  `price_sell` decimal(10,2) DEFAULT NULL,
  `description` tinytext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `article_nr_UNIQUE` (`article_nr`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Daten für Tabelle `aes_article`
--

INSERT INTO `aes_article` (`id`, `article_nr`, `article_nr_extern`, `name`, `group`, `supplier`, `measure_scale_geom`, `measure_symbol_geom`, `measure_scale_weight`, `measure_symbol_weight`, `price_buy`, `price_sell`, `description`) VALUES
(1, 'bolt-00005', '', '4x30 Bolzen', 2, 2, NULL, NULL, NULL, NULL, 0.30, 0.90, ''),
(2, 'bolt-00006', '', '5x30 Bolzen', 2, 2, NULL, NULL, NULL, NULL, 0.34, 0.90, ''),
(3, 'screw-00007', '', 'M10x15 Inbus', 2, 2, NULL, NULL, NULL, NULL, 0.42, 1.11, '8.8'),
(4, 'screw-00008', '', 'M12x15 Inbus', 2, 2, NULL, NULL, NULL, NULL, 0.50, 1.20, '8.8'),
(5, 'h-00001', '', 'Schlosser Stunde', 4, 1, NULL, NULL, NULL, NULL, 35.00, 45.00, 'Schlosser Arbeitsstunde'),
(6, 'h-00002', '', 'Office Stunde', 4, 1, NULL, NULL, NULL, NULL, 32.00, 43.00, 'Büro Arbeitsstunde'),
(7, 'screw-00026', NULL, 'M12x20 Torx', 2, 8, NULL, NULL, NULL, NULL, 1.12, 1.60, 'V2A'),
(8, 'screw-00027', '', 'M10x20 Torx', 2, 8, 33.00, 1, 300.00, 2, 1.03, 1.60, 'V2A'),
(11, 'h-00003', NULL, 'Meister Stunde', 4, 1, NULL, NULL, NULL, NULL, 46.00, 62.50, 'Werkstatt-Meister'),
(12, 'h-00004', NULL, 'Techniker Stunde', 4, 1, NULL, NULL, NULL, NULL, 70.00, 86.00, 'Techniker Ingenieur'),
(13, 'h-00007', NULL, 'Beratung Stunde', 4, 1, NULL, NULL, NULL, NULL, 112.00, 138.00, 'CIO'),
(14, 'screw-00028', NULL, 'M12x15 Torx', 2, 8, NULL, NULL, NULL, NULL, 0.98, 1.26, 'V2A'),
(15, 'ring-00010', NULL, '20x30 Schelle', 2, 2, NULL, NULL, NULL, NULL, 2.30, 3.40, 'verzinkt'),
(16, 'ring-00011', NULL, '20x40 Schelle', 2, 2, NULL, NULL, NULL, NULL, 2.60, 3.70, 'verzinkt'),
(17, 'ring-00012', NULL, '20x50 Schelle', 2, 2, NULL, NULL, NULL, NULL, 2.90, 4.05, 'verzinkt');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_article_group`
--

CREATE TABLE IF NOT EXISTS `aes_article_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(45) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` tinytext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias_UNIQUE` (`alias`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Daten für Tabelle `aes_article_group`
--

INSERT INTO `aes_article_group` (`id`, `alias`, `name`, `description`) VALUES
(1, 'default', 'default', ''),
(2, 'assembly', 'Schrauben Bolzen etc', NULL),
(3, 'glue', 'Verbinder Kleber etc.', NULL),
(4, 'hour', 'Arbeitsstunde', ''),
(5, 'chemic', 'Farben Lacke Lösungsmittel', NULL),
(6, 'office', 'Büro', 'Verbrauch und Maschinen'),
(7, 'spare-parts', 'Ersatzteile', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_config`
--

CREATE TABLE IF NOT EXISTS `aes_config` (
  `key` varchar(50) NOT NULL,
  `value_int` int(11) DEFAULT NULL,
  `value_dec` decimal(15,5) DEFAULT NULL,
  `group` varchar(50) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `aes_config`
--

INSERT INTO `aes_config` (`key`, `value_int`, `value_dec`, `group`, `description`) VALUES
('pagcount', 30, NULL, 'view', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_config_finance`
--

CREATE TABLE IF NOT EXISTS `aes_config_finance` (
  `key` varchar(50) NOT NULL,
  `value` decimal(10,2) NOT NULL,
  `group` varchar(50) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `aes_config_finance`
--

INSERT INTO `aes_config_finance` (`key`, `value`, `group`, `description`) VALUES
('hour_office', 57.82, 'account', 'Preis einer Office Stunde'),
('hour_working', 62.40, 'account', 'Preis einer Werkstattstunde'),
('tax', 19.00, 'account', 'Umsatzsteuer in Prozent');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_cost_center`
--

CREATE TABLE IF NOT EXISTS `aes_cost_center` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Daten für Tabelle `aes_cost_center`
--

INSERT INTO `aes_cost_center` (`id`, `name`, `description`) VALUES
(1, 'Werkzeugbau', NULL),
(2, 'Zuschnitt', NULL),
(3, 'Maßanfertigung', ''),
(4, 'Reparatur', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_country`
--

CREATE TABLE IF NOT EXISTS `aes_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `iso_2` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=251 ;

--
-- Daten für Tabelle `aes_country`
--

INSERT INTO `aes_country` (`id`, `name`, `iso_2`) VALUES
(1, 'Afghanistan', 'AF'),
(2, 'Egypt', 'EG'),
(3, 'Aland Islands', 'AX'),
(4, 'Albania', 'AL'),
(5, 'Algeria', 'DZ'),
(6, 'American Samoa', 'AS'),
(7, 'Virgin Islands, U.s.', 'VI'),
(8, 'Andorra', 'AD'),
(9, 'Angola', 'AO'),
(10, 'Anguilla', 'AI'),
(11, 'Antarctica', 'AQ'),
(12, 'Antigua And Barbuda', 'AG'),
(13, 'Equatorial Guinea', 'GQ'),
(14, 'Argentina', 'AR'),
(15, 'Armenia', 'AM'),
(16, 'Aruba', 'AW'),
(17, 'Ascension', 'AC'),
(18, 'Azerbaijan', 'AZ'),
(19, 'Ethiopia', 'ET'),
(20, 'Australia', 'AU'),
(21, 'Bahamas', 'BS'),
(22, 'Bahrain', 'BH'),
(23, 'Bangladesh', 'BD'),
(24, 'Barbados', 'BB'),
(25, 'Belgium', 'BE'),
(26, 'Belize', 'BZ'),
(27, 'Benin', 'BJ'),
(28, 'Bermuda', 'BM'),
(29, 'Bhutan', 'BT'),
(30, 'Bolivia', 'BO'),
(31, 'Bosnia And Herzegovina', 'BA'),
(32, 'Botswana', 'BW'),
(33, 'Bouvet Island', 'BV'),
(34, 'Brazil', 'BR'),
(35, 'Virgin Islands, British', 'VG'),
(36, 'British Indian Ocean Territory', 'IO'),
(37, 'Brunei Darussalam', 'BN'),
(38, 'Bulgaria', 'BG'),
(39, 'Burkina Faso', 'BF'),
(40, 'Burundi', 'BI'),
(41, 'Chile', 'CL'),
(42, 'China', 'CN'),
(43, 'Cook Islands', 'CK'),
(44, 'Costa Rica', 'CR'),
(45, 'CÔte D''ivoire', 'CI'),
(46, 'Denmark', 'DK'),
(47, 'Germany', 'DE'),
(48, 'Saint Helena', 'SH'),
(49, 'Diego Garcia', 'DG'),
(50, 'Dominica', 'DM'),
(51, 'Dominican Republic', 'DO'),
(52, 'Djibouti', 'DJ'),
(53, 'Ecuador', 'EC'),
(54, 'El Salvador', 'SV'),
(55, 'Eritrea', 'ER'),
(56, 'Estonia', 'EE'),
(57, 'European Union', 'EU'),
(58, 'Falkland Islands (malvinas)', 'FK'),
(59, 'Faroe Islands', 'FO'),
(60, 'Fiji', 'FJ'),
(61, 'Finland', 'FI'),
(62, 'France', 'FR'),
(63, 'French Guiana', 'GF'),
(64, 'French Polynesia', 'PF'),
(65, 'French Southern Territories', 'TF'),
(66, 'Gabon', 'GA'),
(67, 'Gambia', 'GM'),
(68, 'Georgia', 'GE'),
(69, 'Ghana', 'GH'),
(70, 'Gibraltar', 'GI'),
(71, 'Grenada', 'GD'),
(72, 'Greece', 'GR'),
(73, 'Greenland', 'GL'),
(74, 'Guadeloupe', 'GP'),
(75, 'Guam', 'GU'),
(76, 'Guatemala', 'GT'),
(77, 'Guernsey', 'GG'),
(78, 'Guinea', 'GN'),
(79, 'Guinea-bissau', 'GW'),
(80, 'Guyana', 'GY'),
(81, 'Haiti', 'HT'),
(82, 'Heard Island And Mcdonald Islands', 'HM'),
(83, 'Honduras', 'HN'),
(84, 'Hong Kong', 'HK'),
(85, 'India', 'IN'),
(86, 'Indonesia', 'ID'),
(87, 'Isle Of Man', 'IM'),
(88, 'Iraq', 'IQ'),
(89, 'Iran, Islamic Republic Of', 'IR'),
(90, 'Ireland', 'IE'),
(91, 'Iceland', 'IS'),
(92, 'Israel', 'IL'),
(93, 'Italy', 'IT'),
(94, 'Jamaica', 'JM'),
(95, 'Japan', 'JP'),
(96, 'Yemen', 'YE'),
(97, 'Jersey', 'JE'),
(98, 'Jordan', 'JO'),
(99, 'Cayman Islands', 'KY'),
(100, 'Cambodia', 'KH'),
(101, 'Cameroon', 'CM'),
(102, 'Canada', 'CA'),
(103, 'Canary Islands', 'IC'),
(104, 'Cape Verde', 'CV'),
(105, 'Kazakhstan', 'KZ'),
(106, 'Qatar', 'QA'),
(107, 'Kenya', 'KE'),
(108, 'Kyrgyzstan', 'KG'),
(109, 'Kiribati', 'KI'),
(110, 'Cocos (keeling) Islands', 'CC'),
(111, 'Colombia', 'CO'),
(112, 'Comoros', 'KM'),
(113, 'Congo, The Democratic Republic Of The', 'CD'),
(114, 'Congo', 'CG'),
(115, 'Korea, Democratic People''s Republic Of', 'KP'),
(116, 'Korea, Republic Of', 'KR'),
(117, 'Croatia', 'HR'),
(118, 'Cuba', 'CU'),
(119, 'Kuwait', 'KW'),
(120, 'Lao People''s Democratic Republic', 'LA'),
(121, 'Lesotho', 'LS'),
(122, 'Latvia', 'LV'),
(123, 'Lebanon', 'LB'),
(124, 'Liberia', 'LR'),
(125, 'Libyan Arab Jamahiriya', 'LY'),
(126, 'Liechtenstein', 'LI'),
(127, 'Lithuania', 'LT'),
(128, 'Luxembourg', 'LU'),
(129, 'Macao', 'MO'),
(130, 'Madagascar', 'MG'),
(131, 'Malawi', 'MW'),
(132, 'Malaysia', 'MY'),
(133, 'Maldives', 'MV'),
(134, 'Mali', 'ML'),
(135, 'Malta', 'MT'),
(136, 'Morocco', 'MA'),
(137, 'Marshall Islands', 'MH'),
(138, 'Martinique', 'MQ'),
(139, 'Mauritania', 'MR'),
(140, 'Mauritius', 'MU'),
(141, 'Mayotte', 'YT'),
(142, 'Macedonia, The Former Yugoslav Republic Of', 'MK'),
(143, 'Mexico', 'MX'),
(144, 'Micronesia, Federated States Of', 'FM'),
(145, 'Moldova', 'MD'),
(146, 'Monaco', 'MC'),
(147, 'Mongolia', 'MN'),
(148, 'Montserrat', 'MS'),
(149, 'Mozambique', 'MZ'),
(150, 'Myanmar', 'MM'),
(151, 'Namibia', 'NA'),
(152, 'Nauru', 'NR'),
(153, 'Nepal', 'NP'),
(154, 'New Caledonia', 'NC'),
(155, 'New Zealand', 'NZ'),
(156, 'Saudi–Iraqi neutral zone', 'NT'),
(157, 'Nicaragua', 'NI'),
(158, 'Netherlands', 'NL'),
(159, 'Netherlands Antilles', 'AN'),
(160, 'Niger', 'NE'),
(161, 'Nigeria', 'NG'),
(162, 'Niue', 'NU'),
(163, 'Northern Mariana Islands', 'MP'),
(164, 'Norfolk Island', 'NF'),
(165, 'Norway', 'NO'),
(166, 'Oman', 'OM'),
(167, 'Austria', 'AT'),
(168, 'Pakistan', 'PK'),
(169, 'Palestinian Territory, Occupied', 'PS'),
(170, 'Palau', 'PW'),
(171, 'Panama', 'PA'),
(172, 'Papua New Guinea', 'PG'),
(173, 'Paraguay', 'PY'),
(174, 'Peru', 'PE'),
(175, 'Philippines', 'PH'),
(176, 'Pitcairn', 'PN'),
(177, 'Poland', 'PL'),
(178, 'Portugal', 'PT'),
(179, 'Puerto Rico', 'PR'),
(180, 'RÉunion', 'RE'),
(181, 'Rwanda', 'RW'),
(182, 'Romania', 'RO'),
(183, 'Russian Federation', 'RU'),
(184, 'Solomon Islands', 'SB'),
(185, 'Zambia', 'ZM'),
(186, 'Samoa', 'WS'),
(187, 'San Marino', 'SM'),
(188, 'Sao Tome And Principe', 'ST'),
(189, 'Saudi Arabia', 'SA'),
(190, 'Sweden', 'SE'),
(191, 'Switzerland', 'CH'),
(192, 'Senegal', 'SN'),
(193, 'Serbien und Montenegro', 'CS'),
(194, 'Seychelles', 'SC'),
(195, 'Sierra Leone', 'SL'),
(196, 'Zimbabwe', 'ZW'),
(197, 'Singapore', 'SG'),
(198, 'Slovakia', 'SK'),
(199, 'Slovenia', 'SI'),
(200, 'Somalia', 'SO'),
(201, 'Spain', 'ES'),
(202, 'Sri Lanka', 'LK'),
(203, 'Saint Kitts And Nevis', 'KN'),
(204, 'Saint Lucia', 'LC'),
(205, 'Saint Pierre And Miquelon', 'PM'),
(206, 'Saint Vincent And The Grenadines', 'VC'),
(207, 'South Africa', 'ZA'),
(208, 'Sudan', 'SD'),
(209, 'South Georgia And The South Sandwich Islands', 'GS'),
(210, 'Suriname', 'SR'),
(211, 'Svalbard And Jan Mayen', 'SJ'),
(212, 'Swaziland', 'SZ'),
(213, 'Syrian Arab Republic', 'SY'),
(214, 'Tajikistan', 'TJ'),
(215, 'Taiwan', 'TW'),
(216, 'Tanzania, United Republic Of', 'TZ'),
(217, 'Thailand', 'TH'),
(218, 'Timor-leste', 'TL'),
(219, 'Togo', 'TG'),
(220, 'Tokelau', 'TK'),
(221, 'Tonga', 'TO'),
(222, 'Trinidad And Tobago', 'TT'),
(223, 'Tristan da Cunha', 'TA'),
(224, 'Chad', 'TD'),
(225, 'Czech Republic', 'CZ'),
(226, 'Tunisia', 'TN'),
(227, 'Turkey', 'TR'),
(228, 'Turkmenistan', 'TM'),
(229, 'Turks And Caicos Islands', 'TC'),
(230, 'Tuvalu', 'TV'),
(231, 'Uganda', 'UG'),
(232, 'Ukraine', 'UA'),
(233, 'Soviet Union', 'SU'),
(234, 'Uruguay', 'UY'),
(235, 'Uzbekistan', 'UZ'),
(236, 'Vanuatu', 'VU'),
(237, 'Holy See (vatican City State)', 'VA'),
(238, 'Venezuela', 'VE'),
(239, 'United Arab Emirates', 'AE'),
(240, 'United States', 'US'),
(241, 'United Kingdom', 'GB'),
(242, 'Viet Nam', 'VN'),
(243, 'Wallis And Futuna', 'WF'),
(244, 'Christmas Island', 'CX'),
(245, 'Belarus', 'BY'),
(246, 'Western Sahara', 'EH'),
(247, 'Central African Republic', 'CF'),
(248, 'Cyprus', 'CY'),
(249, 'Hungary', 'HU'),
(250, 'Montenegro', 'ME');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_crm_actions`
--

CREATE TABLE IF NOT EXISTS `aes_crm_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'um Aktionen zu definieren die bei Customers als ausgeführt aufgeführt werden',
  `name` varchar(100) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_customer`
--

CREATE TABLE IF NOT EXISTS `aes_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_nr` varchar(45) NOT NULL DEFAULT '1',
  `group` int(11) NOT NULL DEFAULT '1',
  `name_first` varchar(45) DEFAULT NULL,
  `name_last` varchar(45) DEFAULT NULL,
  `name_company` varchar(100) DEFAULT NULL,
  `str` varchar(100) DEFAULT NULL,
  `str_nr` varchar(10) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `city` varchar(80) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `sec_name_first` varchar(45) DEFAULT NULL,
  `sec_name_last` varchar(45) DEFAULT NULL,
  `sec_name_company` varchar(100) DEFAULT NULL,
  `sec_str` varchar(100) DEFAULT NULL,
  `sec_str_nr` varchar(10) DEFAULT NULL,
  `sec_zip` varchar(10) DEFAULT NULL,
  `sec_city` varchar(80) DEFAULT NULL,
  `sec_country` int(11) DEFAULT NULL,
  `executed_crm_actions` text COMMENT 'JSON',
  `tel` varchar(80) DEFAULT NULL,
  `tel_mobile` varchar(80) DEFAULT NULL,
  `tel_fax` varchar(80) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customer_nr_UNIQUE` (`customer_nr`),
  KEY `fk_aes_customer_country_idx` (`country`),
  KEY `fk_aes_customer_country_2ed_idx` (`sec_country`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Daten für Tabelle `aes_customer`
--

INSERT INTO `aes_customer` (`id`, `customer_nr`, `group`, `name_first`, `name_last`, `name_company`, `str`, `str_nr`, `zip`, `city`, `country`, `sec_name_first`, `sec_name_last`, `sec_name_company`, `sec_str`, `sec_str_nr`, `sec_zip`, `sec_city`, `sec_country`, `executed_crm_actions`, `tel`, `tel_mobile`, `tel_fax`, `email`) VALUES
(1, '11166639', 2, 'Stefan', 'Weidner', 'Onkelz', NULL, NULL, '65434', 'Frankfurt', 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'stefan@onkelz.de'),
(2, '2345973', 2, 'Gonzo', 'Kabellu', 'Onkelz', NULL, NULL, '56709', 'Bern', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'gonzo@onkelz.de'),
(3, '38476521', 1, 'Klaus', 'Keiser', 'keine', NULL, NULL, '44011', 'Wuppertal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '83463521', 1, 'Hänsel', 'Gretelbruder', '', 'Brunnenstraße', '11', '48271', 'Alsfeld', 1, 'so', '', '', '', '', '', '', 67, NULL, NULL, NULL, NULL, NULL),
(5, 'kd-456', 1, 'Gretelx', 'Kekse', 'DIE Firma', 'Totenhofweg', '12', '32488', 'Marsdorf', 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'kd-00287', 1, 'Markus', 'Wulf', 'AngstUndBange', 'Karnacksweg', '6', '12345', 'Bulzig', 47, '', '', '', '', '', '', '', 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_customer_group`
--

CREATE TABLE IF NOT EXISTS `aes_customer_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Daten für Tabelle `aes_customer_group`
--

INSERT INTO `aes_customer_group` (`id`, `alias`, `name`, `description`) VALUES
(1, 'default', 'Standard', NULL),
(2, 'friends', 'Freunde', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_delivery`
--

CREATE TABLE IF NOT EXISTS `aes_delivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `datetime_creation` datetime DEFAULT NULL,
  `datetime_start` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_disposition_article`
--

CREATE TABLE IF NOT EXISTS `aes_disposition_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `article_count` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `discharge` int(1) NOT NULL DEFAULT '0' COMMENT '0=neu;1=ja;2=cancel',
  `datetime_creation` datetime DEFAULT NULL,
  `datetime_last_change` datetime DEFAULT NULL COMMENT 'dann auch finish time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_disposition_factory_article`
--

CREATE TABLE IF NOT EXISTS `aes_disposition_factory_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `factory_article_id` int(11) NOT NULL,
  `factory_article_count` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `discharge` int(1) NOT NULL DEFAULT '0' COMMENT '0=neu;1=ja;2=cancel',
  `datetime_creation` datetime DEFAULT NULL,
  `datetime_last_change` datetime DEFAULT NULL COMMENT 'dann auch finish time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_disposition_material`
--

CREATE TABLE IF NOT EXISTS `aes_disposition_material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `material_id` int(11) NOT NULL,
  `material_amount` decimal(15,5) NOT NULL DEFAULT '0.00000',
  `order_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `discharge` int(1) NOT NULL DEFAULT '0' COMMENT '0=neu;1=ja;2=cancel',
  `datetime_creation` datetime DEFAULT NULL,
  `datetime_last_change` datetime DEFAULT NULL COMMENT 'dann auch finish time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Daten für Tabelle `aes_disposition_material`
--

INSERT INTO `aes_disposition_material` (`id`, `material_id`, `material_amount`, `order_id`, `employee_id`, `discharge`, `datetime_creation`, `datetime_last_change`) VALUES
(1, 2, 5.00000, NULL, 1, 0, NULL, NULL),
(2, 5, 3.00000, NULL, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_employee`
--

CREATE TABLE IF NOT EXISTS `aes_employee` (
  `user_id` int(11) NOT NULL,
  `employee_nr` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_factory_article`
--

CREATE TABLE IF NOT EXISTS `aes_factory_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `factory_article_nr` varchar(45) NOT NULL DEFAULT 'nn',
  `name` varchar(45) DEFAULT NULL,
  `group` int(11) NOT NULL DEFAULT '1',
  `measure_scale_geom` decimal(10,2) DEFAULT NULL COMMENT 'quantity of measure_identifier',
  `measure_symbol_geom` int(11) DEFAULT NULL COMMENT 'm, cm, kg, ccm',
  `measure_scale_weight` decimal(10,2) DEFAULT NULL COMMENT 'based on measure_default_size',
  `measure_symbol_weight` int(11) DEFAULT NULL COMMENT 'measure identifier only for weight_value, may differ from measure_identifier',
  `price_sell` decimal(10,2) DEFAULT NULL,
  `description` tinytext,
  `articles` mediumtext COMMENT 'JSON',
  `materials` mediumtext COMMENT 'JSON',
  `services` mediumtext COMMENT 'JSON',
  `factory_articles` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Daten für Tabelle `aes_factory_article`
--

INSERT INTO `aes_factory_article` (`id`, `factory_article_nr`, `name`, `group`, `measure_scale_geom`, `measure_symbol_geom`, `measure_scale_weight`, `measure_symbol_weight`, `price_sell`, `description`, `articles`, `materials`, `services`, `factory_articles`) VALUES
(1, 'fa-0001', 'GS 1000 V2A 4in1', 2, 2.00, 6, 4.50, 3, 2500.00, 'ne geile 4in1 für die dicke Suzi', '{"1":2,"2":1,"6":1}', '{"5":2.8,"6":1.2}', '{"1":1.2}', NULL),
(2, 'fa-0002', 'GS 750 V2A 4in1', 2, NULL, NULL, NULL, NULL, 2500.00, 'ne geile 4in1 für die Suzi', NULL, NULL, NULL, NULL),
(3, 'fa-0003', 'Scheinwerfer 20d', 2, NULL, NULL, NULL, NULL, 342.00, 'Scheinwerfer Custom rund Durchm 20', NULL, NULL, NULL, NULL),
(4, 'fa-0004', 'Scheinwerfer 20x10', 2, NULL, NULL, NULL, NULL, 350.00, 'Scheinwerfer Custom eckig 20 x 10', NULL, NULL, NULL, NULL),
(5, 'fa-0062', 'GS 1000 Bürzel Gnome', 2, NULL, NULL, NULL, NULL, 164.00, 'Suzi gs1000 Bürzel Glasfaser Model:Gnome', NULL, NULL, NULL, NULL),
(6, 'fa-0063', 'GS 1000 Seitendeckel Gnome', 2, NULL, NULL, NULL, NULL, 195.00, 'Suzi gs1000 Seitendeckel 2x Glasfaser Model:Gnome', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_factory_article_group`
--

CREATE TABLE IF NOT EXISTS `aes_factory_article_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(45) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Daten für Tabelle `aes_factory_article_group`
--

INSERT INTO `aes_factory_article_group` (`id`, `alias`, `name`, `description`) VALUES
(1, 'default', 'default', NULL),
(2, 'custom-bikes', 'Custom Bike', NULL),
(3, 'machinery', 'Maschinenbau', NULL),
(4, 'construct', 'Konstruirtes', 'vielleicht auch Messebau, Gerüste etc');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_factory_order`
--

CREATE TABLE IF NOT EXISTS `aes_factory_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `factory_order_nr` varchar(45) NOT NULL DEFAULT 'nn',
  `factory_article_id` int(11) NOT NULL,
  `factory_article_count` int(11) NOT NULL DEFAULT '1',
  `location` int(11) DEFAULT NULL,
  `employee_id` int(11) NOT NULL DEFAULT '0',
  `datetime_creation` datetime DEFAULT NULL,
  `datetime_last_change` datetime DEFAULT NULL,
  `datetime_start` datetime DEFAULT NULL COMMENT 'first factory-order for thie product-order combi',
  `datetime_finish` datetime DEFAULT NULL,
  `datetime_cancel` datetime DEFAULT NULL,
  `cancel_reason` tinytext,
  `comment` tinytext,
  `cost_center` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Daten für Tabelle `aes_factory_order`
--

INSERT INTO `aes_factory_order` (`id`, `factory_order_nr`, `factory_article_id`, `factory_article_count`, `location`, `employee_id`, `datetime_creation`, `datetime_last_change`, `datetime_start`, `datetime_finish`, `datetime_cancel`, `cancel_reason`, `comment`, `cost_center`) VALUES
(1, 'ba-1', 1, 0, 2, 2, '2014-02-01 13:40:00', '2014-02-02 11:07:09', NULL, NULL, NULL, NULL, NULL, 1),
(4, 'ba-2', 2, 0, 3, 2, '2014-02-02 11:26:56', '2014-03-06 04:54:39', NULL, NULL, NULL, NULL, 'aktuell KPx', 1),
(5, 'ba-3', 3, 0, NULL, 2, '2014-02-06 17:11:07', '2014-02-08 15:51:13', NULL, NULL, NULL, NULL, NULL, 1),
(6, 'ba-6', 4, 0, NULL, 2, '2014-02-21 21:26:56', '2014-02-23 10:12:30', NULL, NULL, NULL, NULL, NULL, 1),
(7, 'nnx', 5, 0, 2, 2, '2014-02-23 12:45:21', '2014-02-23 15:44:08', NULL, NULL, NULL, NULL, '', 1),
(8, 'nn', 6, 0, NULL, 2, '2014-02-23 12:54:25', NULL, NULL, NULL, NULL, NULL, NULL, 1),
(9, 'nn', 4, 0, NULL, 2, '2014-02-23 16:47:54', NULL, NULL, NULL, NULL, NULL, NULL, 1),
(10, 'ba-10', 3, 0, 3, 2, '2014-02-23 16:49:24', '2014-02-23 16:50:27', NULL, NULL, NULL, NULL, 'geht bald los', 1),
(11, 'nn', 2, 0, NULL, 2, '2014-02-25 20:16:02', NULL, NULL, NULL, NULL, NULL, NULL, 1),
(12, 'nn', 5, 0, NULL, 2, '2014-02-27 22:17:46', NULL, NULL, NULL, NULL, NULL, NULL, 1),
(13, 'nn', 1, 0, NULL, 2, '2014-02-28 09:01:13', NULL, NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_lang_de`
--

CREATE TABLE IF NOT EXISTS `aes_lang_de` (
  `key` varchar(100) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `aes_lang_de`
--

INSERT INTO `aes_lang_de` (`key`, `value`) VALUES
('16text_display_name', 'xy'),
('16text_long', ''),
('16text_short', ''),
('17text_display_name', 'unser Freitag Produkt'),
('17text_long', '<p>Als wir damals.... da gab es auch einen <strong>Freitag</strong> ...und dann aben wir... ...und machten ein Freitag produkt</p>\r\n'),
('17text_short', '<p>eins das am Freitag entstandx</p>\r\n'),
('18text_display_name', 'x'),
('18text_long', ''),
('18text_short', ''),
('19text_display_name', ''),
('19text_long', ''),
('19text_short', '<p><img alt="sad" src="http://zend.local/js/ckeditor_full/plugins/smiley/images/sad_smile.png" style="height:23px; width:23px" title="sad" /></p>\r\n'),
('21text_display_name', 'nix'),
('21text_long', ''),
('21text_short', '<div class="testclass" style="background:#eee;border:1px solid #ccc;padding:5px 10px;">\r\n<p>mal was im special DIV container</p>\r\n</div>\r\n'),
('27text_display_name', 'Müll von den Had-Bangern'),
('27text_long', ''),
('27text_short', ''),
('test', 'deutscher Testtext :) aus aesTranslate()');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_lang_en`
--

CREATE TABLE IF NOT EXISTS `aes_lang_en` (
  `key` varchar(100) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `aes_lang_en`
--

INSERT INTO `aes_lang_en` (`key`, `value`) VALUES
('test', 'englisch testtext');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_location`
--

CREATE TABLE IF NOT EXISTS `aes_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `address_street` varchar(100) DEFAULT NULL,
  `address_city` varchar(80) DEFAULT NULL,
  `address_zip` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Daten für Tabelle `aes_location`
--

INSERT INTO `aes_location` (`id`, `name`, `address_street`, `address_city`, `address_zip`) VALUES
(1, 'Hagen', NULL, NULL, NULL),
(2, 'Dortmund', NULL, NULL, NULL),
(3, 'Kassel', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_mail`
--

CREATE TABLE IF NOT EXISTS `aes_mail` (
  `id` int(11) NOT NULL,
  `datetime_send` datetime DEFAULT NULL,
  `subject` tinytext,
  `text` mediumtext,
  `customers` text COMMENT 'CSV with IDs',
  `suppliers` text COMMENT 'CSV with IDs',
  `users` text COMMENT 'CSV with IDs',
  `group` int(11) DEFAULT NULL COMMENT 'user_group ID',
  `reason` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_material`
--

CREATE TABLE IF NOT EXISTS `aes_material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `group` int(11) NOT NULL DEFAULT '1',
  `supplier` int(11) DEFAULT NULL COMMENT 'can be myself',
  `measure_scale_geom` decimal(10,2) DEFAULT NULL COMMENT 'quantity of measure_identifier',
  `measure_symbol_geom` int(11) DEFAULT NULL COMMENT 'm, cm, kg, ccm',
  `measure_scale_weight` decimal(10,2) DEFAULT NULL COMMENT 'based on measure_default_size',
  `measure_symbol_weight` int(11) DEFAULT NULL COMMENT 'measure identifier only for weight_value, may differ from measure_identifier',
  `price_buy` decimal(10,2) DEFAULT NULL COMMENT 'based on measure_default_size',
  `price_sell` decimal(10,2) DEFAULT NULL,
  `description` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Daten für Tabelle `aes_material`
--

INSERT INTO `aes_material` (`id`, `name`, `group`, `supplier`, `measure_scale_geom`, `measure_symbol_geom`, `measure_scale_weight`, `measure_symbol_weight`, `price_buy`, `price_sell`, `description`) VALUES
(1, '40x10 Flachstahl', 3, 3, 100.00, 1, 2.20, 3, 1.00, 3.20, 'St42'),
(2, '60x10 Flachstahl', 3, 3, 100.00, 1, 3.00, 3, 2.40, 4.50, 'lange dünne Eisenstreifen'),
(3, '1.2 Kupferblech', 2, 10, 0.50, 5, 0.70, 3, 7.44, 8.50, '1,2 mm Kupferblech'),
(4, '40x10 U-Profil', 3, 3, 1.00, 4, 0.60, 3, 6.40, 14.75, 'Alu 40 breit'),
(5, '0.8 Blech Stahl', 2, 3, 1.00, 5, 1.20, 3, 0.47, 0.89, 'Karosserieblech'),
(6, '1.0 V2A Blech', 2, 10, 1.00, 4, 2.30, 3, 23.00, 47.20, ''),
(7, '34x1.2 V2A Rohr', 3, 11, 1.00, 4, 1.60, 3, 32.70, 46.90, ''),
(8, '46x1.2 V2A Rohr', 3, 11, 1.00, 4, 1.70, 3, 38.90, 49.00, '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_material_group`
--

CREATE TABLE IF NOT EXISTS `aes_material_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` tinytext,
  `stock` int(1) NOT NULL DEFAULT '1' COMMENT 'z.B. Beton was erst vor Ort geliefert wird = 0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Daten für Tabelle `aes_material_group`
--

INSERT INTO `aes_material_group` (`id`, `alias`, `name`, `description`, `stock`) VALUES
(1, 'default', 'default', '', 1),
(2, 'blech', 'Blech', 'dünnes Zeugz', 1),
(3, 'profile-metal', 'Profile Metall', NULL, 1),
(4, 'profile-oil', 'Profile Kunststoff', NULL, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_offer`
--

CREATE TABLE IF NOT EXISTS `aes_offer` (
  `id` int(11) NOT NULL,
  `offer_nr` varchar(45) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `products` text COMMENT 'JSON',
  `datetime_creation` datetime DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `description` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_order`
--

CREATE TABLE IF NOT EXISTS `aes_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_nr` varchar(45) NOT NULL COMMENT 'Auftragsnummer intern',
  `customer_id` int(11) NOT NULL,
  `products` text COMMENT 'JSON',
  `datetime_creation` datetime DEFAULT NULL,
  `datetime_last_change` datetime DEFAULT NULL,
  `datetime_start` datetime DEFAULT NULL,
  `datetime_finish` datetime DEFAULT NULL,
  `datetime_cancel` datetime DEFAULT NULL,
  `employee_id` int(11) NOT NULL DEFAULT '0',
  `cost_center` int(11) NOT NULL DEFAULT '0',
  `shipment` int(11) unsigned NOT NULL DEFAULT '1',
  `description` tinytext,
  `fixed` int(1) NOT NULL DEFAULT '0',
  `user_create` int(11) NOT NULL DEFAULT '0',
  `user_last_editor` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_nr_UNIQUE` (`order_nr`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Daten für Tabelle `aes_order`
--

INSERT INTO `aes_order` (`id`, `order_nr`, `customer_id`, `products`, `datetime_creation`, `datetime_last_change`, `datetime_start`, `datetime_finish`, `datetime_cancel`, `employee_id`, `cost_center`, `shipment`, `description`, `fixed`, `user_create`, `user_last_editor`) VALUES
(1, '2014-0001x', 5, '{"16":2,"18":1,"19":1}', '2014-01-21 13:44:52', '2014-06-25 11:19:01', NULL, NULL, NULL, 2, 2, 1, '', 0, 2, 2),
(2, '2014-0002x', 4, '{"13":1,"14":1,"19":3,"12":2}', '2014-01-22 13:44:52', '2015-06-13 10:44:35', NULL, NULL, NULL, 2, 1, 3, 'eines der ersten', 0, 0, 1),
(3, '2014-0003', 6, '{"16":2,"15":3}', '2014-01-24 13:54:10', '2014-03-08 11:23:13', NULL, NULL, NULL, 2, 1, 1, 'der dritte Order', 0, 0, 0),
(4, '2014-0004', 3, '', '2014-01-30 13:34:56', '2014-01-30 14:26:26', NULL, NULL, NULL, 2, 2, 1, '', 0, 0, 0),
(5, '2014-0007', 4, '{"14":2}', '2014-01-30 14:27:27', '2014-02-27 22:17:46', NULL, NULL, NULL, 2, 4, 1, 'kleines mit Harbsch', 0, 0, 0),
(6, 'auf-12345', 1, '{"14":1}', '2014-02-16 19:49:24', '2014-02-16 19:49:35', NULL, NULL, NULL, 2, 4, 1, '', 0, 0, 0),
(7, '2008-wurb12', 2, '{"12":1,"19":1}', '2014-02-21 19:59:15', '2014-02-22 09:17:27', NULL, NULL, NULL, 2, 4, 1, 'repair Wurb', 0, 0, 0),
(8, 'auf-0987682', 1, '{"21":1}', '2014-03-07 17:01:18', '2014-03-07 18:31:21', NULL, NULL, NULL, 2, 3, 1, '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_product`
--

CREATE TABLE IF NOT EXISTS `aes_product` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_nr` varchar(45) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `group` int(11) unsigned NOT NULL DEFAULT '1',
  `description` tinytext,
  `price` decimal(10,2) DEFAULT NULL COMMENT 'without materials.price',
  `articles` mediumtext COMMENT 'JSON',
  `materials` mediumtext COMMENT 'JSON',
  `services` mediumtext COMMENT 'JSON',
  `factory_articles` mediumtext,
  `shipment` int(11) unsigned NOT NULL DEFAULT '1',
  `text_display_name` varchar(100) DEFAULT NULL COMMENT 'key for lang tables (product_id+fieldname)',
  `text_short` varchar(100) DEFAULT NULL COMMENT 'key for lang tables (product_id+fieldname)',
  `text_long` varchar(100) DEFAULT NULL COMMENT 'key for lang tables (product_id+fieldname)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Daten für Tabelle `aes_product`
--

INSERT INTO `aes_product` (`id`, `product_nr`, `name`, `group`, `description`, `price`, `articles`, `materials`, `services`, `factory_articles`, `shipment`, `text_display_name`, `text_short`, `text_long`) VALUES
(12, '1234', 'Zwölfere', 3, '', 44.80, '{"1":4,"2":2,"3":2,"8":6}', '{"1":7.2,"3":1.4,"2":2.2,"4":3.2,"5":1.4,"6":6.5}', NULL, '{"3":1,"6":1}', 1, '12text_display_name', '12text_short', '12text_long'),
(13, NULL, 'test', 1, '', 22.00, '', '', NULL, NULL, 1, '13text_display_name', '13text_short', '13text_long'),
(14, NULL, 'Harbsch', 1, '', 22.00, '{"1":1}', '{"2":1}', NULL, NULL, 1, '14text_display_name', '14text_short', '14text_long'),
(15, NULL, 'wrwe', 1, '', 22.00, '', '', NULL, NULL, 1, NULL, NULL, NULL),
(16, NULL, 'Tagessupperx', 3, 'Suppe 01', 2.50, '{"3":1}', '{"2":2,"3":1.2}', NULL, '{"3":1}', 1, '16text_display_name', '16text_short', '16text_long'),
(17, NULL, 'Freitagx', 3, '', 22.00, '{"3":1}', '{"5":7.5,"3":1.2}', NULL, NULL, 1, '17text_display_name', '17text_short', '17text_long'),
(18, 'PD-00074', 'Fr. Nachmittag', 3, '', 18.00, '{"1":2,"2":1}', '{"3":1.2,"4":2.3,"2":4}', '{"1":2.1}', '{"5":1,"1":1}', 1, '18text_display_name', '18text_short', '18text_long'),
(19, NULL, 'Sonntagsprodukt', 2, '', 33.00, '{"1":2}', '{"1":2}', NULL, NULL, 1, '19text_display_name', '19text_short', '19text_long'),
(21, '12345', 'Suzuki GS 850 Streetfighter', 3, 'Suzuki GS 850 Streetfighter Ausführung', 55.00, '{"5":4,"2":3,"11":1}', '{"6":2,"5":0.2,"2":1.2}', '{"1":3}', NULL, 1, '21text_display_name', '21text_short', '21text_long'),
(26, 'PD-00072', 'biggest Thing', 1, 'recht groß', 0.00, '{"4":1}', '{"3":2.6,"4":22.5}', '{"1":4.3}', NULL, 3, '26text_display_name', '26text_short', '26text_long'),
(27, NULL, 'Metallica trash', 2, 'Tonne für Metallica', 0.00, '{"5":1,"1":1}', '{"6":2}', NULL, NULL, 1, '27text_display_name', '27text_short', '27text_long');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_product_group`
--

CREATE TABLE IF NOT EXISTS `aes_product_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Daten für Tabelle `aes_product_group`
--

INSERT INTO `aes_product_group` (`id`, `alias`, `name`, `description`) VALUES
(1, 'default', 'default', ''),
(2, 'work', 'Arbeit', 'default Arbeit in Stunden'),
(3, 'production', 'Produktion', 'etwas aus der Produktion');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_service`
--

CREATE TABLE IF NOT EXISTS `aes_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_nr` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `group` int(11) NOT NULL DEFAULT '1',
  `price_buy` decimal(10,2) DEFAULT NULL,
  `price_sell` decimal(10,2) DEFAULT NULL,
  `description` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Daten für Tabelle `aes_service`
--

INSERT INTO `aes_service` (`id`, `service_nr`, `name`, `group`, `price_buy`, `price_sell`, `description`) VALUES
(1, 's-0001', 'Hiwi Stunde', 1, 8.50, 22.00, 'der Hiwi Kram');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_service_group`
--

CREATE TABLE IF NOT EXISTS `aes_service_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(45) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Daten für Tabelle `aes_service_group`
--

INSERT INTO `aes_service_group` (`id`, `alias`, `name`, `description`) VALUES
(1, 'default', 'Standart', NULL),
(2, 'cutting', 'Zuschnitt', NULL),
(3, 'assembling', 'Zusammenbau', NULL),
(4, 'welding', 'Schweissen', NULL),
(5, 'painting', 'Lackieren', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_shipment`
--

CREATE TABLE IF NOT EXISTS `aes_shipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `group` int(11) NOT NULL DEFAULT '1',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Daten für Tabelle `aes_shipment`
--

INSERT INTO `aes_shipment` (`id`, `name`, `group`, `price`) VALUES
(1, 'kein Versand', 1, 0.00),
(3, 'Hermes Päckchen', 1, 4.00);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_shipment_group`
--

CREATE TABLE IF NOT EXISTS `aes_shipment_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Daten für Tabelle `aes_shipment_group`
--

INSERT INTO `aes_shipment_group` (`id`, `name`, `description`) VALUES
(1, 'default', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_stock_article`
--

CREATE TABLE IF NOT EXISTS `aes_stock_article` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `count` int(11) NOT NULL DEFAULT '0' COMMENT 'auch negativ um Fehlverhalten aufzufangen',
  `pos_frame` varchar(45) NOT NULL,
  `pos_row` varchar(45) NOT NULL,
  `pos_field` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_stock_factory_article`
--

CREATE TABLE IF NOT EXISTS `aes_stock_factory_article` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `factory_article_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `count` int(11) NOT NULL DEFAULT '0' COMMENT 'auch negativ um Fehlverhalten aufzufangen',
  `pos_frame` varchar(45) NOT NULL,
  `pos_row` varchar(45) NOT NULL,
  `pos_field` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_stock_material`
--

CREATE TABLE IF NOT EXISTS `aes_stock_material` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `material_id` int(11) NOT NULL COMMENT 'product_id | material_id',
  `location_id` int(11) NOT NULL,
  `count` decimal(15,5) NOT NULL DEFAULT '0.00000' COMMENT 'auch negativ um Fehlverhalten aufzufangen',
  `pos_frame` varchar(45) NOT NULL,
  `pos_row` varchar(45) NOT NULL,
  `pos_field` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_supplier`
--

CREATE TABLE IF NOT EXISTS `aes_supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `str` varchar(100) DEFAULT NULL,
  `str_nr` varchar(10) DEFAULT NULL,
  `group` int(11) DEFAULT NULL,
  `rating` int(2) NOT NULL DEFAULT '0',
  `rating_notice` tinytext,
  `description` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Daten für Tabelle `aes_supplier`
--

INSERT INTO `aes_supplier` (`id`, `name`, `str`, `str_nr`, `group`, `rating`, `rating_notice`, `description`) VALUES
(1, 'self', NULL, NULL, NULL, 0, NULL, NULL),
(2, 'ABC Metallwaren', 'Induweg', '44', NULL, 0, NULL, NULL),
(3, 'Metall Schwerte', 'Hagenerstr', '82', NULL, 0, NULL, NULL),
(4, 'Schulz Lacke', 'Sedanstr', '12', NULL, 0, NULL, NULL),
(5, 'Tool Master', 'Eckeseyerstr', '230 - 238', NULL, 0, NULL, NULL),
(6, 'PaperPaul', 'Eckeseyerstr', '311 - 315', NULL, 0, NULL, 'Büroartikel'),
(7, 'Office Jack', 'Am großen Wall', '16', NULL, 0, NULL, 'Office Verbrauch und Maschinen'),
(8, 'N & B Metall Group', 'Niederflur', '51', 0, 0, NULL, 'Metallwaren'),
(9, 'OilSub', 'Hacking Allee', '72-74', NULL, 0, NULL, 'Kunststoff und Verbundwerkstoffe'),
(10, 'Nichteisen Jakob', 'Kölnerstr', '312 - 318', NULL, 0, NULL, 'Kupfer Alu etc'),
(11, 'Edeleisen Jupp', 'Völknerstraße', '261-263', NULL, 0, NULL, 'Edelstahl Profile und Blech');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_supplier_group`
--

CREATE TABLE IF NOT EXISTS `aes_supplier_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_symbol_geom`
--

CREATE TABLE IF NOT EXISTS `aes_symbol_geom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `symbol` varchar(20) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` tinytext,
  `multiplikator` decimal(18,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Daten für Tabelle `aes_symbol_geom`
--

INSERT INTO `aes_symbol_geom` (`id`, `symbol`, `name`, `description`, `multiplikator`) VALUES
(1, 'cm', NULL, NULL, NULL),
(2, 'cm²', NULL, NULL, NULL),
(3, 'cm³', NULL, NULL, NULL),
(4, 'm', NULL, NULL, NULL),
(5, 'm²', NULL, NULL, NULL),
(6, 'm³', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aes_symbol_weight`
--

CREATE TABLE IF NOT EXISTS `aes_symbol_weight` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `symbol` varchar(20) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` tinytext,
  `multiplikator` decimal(18,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Daten für Tabelle `aes_symbol_weight`
--

INSERT INTO `aes_symbol_weight` (`id`, `symbol`, `name`, `description`, `multiplikator`) VALUES
(1, 'mg', NULL, NULL, NULL),
(2, 'g', NULL, NULL, NULL),
(3, 'kg', NULL, NULL, NULL),
(4, 'T', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `album`
--

CREATE TABLE IF NOT EXISTS `album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `artist` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Daten für Tabelle `album`
--

INSERT INTO `album` (`id`, `artist`, `title`) VALUES
(1, 'Onkelz', 'es ist soweit');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ashop_product`
--

CREATE TABLE IF NOT EXISTS `ashop_product` (
  `id_product` int(11) NOT NULL,
  `active` int(1) unsigned NOT NULL DEFAULT '1',
  `seo_alias` tinytext,
  `seo_keywords` text,
  `seo_description` text,
  `bestseller` int(1) unsigned NOT NULL DEFAULT '1',
  `click_count` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bitkorn_cloud_file`
--

CREATE TABLE IF NOT EXISTS `bitkorn_cloud_file` (
  `id` int(11) NOT NULL,
  `filename` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bitkorn_cloud_icon`
--

CREATE TABLE IF NOT EXISTS `bitkorn_cloud_icon` (
  `key` varchar(20) NOT NULL,
  `filename` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `bitkorn_cloud_icon`
--

INSERT INTO `bitkorn_cloud_icon` (`key`, `filename`, `title`) VALUES
('application/pdf', 'application-pdf.png', 'PDF'),
('folder', 'folder.png', 'Verzeichnis'),
('home', 'go-home-32.png', 'Home'),
('image/jpeg', 'image-x-generic.png', 'Bild'),
('image/png', 'image-x-generic.png', 'Bild'),
('js', 'text-x-script.png', 'JavaScript'),
('json', 'text-x-script.png', 'JSON'),
('text/html', 'text-html.png', 'HTML'),
('text/plain', 'text-x-generic.png', 'Datei');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bitkorn_content`
--

CREATE TABLE IF NOT EXISTS `bitkorn_content` (
  `id` int(11) NOT NULL,
  `category` int(11) NOT NULL DEFAULT '0',
  `alias` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `title` tinytext NOT NULL,
  `content` mediumtext NOT NULL,
  `meta_title` tinytext,
  `menu_id` int(11) NOT NULL DEFAULT '0',
  `parent_content_id` int(11) NOT NULL DEFAULT '0',
  `meta_description` text,
  `meta_keywords` text,
  `datetime_creation` datetime DEFAULT NULL,
  `datetime_edited` datetime DEFAULT NULL,
  `startseite` int(1) DEFAULT '0',
  `active` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `bitkorn_content`
--

INSERT INTO `bitkorn_content` (`id`, `category`, `alias`, `name`, `title`, `content`, `meta_title`, `menu_id`, `parent_content_id`, `meta_description`, `meta_keywords`, `datetime_creation`, `datetime_edited`, `startseite`, `active`) VALUES
(0, 17, 'onkelz', 'Böhse Onkelz', 'nette Männer', '<p>Trotz zahlreicher Distanzierungen wurden den Onkelz &ouml;fter neonazistische Tendenzen vorgeworfen. Dabei wird insbesondere das Lied T&uuml;rken raus aus dem Jahr 1981 angef&uuml;hrt. In diesem St&uuml;ck, das die Band in ihrer Punk-Phase aufnahm, finden sich Zeilen wie: &bdquo;T&uuml;rkenpack, raus aus unserm Land, geht zur&uuml;ck nach Ankara, denn ihr macht mich krank.&ldquo; Nach Angaben der Onkelz ist das Lied als Reaktion auf eine Gruppe verfeindeter t&uuml;rkischer Jugendlicher entstanden, mit der sie h&auml;ufig in Schl&auml;gereien verwickelt gewesen seien.</p>', 'Böhse Onkelz', 0, 0, 'die wirklich beste Band', 'onkelz,deutschrock', '2013-11-30 19:25:31', NULL, 0, 1),
(1, 3, 'aldi', 'Aldi', 'bei Aldi Nord', '<p>etwas Testtext f&uuml;r Aldi ...kein HTML</p>', 'ein Test', 2, 0, 'dies ist nur eine testseite', 'test,testen,getestet', '2013-10-11 17:07:00', '2014-01-17 18:26:37', 1, 1),
(5, 4, 'meine-rosen', 'Rosen', 'meine Rosen', '<p>Heute war es toll und wir fahren wieder dort hin.y</p>', '', 1, 0, '', '', '2013-10-15 18:04:57', '2013-10-27 00:30:20', 0, 0),
(6, 17, 'lexieintrag1', 'ein LexiEintrag', 'erster Lexi Eintrag', '<p>wieder TestText ...sollte man sich was aufe taste legen.</p>', '', 0, 0, '', '', '2013-10-26 14:44:10', '2013-11-03 11:14:06', 0, 1),
(8, 17, 'wiki-eintrag', 'Wiki Eintragx', 'Ein Wiki Eintrag', '<p>ganz viel Wissen</p>', 'ein meta title', 1, 0, 'datt ist Wiki Content', 'wiki,speziell,wurbel', '2013-10-26 22:16:52', '2013-11-03 12:58:04', 1, 1),
(9, 15, 'lexi-klein-entry', 'klein Lexi Entry', 'ein Eintragstitel', '<p>Die Cabo Machichaco (fr&uuml;herer Name: Benisaf) war ein 1882 erbautes Frachtschiff. Urspr&uuml;nglich als Kohlen&shy;transporter eingesetzt, wurde es nach dem Verkauf an das Unternehmen Ybarra y Co. aus Sevilla 1885 zum St&uuml;ckgut&shy;schiff umgebaut und in Cabo Machichaco umbenannt.x</p>', '', 1, 0, '', '', '2013-11-03 14:03:35', '2014-11-27 10:21:03', 0, 1),
(10, 18, 'gross-lexi-entry', 'LexiGrossEntry', 'ein Titel für Große', '<p>Die entstandene Druckwelle verursachte ein Erdbeben, welches noch in acht Kilometern Entfernung erfasst wurde. Es war die gr&ouml;&szlig;te zivile Katastrophe im Spanien des 19. Jahrhunderts. W&auml;hrend Arbeiter sp&auml;ter versuchten, den noch im Schiff verbliebenen Sprengstoff zu entfernen, ereignete sich am 21. M&auml;rz 1894 eine zweite Explosion mit 15 Todesopfern.</p>\r\n<p style="font-size: 1.2em;">blah</p>', '', 1, 0, '', '', '2013-11-03 14:13:36', '2013-12-30 08:57:44', 0, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bitkorn_content_categories`
--

CREATE TABLE IF NOT EXISTS `bitkorn_content_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(45) NOT NULL COMMENT 'dient als route',
  `name` varchar(45) DEFAULT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `depth` int(11) NOT NULL,
  `route` varchar(100) NOT NULL DEFAULT '',
  `meta_title` tinytext,
  `meta_description` text,
  `meta_keywords` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias_UNIQUE` (`alias`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Daten für Tabelle `bitkorn_content_categories`
--

INSERT INTO `bitkorn_content_categories` (`id`, `alias`, `name`, `parent`, `depth`, `route`, `meta_title`, `meta_description`, `meta_keywords`) VALUES
(1, 'ursprung', 'Ursprung', 0, 1, '', NULL, NULL, NULL),
(2, 'blumen', 'meine Blumen', 1, 2, '/blumen', NULL, NULL, NULL),
(3, 'reisen', 'Reisen', 1, 2, '/reisen', NULL, NULL, NULL),
(4, 'rosen', 'meine Rosen', 2, 3, '/blumen/rosen', NULL, NULL, NULL),
(15, 'kleines-lexikon', 'Small Lexikon', 17, 3, '/wiki/kleines-lexikon', NULL, NULL, NULL),
(17, 'wiki', 'mein Wiki', 1, 2, '/wiki', 'bITkorn Wiki', 'das Wiki von bITkorn', 'wiki,wisse,bits,bytes'),
(18, 'gross-lexi', 'großes lexikon', 17, 3, '/wiki/gross-lexi', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bitkorn_content_inline`
--

CREATE TABLE IF NOT EXISTS `bitkorn_content_inline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(45) NOT NULL,
  `inline_content` mediumtext NOT NULL,
  `datetime_creation` datetime NOT NULL,
  `datetime_edited` datetime DEFAULT NULL,
  `comment` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Daten für Tabelle `bitkorn_content_inline`
--

INSERT INTO `bitkorn_content_inline` (`id`, `alias`, `inline_content`, `datetime_creation`, `datetime_edited`, `comment`) VALUES
(1, 'test-zeug', '<p>etwas<span style="font-size:22px"> inline</span> Content</p>', '2013-11-30 20:04:32', '2013-12-30 09:25:42', 'aktuell auf der Startseite 2013-12'),
(2, 'dfasdgfa', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,</p>', '2013-12-01 11:09:27', NULL, NULL),
(3, 'dfasdgfa', '<p>sdfhsdhd ...Onkelz for ever</p>', '2013-12-01 11:11:38', '2013-12-28 10:14:59', 'der onkelz inline'),
(4, 'dfasdgfa', '<p>sdfhsdhd ...&Auml;rzte for ever</p>', '2013-12-01 11:14:28', '2013-12-28 10:12:12', 'das is ja nur testx'),
(5, 'dfgds', '<p>wieder n zeug</p>', '2013-12-01 11:16:26', '2013-12-28 10:08:01', NULL),
(6, 'dfasdgfa', '<p>sdfhsdhdx</p>', '2013-12-01 13:37:23', NULL, NULL),
(7, 'dfasdgfa', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, p<strong>ellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante,</strong> dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,</p>', '2013-12-01 13:54:09', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bitkorn_content_menu`
--

CREATE TABLE IF NOT EXISTS `bitkorn_content_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(45) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `name_display` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Daten für Tabelle `bitkorn_content_menu`
--

INSERT INTO `bitkorn_content_menu` (`id`, `alias`, `name`, `name_display`) VALUES
(1, 'einsmenu', 'first', 'erstes Menu'),
(2, 'zwei-menu', 'second', '2ed menu');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bitkorn_content_menu_entries`
--

CREATE TABLE IF NOT EXISTS `bitkorn_content_menu_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_bitkorn_content_menu_entries_menu_idx` (`menu_id`),
  KEY `fk_bitkorn_content_menu_entries_content_idx` (`content_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Daten für Tabelle `bitkorn_content_menu_entries`
--

INSERT INTO `bitkorn_content_menu_entries` (`id`, `menu_id`, `content_id`) VALUES
(1, 1, 1),
(3, 1, 6),
(12, 2, 1),
(13, 1, 8);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bitkorn_menu`
--

CREATE TABLE IF NOT EXISTS `bitkorn_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) DEFAULT NULL COMMENT 'wie eine Category, wo id & parent gleich ist ein CategoryParent , oft enabled = 0',
  `name` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `route` varchar(45) DEFAULT NULL COMMENT 'ergibt sich auch aus category und alias aus bitkorn_content',
  `cms_category` varchar(45) DEFAULT NULL,
  `cms_alias` varchar(45) DEFAULT NULL,
  `enabled` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Daten für Tabelle `bitkorn_menu`
--

INSERT INTO `bitkorn_menu` (`id`, `parent`, `name`, `title`, `route`, `cms_category`, `cms_alias`, `enabled`) VALUES
(1, 1, 'home', 'Home', '/', NULL, NULL, 0),
(2, 1, 'zum Album', 'mein Album', 'album', NULL, NULL, 1),
(3, 1, 'Formular', 'ein Formular', 'customerform', NULL, NULL, 1),
(4, 1, 'grüßen', 'Gruss Service', 'sayhello', NULL, NULL, 1),
(6, 1, 'cmstest', 'CMS Test', 'bitkorn_cms_content_id', 'aldi', 'Frischkäse', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bitkorn_termine`
--

CREATE TABLE IF NOT EXISTS `bitkorn_termine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `month` int(2) NOT NULL,
  `date` date NOT NULL,
  `von` time NOT NULL,
  `bis` time NOT NULL,
  `preis` decimal(6,2) NOT NULL,
  `reserviert` int(1) NOT NULL DEFAULT '0' COMMENT 'Vorstufe zu NICHT frei',
  `frei` int(1) NOT NULL DEFAULT '1' COMMENT '0 wenn response from payment success',
  `datetime_anfrage` datetime DEFAULT NULL,
  `buchungstext` text,
  `vorname` varchar(45) DEFAULT NULL,
  `nachname` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `telefon` varchar(45) DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `payment_method` varchar(45) DEFAULT NULL,
  `payment_hash_intern` varchar(100) DEFAULT NULL,
  `payment_hash_institut` varchar(60) DEFAULT NULL,
  `paypal_correlation_id` varchar(45) DEFAULT NULL,
  `payed` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=338 ;

--
-- Daten für Tabelle `bitkorn_termine`
--

INSERT INTO `bitkorn_termine` (`id`, `month`, `date`, `von`, `bis`, `preis`, `reserviert`, `frei`, `datetime_anfrage`, `buchungstext`, `vorname`, `nachname`, `email`, `telefon`, `ip`, `payment_method`, `payment_hash_intern`, `payment_hash_institut`, `paypal_correlation_id`, `payed`) VALUES
(11, 0, '2013-10-15', '09:30:00', '09:45:00', 20.00, 1, 0, '2013-10-12 11:24:00', 'bitte frischen Kaffee', 'Klaus', 'Niemeier', 'klaus@web.de', '0231 8769021', '', 'pp', NULL, NULL, NULL, 0),
(13, 0, '2013-10-15', '10:00:00', '10:15:00', 20.00, 1, 0, '2013-08-21 20:10:00', 'keinen Kommentar', 'Wolfram', 'Kammern', 'winni@yahoo.de', '0234 7687654', NULL, 'su', NULL, NULL, NULL, 0),
(76, 0, '2013-10-17', '07:00:00', '07:20:00', 20.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(77, 0, '2013-10-17', '07:20:00', '07:40:00', 20.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(78, 0, '2013-10-17', '07:40:00', '08:00:00', 20.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(83, 0, '2013-10-19', '12:20:00', '12:40:00', 20.00, 1, 0, '2013-10-12 11:24:00', '', 'Peter', 'Wurlizer', 'pwurz@yahoo.de', '0176 2346512', '', 'su', NULL, NULL, NULL, 0),
(113, 0, '2013-10-18', '08:20:00', '08:40:00', 20.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(114, 0, '2013-10-18', '08:40:00', '09:00:00', 20.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(115, 0, '2013-10-18', '09:00:00', '09:20:00', 20.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(116, 0, '2013-10-18', '09:20:00', '09:40:00', 20.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(117, 0, '2013-10-18', '09:40:00', '10:00:00', 20.00, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(118, 0, '2013-10-20', '09:00:00', '09:20:00', 20.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(119, 0, '2013-10-20', '09:20:00', '09:40:00', 20.00, 0, 1, '2013-10-31 13:47:45', NULL, 'Torstenx', 'Brieskorn', 'mail@t-brieskorn.de', '2345345634', '127.0.0.1', 'pp', '68dd1d4a9c8271ee955e61da635c4305', 'EC-66V95910R3188515C', NULL, 0),
(120, 0, '2013-10-20', '09:40:00', '10:00:00', 20.00, 1, 0, '2013-10-24 15:12:52', NULL, 'Torsten', 'Brieskorn', 'mail@t-brieskorn.de', '2345345634', '127.0.0.1', 'pp', '8b1c6227317555566295a294bae1e17f', '', NULL, 0),
(124, 0, '2013-10-19', '13:00:00', '13:20:00', 20.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(125, 0, '2013-10-19', '13:20:00', '13:40:00', 20.00, 1, 0, '2013-10-24 12:41:54', NULL, 'Torsten', 'Brieskorn', 'mail@t-brieskorn.de', '2345345634', '127.0.0.1', 'su', '$2y$14$LZ.kMKed0yqhGSOZ5RFp5O41vd88eaKuuFobQOD0AqUt9hNKcozHq', '', NULL, 0),
(126, 0, '2013-10-19', '13:40:00', '14:00:00', 20.00, 0, 1, '2013-10-24 14:50:24', NULL, 'Torsten', 'Brieskorn', 'mail@t-brieskorn.de', '2345345634', '127.0.0.1', 'pp', '2175f33b9d4ef91348afeb6a7544d1fc', NULL, NULL, 0),
(127, 0, '2013-11-20', '11:00:00', '11:20:00', 20.00, 1, 0, '2013-11-12 15:45:45', NULL, 'Torsten', 'Brieskorn', 'mail@t-brieskorn.de', '2345345634', '127.0.0.1', 'su', '937f443b90796874024bcf6ac5ff6ffa', '', NULL, 1),
(128, 0, '2013-11-20', '11:20:00', '11:40:00', 20.00, 1, 0, '2013-11-23 10:40:21', NULL, 'Torsten', 'Brieskorn', 'mail@t-brieskorn.de', '2345345634', '127.0.0.1', 'pp', '72de8f09c418e86f534f67e416ce8ea8', 'EC-7S125214RV4756925', NULL, 0),
(129, 0, '2013-11-20', '11:40:00', '12:00:00', 20.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(130, 0, '2013-11-20', '12:00:00', '12:20:00', 20.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(131, 0, '2013-11-20', '12:20:00', '12:40:00', 20.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(132, 0, '2013-11-20', '12:40:00', '13:00:00', 20.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(313, 0, '2013-12-11', '09:00:00', '09:15:00', 11.20, 1, 0, '2013-12-10 14:11:53', NULL, 'Torsten', 'Brieskorn', 'mail@t-brieskorn.de', '2345345634', '127.0.0.1', 'pp', 'ad62b23037af0d84ca6c0494fbf6161c', 'EC-1YA59319K0380273X', NULL, 0),
(314, 0, '2013-12-11', '09:15:00', '09:30:00', 11.20, 1, 0, '2013-12-10 14:18:07', NULL, 'Gustav', 'Brieskorn', 'mail@t-brieskorn.de', '2345345634', '127.0.0.1', 'pp', 'f0a84813a9330eac49752583c3573e21', 'EC-9MP81527CY571313X', NULL, 0),
(315, 0, '2013-12-11', '09:30:00', '09:45:00', 11.20, 0, 1, '2014-01-09 08:33:57', NULL, 'Torsten', 'Brieskorn', 'mail@t-brieskorn.de', '2345345634', '127.0.0.1', 'su', 'e9be361bb3ee7063d4f10d707a538801', NULL, NULL, 0),
(316, 0, '2013-12-11', '09:45:00', '10:00:00', 11.20, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(317, 0, '2013-12-11', '10:00:00', '10:15:00', 11.20, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(318, 0, '2013-12-11', '10:15:00', '10:30:00', 11.20, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(319, 0, '2013-12-11', '10:30:00', '10:45:00', 11.20, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(320, 0, '2013-12-11', '10:45:00', '11:00:00', 11.20, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(321, 0, '2013-12-13', '09:00:00', '09:30:00', 35.30, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(322, 0, '2013-12-13', '09:30:00', '10:00:00', 35.30, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(323, 0, '2014-07-11', '09:00:00', '09:20:00', 22.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(324, 0, '2014-07-11', '09:20:00', '09:40:00', 22.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(325, 0, '2014-07-11', '09:40:00', '10:00:00', 22.00, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(326, 0, '2014-07-11', '10:00:00', '10:20:00', 22.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(327, 0, '2014-07-11', '10:20:00', '10:40:00', 22.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(328, 0, '2014-07-11', '10:40:00', '11:00:00', 22.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(329, 0, '2014-07-11', '11:00:00', '11:20:00', 22.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(330, 0, '2014-07-11', '11:20:00', '11:40:00', 22.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(331, 0, '2014-07-11', '11:40:00', '12:00:00', 22.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(332, 0, '2014-07-11', '12:00:00', '12:20:00', 22.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(333, 0, '2014-07-11', '12:20:00', '12:40:00', 22.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(334, 0, '2014-07-11', '12:40:00', '13:00:00', 22.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(335, 0, '2014-07-11', '13:00:00', '13:20:00', 22.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(336, 0, '2014-07-11', '13:20:00', '13:40:00', 22.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(337, 0, '2014-07-11', '13:40:00', '14:00:00', 22.00, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bitkorn_user`
--

CREATE TABLE IF NOT EXISTS `bitkorn_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `passwd` varchar(100) DEFAULT NULL COMMENT 'Apache sha1',
  `role` varchar(45) DEFAULT 'user',
  `groups` tinytext COMMENT 'bitkorn_user_groups IDs CSV',
  `datetime_register` datetime DEFAULT NULL,
  `register_hash` varchar(100) DEFAULT NULL,
  `confirmed` int(1) NOT NULL DEFAULT '0',
  `datetime_passwd` varchar(45) DEFAULT NULL COMMENT 'Zeit der passwd_hash Generierung',
  `passwd_hash` varchar(45) DEFAULT NULL,
  `name_first` varchar(45) DEFAULT NULL,
  `name_last` varchar(45) DEFAULT NULL,
  `str` varchar(100) DEFAULT NULL,
  `str_nr` varchar(10) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `city` varchar(80) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `tel` varchar(80) DEFAULT NULL,
  `tel_mobile` varchar(80) DEFAULT NULL,
  `tel_fax` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `passwd_hash_UNIQUE` (`passwd_hash`),
  UNIQUE KEY `register_hash_UNIQUE` (`register_hash`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Daten für Tabelle `bitkorn_user`
--

INSERT INTO `bitkorn_user` (`id`, `username`, `email`, `passwd`, `role`, `groups`, `datetime_register`, `register_hash`, `confirmed`, `datetime_passwd`, `passwd_hash`, `name_first`, `name_last`, `str`, `str_nr`, `zip`, `city`, `country`, `tel`, `tel_mobile`, `tel_fax`) VALUES
(1, 'bitkorn', 'mail@bitkorn.de', '{SHA}pOsEho5rVp+xa4nOub+fMNp1GqE=', 'admin', '1,2,3,4,5,6,7', '2013-10-31 11:45:17', '$2y$14$VXsRE.W6pwQvNs2l0DKKCuE0HXmR.MntlIN9eKrAGiCW/MmBAjFVG', 1, '2013-12-17 17:23:38', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'allapow', 'allapow.okey@yahoo.de', '{SHA}pOsEho5rVp+xa4nOub+fMNp1GqE=', 'admin', '1,2,3,4,5,6,7', '2013-11-01 11:45:17', NULL, 1, NULL, NULL, 'Allapow', 'Okey', '', '', '', '', 47, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bitkorn_user_group`
--

CREATE TABLE IF NOT EXISTS `bitkorn_user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(45) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Daten für Tabelle `bitkorn_user_group`
--

INSERT INTO `bitkorn_user_group` (`id`, `alias`, `name`, `description`) VALUES
(1, 'boss', 'Chef', 'darf alles'),
(2, 'manager', 'Manager', 'darf fast alles'),
(3, 'admin', 'Administrator', ''),
(4, 'purchase', 'Einkauf', NULL),
(5, 'order', 'Aufträge', NULL),
(6, 'office', 'Sekretariat', NULL),
(7, 'employee', 'Angestellter', '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bitkorn_user_relations`
--

CREATE TABLE IF NOT EXISTS `bitkorn_user_relations` (
  `id_user_origin` int(11) NOT NULL COMMENT 'der der als Freund kennzeichnete',
  `id_user_friend` int(11) DEFAULT NULL COMMENT 'der der als Freund gekennzeichnet wurde',
  `merged_user_ids` varchar(45) DEFAULT NULL COMMENT 'Alternative zu id_user_origin & id_user_friend: kleinere User-ID steht am Anfang; Freunde wenn ',
  PRIMARY KEY (`id_user_origin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bitkorn_user_social`
--

CREATE TABLE IF NOT EXISTS `bitkorn_user_social` (
  `id_user` int(11) NOT NULL,
  `data_privacy` int(1) DEFAULT '0' COMMENT '0=private,1=friendly,2=public',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `brd_lander`
--

CREATE TABLE IF NOT EXISTS `brd_lander` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(2) NOT NULL,
  `name` varchar(45) NOT NULL,
  `hauptstadt` varchar(45) DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Daten für Tabelle `brd_lander`
--

INSERT INTO `brd_lander` (`id`, `alias`, `name`, `hauptstadt`, `active`) VALUES
(1, 'BW', 'Baden-Württemberg', 'Stuttgart', 1),
(2, 'BY', 'Bayern', 'München', 1),
(3, 'BE', 'Berlin', '', 1),
(4, 'BB', 'Brandenburg', 'Potsdam', 1),
(5, 'HB', 'Bremen', '', 1),
(6, 'HH', 'Hamburg', '', 1),
(7, 'HE', 'Hessen', 'Wiesbaden', 1),
(8, 'MV', 'Mecklenburg-Vorpommern', 'Schwerin', 1),
(9, 'NI', 'Niedersachsen', 'Hannover', 1),
(10, 'NW', 'Nordrhein-Westfalen', 'Düsseldorf', 1),
(11, 'RP', 'Rheinland-Pfalz', 'Mainz', 1),
(12, 'SL', 'Saarland', 'Saarbrücken', 1),
(13, 'SN', 'Sachsen', 'Dresden', 1),
(14, 'ST', 'Sachsen-Anhalt', 'Magdeburg', 1),
(15, 'SH', 'Schleswig-Holstein', 'Kiel', 1),
(16, 'TH', 'Thüringen', 'Erfurt', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `egvp_anfrage`
--

CREATE TABLE IF NOT EXISTS `egvp_anfrage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anfrage_art` int(11) DEFAULT NULL COMMENT 'Inkasso oder Zwangs',
  `glaub_art` varchar(10) DEFAULT NULL COMMENT 'privat oder gewerbe',
  `glaub_person_count` int(2) DEFAULT NULL,
  `glaub_person_vertreter_count` int(2) DEFAULT NULL,
  `glaub_person_id` int(11) DEFAULT NULL,
  `glaub_gewerbe_art` int(2) DEFAULT NULL,
  `glaub_gewerbe_id` int(11) DEFAULT NULL,
  `schuld_art` varchar(10) DEFAULT NULL,
  `schuld_person_count` int(2) DEFAULT NULL,
  `schuld_person_vertreter_count` int(2) DEFAULT NULL,
  `schuld_person_id` int(11) DEFAULT NULL,
  `schuld_gewerbe_art` int(2) DEFAULT NULL,
  `schuld_gewerbe_id` int(11) DEFAULT NULL,
  `forderung_art` int(11) DEFAULT NULL,
  `forderung_grundlage` varchar(45) DEFAULT NULL,
  `forderung_grundlage_datum` date DEFAULT NULL,
  `forderung_betrag` decimal(9,2) DEFAULT NULL,
  `forderung_rechnung_datum` date DEFAULT NULL,
  `forderung_rechnung_nr` varchar(45) DEFAULT NULL,
  `forderung_sonstiges` text,
  `nebenforderung_zinsen` decimal(9,2) DEFAULT NULL,
  `nebenforderung_zinsen_ab_datum` date DEFAULT NULL,
  `nebenforderung_auslagen` decimal(9,2) DEFAULT NULL,
  `nebenforderung_sonstiges` text,
  `mahnung1_datum` date DEFAULT NULL,
  `mahnung2_datum` date DEFAULT NULL,
  `mahnung3_datum` date DEFAULT NULL,
  `mahnung4_datum` date DEFAULT NULL,
  `session_id` varchar(45) DEFAULT NULL,
  `attachment` int(11) DEFAULT NULL COMMENT 'Anzahl der Attachments',
  `ip` varchar(45) DEFAULT NULL,
  `startzeit` datetime DEFAULT NULL,
  `endzeit` datetime DEFAULT NULL,
  `abgerufen` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_egvp_anfrage_gewerbeArt_idx` (`glaub_gewerbe_art`),
  KEY `fk_egvp_anfrage_schuldGewerbeArt_idx` (`schuld_gewerbe_art`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=120 ;

--
-- Daten für Tabelle `egvp_anfrage`
--

INSERT INTO `egvp_anfrage` (`id`, `anfrage_art`, `glaub_art`, `glaub_person_count`, `glaub_person_vertreter_count`, `glaub_person_id`, `glaub_gewerbe_art`, `glaub_gewerbe_id`, `schuld_art`, `schuld_person_count`, `schuld_person_vertreter_count`, `schuld_person_id`, `schuld_gewerbe_art`, `schuld_gewerbe_id`, `forderung_art`, `forderung_grundlage`, `forderung_grundlage_datum`, `forderung_betrag`, `forderung_rechnung_datum`, `forderung_rechnung_nr`, `forderung_sonstiges`, `nebenforderung_zinsen`, `nebenforderung_zinsen_ab_datum`, `nebenforderung_auslagen`, `nebenforderung_sonstiges`, `mahnung1_datum`, `mahnung2_datum`, `mahnung3_datum`, `mahnung4_datum`, `session_id`, `attachment`, `ip`, `startzeit`, `endzeit`, `abgerufen`) VALUES
(78, 1, 'gg', NULL, NULL, NULL, 2, 3, 'sp', 1, 3, 11, NULL, NULL, 1, 'Handschlag', '2013-10-08', 2341.00, '2013-10-16', '8237.34.gf', '', 23.00, '2013-10-15', 0.00, '', '2013-10-28', '2013-10-28', '2013-10-28', '2013-10-28', 'cda7g6fel5cmmc6s2km1fcuq91', NULL, '127.0.0.1', '2013-10-29 12:54:55', '2013-10-29 13:07:02', 0),
(79, 2, 'gp', 1, 3, 12, 2, 5, 'sp', 1, 3, 13, 1, 4, 1, 'Handschlag', '2013-08-23', 3445.00, '2013-08-23', '12fg43', '', 23.00, '2013-10-21', 0.00, '', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '89mha0gr3k6f68ncne5an9cod7', NULL, '127.0.0.1', '2013-10-29 22:14:43', '2013-10-30 10:26:32', 0),
(80, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '89mha0gr3k6f68ncne5an9cod7', NULL, '127.0.0.1', '2013-10-30 10:13:34', NULL, 0),
(81, 1, 'gg', NULL, NULL, NULL, 2, 6, 'sp', 1, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'g2mv04tknqf2cv2mp1apt39896', NULL, '127.0.0.1', '2013-10-30 13:27:27', NULL, 0),
(82, 2, 'gg', NULL, NULL, NULL, 2, 7, 'sp', 1, 3, 15, NULL, NULL, 1, 'Handschlag', '2013-10-09', 3445.00, '2013-11-13', '345132', '', 23.00, '2013-11-14', 4.00, '', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0s4duicjt88ar8a507gvdsvr64', 1, '127.0.0.1', '2013-11-05 17:23:03', '2013-11-05 19:30:26', 0),
(83, 1, 'gp', 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2sja38lpqa7uh1d3c1v522lev7', NULL, '127.0.0.1', '2013-12-13 14:20:09', NULL, 0),
(84, 1, 'gp', 1, 3, 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jt70f0k7e3fa7flstokh9k5gv6', NULL, '127.0.0.1', '2013-12-15 09:29:13', NULL, 0),
(85, 2, 'gp', 1, 3, 17, NULL, NULL, 'sp', 1, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'kmu95v0nbvrrvblv39mgkqr7a4', NULL, '127.0.0.1', '2013-12-20 16:01:40', NULL, 0),
(86, 1, 'gp', 1, 3, 18, NULL, NULL, 'sp', 1, 3, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9mr4pljmgqt3mhe49ksq3p2vl0', NULL, '127.0.0.1', '2013-12-28 08:50:30', NULL, 0),
(87, 1, 'gp', 1, 3, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'k0fa51n9s1n5lkenfrrn5v5k83', NULL, '127.0.0.1', '2014-01-07 20:53:45', NULL, 0),
(88, 1, 'gp', 1, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'cq55cj54jdgpiq684rklj2o8g4', NULL, '127.0.0.1', '2014-01-11 07:48:27', NULL, 0),
(89, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'cq55cj54jdgpiq684rklj2o8g4', NULL, '127.0.0.1', '2014-01-11 08:38:01', NULL, 0),
(90, 1, 'gg', 1, 3, 21, 1, 12, 'sg', 1, 3, 22, 1, 8, 4, 'Handschlag', '2014-01-08', 2341.00, '2014-01-08', 'wf4fd443', '', 5.00, '2014-01-22', 0.00, '', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '8c1t793olbcfljq5ufuhvgbod2', 1, '127.0.0.1', '2014-01-11 08:43:22', '2014-01-11 12:26:38', 0),
(91, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8c1t793olbcfljq5ufuhvgbod2', NULL, '127.0.0.1', '2014-01-11 08:59:17', NULL, 0),
(92, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8c1t793olbcfljq5ufuhvgbod2', NULL, '127.0.0.1', '2014-01-11 09:53:38', NULL, 0),
(93, 1, 'gg', NULL, NULL, NULL, 3, 19, 'sp', 1, 3, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5ub10vk1ae7dt2o5t9papf7nr5', NULL, '127.0.0.1', '2014-01-11 12:53:22', NULL, 0),
(94, 1, 'gp', 1, 3, 24, NULL, NULL, 'sg', NULL, NULL, NULL, 5, 23, 24, 'Handschlag', '2014-01-08', 12323.00, '2013-10-09', 'wf4fd443', '', 3.00, '2014-01-22', 0.00, '', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '7j651o6aqe51ppp9167p4eoth3', NULL, '127.0.0.1', '2014-01-11 13:05:51', '2014-01-11 15:04:41', 0),
(95, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7j651o6aqe51ppp9167p4eoth3', NULL, '127.0.0.1', '2014-01-11 14:59:12', NULL, 0),
(96, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '60pk4j6g99eu2197veo7n1m6q4', NULL, '127.0.0.1', '2014-01-11 19:33:23', NULL, 0),
(97, 1, 'gp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2u894244bssje09as24uqugip3', NULL, '127.0.0.1', '2014-01-12 07:45:10', NULL, 0),
(98, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7pr2elp0600e1konghdmqjrsv3', NULL, '127.0.0.1', '2014-01-12 08:23:16', NULL, 0),
(99, 1, 'gp', 1, 3, 25, NULL, NULL, 'sp', 1, 3, 26, NULL, NULL, 22, 'Handschlag', '2014-01-15', 3445.00, '2012-01-11', '345132', '', 5.00, '2014-01-08', 0.00, '', NULL, NULL, NULL, NULL, 'hhdsq9var2d748qp3i20qcnpq7', NULL, '127.0.0.1', '2014-01-12 08:23:58', '2014-01-12 08:40:37', 0),
(100, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8j8knn8n1m3k9ilihujhlqej42', NULL, '127.0.0.1', '2014-01-17 10:57:54', NULL, 0),
(101, 1, 'gp', 1, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ectej2j03hvr8l8anljmnr7e17', NULL, '127.0.0.1', '2014-01-17 15:15:33', NULL, 0),
(102, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jn6bs00jm1rjftumq1bvuvmh96', NULL, '127.0.0.1', '2014-01-22 08:35:59', NULL, 0),
(103, 1, 'gg', NULL, NULL, NULL, 5, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'acqltp6h67e8btp4hvke3v1vu1', NULL, '127.0.0.1', '2014-01-24 17:15:39', NULL, 0),
(104, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '69ng9p6j4d5lcf3sft85581a40', NULL, '127.0.0.1', '2014-01-26 09:26:06', NULL, 0),
(105, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2h4tc4cp83jsob87nti0u3sla2', NULL, '127.0.0.1', '2014-06-21 19:38:28', NULL, 0),
(106, 1, 'gp', 2, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3d0hbsjavbgshrfil2ceu150m7', NULL, '127.0.0.1', '2014-08-05 22:05:24', NULL, 0),
(107, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0vdbgd7gmfv2f4qnvh1bfi0g52', NULL, '127.0.0.1', '2014-09-12 08:12:10', NULL, 0),
(108, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'd0acir6tm0351095pc1ojd2b66', NULL, '127.0.0.1', '2014-09-14 09:09:08', NULL, 0),
(109, 1, 'gp', 1, 3, 27, NULL, NULL, 'sp', 1, 1, 28, 2, 26, 7, 'kgh', '2014-09-09', 2.00, '2014-09-10', 'jvgh565', 'cgh', 1.00, '2014-09-25', 2.00, 'iufgvg', NULL, NULL, NULL, NULL, '93eugtkacqklgj3id50042aob7', NULL, '127.0.0.1', '2014-09-24 18:40:10', '2014-09-24 18:51:39', 0),
(110, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3l3i4i1hfktb06gg86e3f5p0r1', NULL, '127.0.0.1', '2014-09-24 19:03:40', NULL, 0),
(111, 1, 'gg', NULL, NULL, NULL, 2, 27, 'sp', 1, 3, 29, NULL, NULL, 4, 'gkhh', '2014-11-06', 300.00, '2014-11-04', 'jvgh565', '', 3.00, '2014-11-03', 0.00, '', NULL, NULL, NULL, NULL, 'ar3r3cl8sceeremstimj64n3m3', 1, '127.0.0.1', '2014-11-26 13:42:23', '2014-11-26 14:34:31', 0),
(112, 1, 'gp', 2, 1, 30, NULL, NULL, 'sg', NULL, NULL, NULL, 2, 28, 4, 'rrgr', '2014-11-13', 234.00, '2014-11-06', 'fg345tr', '', 34.00, '2014-11-13', 0.00, '', NULL, NULL, NULL, NULL, 't3qeaie386mklo1vs9dboghou1', NULL, '127.0.0.1', '2014-11-26 16:43:15', '2014-11-26 16:53:06', 0),
(113, 1, 'gg', 1, 3, 31, 3, 30, 'sp', 1, 3, 32, 2, 29, 1, 'gkhh', '2014-12-16', 4000.00, '2014-12-09', 'jvgh565', '', 2.00, '2014-12-10', 2.00, '', NULL, NULL, NULL, NULL, 'f61c1o3net1fasedc60ka0u0s2', NULL, '127.0.0.1', '2014-12-05 08:36:42', '2014-12-05 08:57:11', 0),
(114, 1, 'gp', 2, 1, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'd2dk8dbcgvclgerte4grt7au90', NULL, '127.0.0.1', '2015-02-03 20:53:13', NULL, 0),
(115, 1, 'gg', NULL, NULL, NULL, 4, 31, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'so6917ml7s1ecbgmpj0j3qibt5', NULL, '127.0.0.1', '2015-02-03 20:54:16', NULL, 0),
(116, 1, 'gg', NULL, NULL, NULL, 3, 34, 'sp', 1, 3, 34, NULL, NULL, 4, 'sdfg', '2015-03-10', 400.00, '2015-03-03', '345frt', '', 2.00, '2015-03-04', 0.00, '', NULL, NULL, NULL, NULL, '24jrjud2t9330j6u2mssvls2h7', NULL, '127.0.0.1', '2015-03-16 10:03:36', '2015-03-16 12:36:29', 0),
(117, 1, 'gg', NULL, NULL, NULL, 5, 36, 'sg', NULL, NULL, NULL, 4, 37, 4, '332de', '2015-03-11', 200.00, '2015-03-03', '4ffer54', 'Lorem ipsum dolor sit amet, consetet', 2.00, '2015-03-19', 41.00, 'psum dolor sit am', NULL, NULL, NULL, NULL, '66d5b765tuhgm8eornknkph4g4', 5, '127.0.0.1', '2015-03-16 12:42:27', '2015-03-16 14:54:54', 0),
(118, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'giuca9avne2n80f49s85ts2fh1', NULL, '127.0.0.1', '2015-03-16 15:26:10', NULL, 0),
(119, 1, 'gg', NULL, NULL, NULL, 1, 39, 'sg', NULL, NULL, NULL, 1, 40, 3, '332de', '2015-03-10', 70.00, '2015-03-04', '4ffer54', '', 1.00, '2015-03-25', 0.00, '', NULL, NULL, NULL, NULL, '8ule7klvr03it38bo8425j1hq7', NULL, '127.0.0.1', '2015-03-16 20:05:39', '2015-03-16 20:13:56', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `egvp_anfrage_arten`
--

CREATE TABLE IF NOT EXISTS `egvp_anfrage_arten` (
  `id` int(11) NOT NULL,
  `alias` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `egvp_anfrage_arten`
--

INSERT INTO `egvp_anfrage_arten` (`id`, `alias`, `name`, `description`) VALUES
(1, 'inkasso', 'Inkassoverfahren', NULL),
(2, 'zwangsv', 'Zwangsvollstreckung', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `egvp_forderung_arten`
--

CREATE TABLE IF NOT EXISTS `egvp_forderung_arten` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Daten für Tabelle `egvp_forderung_arten`
--

INSERT INTO `egvp_forderung_arten` (`id`, `name`, `description`) VALUES
(1, 'Werkvertrag', NULL),
(2, 'Kauf', NULL),
(3, 'Miete', NULL),
(4, 'Schadenersatz', NULL),
(5, 'Darlehen', NULL),
(6, 'Miete', NULL),
(7, 'Pacht', NULL),
(8, 'Leasing', NULL),
(9, 'Leihe', NULL),
(10, 'Dienstleistung', NULL),
(11, 'Dienstleistungen höherer Art', NULL),
(12, 'Geschäftsführung ohne Auftrag', NULL),
(13, 'Handwerkerleistung', NULL),
(14, 'sonstiger Werkvertrag', NULL),
(15, 'Kreditvertrag', NULL),
(16, 'Bürgschaft', NULL),
(17, 'Arztvertrag', NULL),
(18, 'Bewirtungsvertrag', NULL),
(19, 'online-Kaufvertrag', NULL),
(20, 'online-Dienstleistungsvertrag', NULL),
(21, 'Arbeitsvertrag', NULL),
(22, 'Freie Mitarbeit', NULL),
(23, 'Praktikumsvertrag', NULL),
(24, 'geringfügige Beschäftigung', NULL),
(25, 'Versicherungsvertrag', NULL),
(26, 'Schenkung', NULL),
(27, 'Gelddarlehen', NULL),
(28, 'Sachdarlehen', NULL),
(29, 'Pachtvertrag', NULL),
(30, 'Auftrag', NULL),
(31, 'Verwahrungsvertrag', NULL),
(32, 'Factoring', NULL),
(33, 'Franchising', NULL),
(34, 'Rechtsgeschäftliche und rechtsgeschäftsähnlic', NULL),
(35, 'Schadenersatz wegen Pflichtverletzung = ehema', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `egvp_gewerbe`
--

CREATE TABLE IF NOT EXISTS `egvp_gewerbe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `art` int(11) DEFAULT NULL COMMENT '1 - 5 aus egvp_gewerbe_arten',
  `freelance_beruf` varchar(45) DEFAULT NULL,
  `gewerbe_name` varchar(45) DEFAULT NULL,
  `gewerbe_strasse` varchar(45) DEFAULT NULL,
  `gewerbe_str_nr` varchar(45) DEFAULT NULL,
  `gewerbe_plz` varchar(45) DEFAULT NULL,
  `gewerbe_ort` varchar(45) DEFAULT NULL,
  `gewerbe_b_land` varchar(45) DEFAULT NULL,
  `person_titel` varchar(10) DEFAULT NULL,
  `person_vorname` varchar(45) DEFAULT NULL,
  `person_nachname` varchar(45) DEFAULT NULL,
  `person_strasse` varchar(45) DEFAULT NULL,
  `person_str_nr` varchar(45) DEFAULT NULL,
  `person_plz` varchar(45) DEFAULT NULL,
  `person_ort` varchar(45) DEFAULT NULL,
  `person_b_land` varchar(45) DEFAULT NULL,
  `person2_titel` varchar(10) DEFAULT NULL,
  `person2_vorname` varchar(45) DEFAULT NULL,
  `person2_nachname` varchar(45) DEFAULT NULL,
  `person2_strasse` varchar(45) DEFAULT NULL,
  `person2_str_nr` varchar(45) DEFAULT NULL,
  `person2_plz` varchar(45) DEFAULT NULL,
  `person2_ort` varchar(45) DEFAULT NULL,
  `person2_b_land` varchar(45) DEFAULT NULL,
  `kg_name` varchar(45) DEFAULT NULL,
  `kg_strasse` varchar(45) DEFAULT NULL,
  `kg_str_nr` varchar(45) DEFAULT NULL,
  `kg_plz` varchar(45) DEFAULT NULL,
  `kg_ort` varchar(45) DEFAULT NULL,
  `kg_b_land` varchar(45) DEFAULT NULL,
  `session_id` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL COMMENT 'g (glaubiger) | s (schuldner)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

--
-- Daten für Tabelle `egvp_gewerbe`
--

INSERT INTO `egvp_gewerbe` (`id`, `art`, `freelance_beruf`, `gewerbe_name`, `gewerbe_strasse`, `gewerbe_str_nr`, `gewerbe_plz`, `gewerbe_ort`, `gewerbe_b_land`, `person_titel`, `person_vorname`, `person_nachname`, `person_strasse`, `person_str_nr`, `person_plz`, `person_ort`, `person_b_land`, `person2_titel`, `person2_vorname`, `person2_nachname`, `person2_strasse`, `person2_str_nr`, `person2_plz`, `person2_ort`, `person2_b_land`, `kg_name`, `kg_strasse`, `kg_str_nr`, `kg_plz`, `kg_ort`, `kg_b_land`, `session_id`, `type`) VALUES
(3, 2, NULL, 'TestGbR', 'Bringerstreet', '3443', '45678', 'Hamburg', 'SL', NULL, 'Gertrud', 'Weidner', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'cda7g6fel5cmmc6s2km1fcuq91', 'g'),
(4, 1, 'Softwareentwickler', 'Testcompany', 'Hammacherstr', '6', '12345', 'Frankfurt', 'SN', NULL, 'Stefanx', 'Bitkorn', 'Hammacherstr', '4', '12345', 'Frankfurt', 'SH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '89mha0gr3k6f68ncne5an9cod7', 's'),
(5, 2, NULL, 'TestGbR', 'Bringerstreet', '4', '12345', 'Frankfurt', 'SH', NULL, 'Stefanx', 'Weidner', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '89mha0gr3k6f68ncne5an9cod7', 'g'),
(6, 2, NULL, 'TestGbR', 'Hammacherstr', '6', '45678', 'Hamburg', 'RP', NULL, 'Gertrud', 'Weidner', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'g2mv04tknqf2cv2mp1apt39896', 'g'),
(7, 2, 'sgasg', 'TestGbR', 'Hammacherstr', '4', '12345', 'Frankfurt', 'SH', NULL, 'Gertrud', 'Weidner', 'Bringerstreet', '4', '12345', 'Frankfurt', 'ST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0s4duicjt88ar8a507gvdsvr64', 'g'),
(8, 1, 'Softwareentwickler', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Gertrud', 'Weidner', 'Hammacherstr', '4', '12345', 'Frankfurt', 'SL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8c1t793olbcfljq5ufuhvgbod2', 's'),
(9, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8c1t793olbcfljq5ufuhvgbod2', 's'),
(10, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8c1t793olbcfljq5ufuhvgbod2', 's'),
(11, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8c1t793olbcfljq5ufuhvgbod2', 's'),
(12, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8c1t793olbcfljq5ufuhvgbod2', 'g'),
(13, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8c1t793olbcfljq5ufuhvgbod2', 'g'),
(14, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8c1t793olbcfljq5ufuhvgbod2', 'g'),
(15, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8c1t793olbcfljq5ufuhvgbod2', 'g'),
(16, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8c1t793olbcfljq5ufuhvgbod2', 's'),
(17, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8c1t793olbcfljq5ufuhvgbod2', 's'),
(18, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8c1t793olbcfljq5ufuhvgbod2', 'g'),
(19, 3, NULL, 'Testcompany', NULL, NULL, NULL, NULL, NULL, NULL, 'Stefanx', 'Bitkorn', 'Hammacherstr', '4', '12345', 'Frankfurt', 'SN', NULL, 'Winfried', 'Meier', 'Wurlizerstrasse', '2', '43567', 'Dortmund', 'ST', NULL, NULL, NULL, NULL, NULL, NULL, '5ub10vk1ae7dt2o5t9papf7nr5', 'g'),
(20, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5ub10vk1ae7dt2o5t9papf7nr5', 'g'),
(21, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5ub10vk1ae7dt2o5t9papf7nr5', 'g'),
(22, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5ub10vk1ae7dt2o5t9papf7nr5', 'g'),
(23, 5, NULL, 'Die Firma GmbH', 'Fluntingstreet', '22', '48589', 'Würzburg', 'RP', NULL, 'Gertrud', 'Bitkorn', 'Hammacherstr', '4', '12345', 'Hamburg', 'ST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'KG Quax', 'Ameisentreet', '2', '32345', 'Olpe', 'HH', '7j651o6aqe51ppp9167p4eoth3', 's'),
(24, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7j651o6aqe51ppp9167p4eoth3', 's'),
(25, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'acqltp6h67e8btp4hvke3v1vu1', 'g'),
(26, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '93eugtkacqklgj3id50042aob7', 's'),
(27, 2, NULL, 'Angst und Bange', 'Feilchenweg', '6', '44200', 'Bochum', 'NW', NULL, 'Peter', 'Lustig', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ar3r3cl8sceeremstimj64n3m3', 'g'),
(28, 2, NULL, 'Angst und Bange', 'Mauserstraße', '5', '44200', 'Suchdorf', 'ST', NULL, 'Gabriel', 'Gaukelei', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 't3qeaie386mklo1vs9dboghou1', 's'),
(29, 2, NULL, 'Angst und Bange', 'Mauserstraße', '66', '44200', 'Bochum', 'SH', NULL, 'Peter', 'Gaukelei', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'f61c1o3net1fasedc60ka0u0s2', 's'),
(30, 3, NULL, 'Angst und Bange', NULL, NULL, NULL, NULL, NULL, NULL, 'Peter', 'Dussel', 'Feilchenweg', '5', '12345', 'Hagen', 'HH', NULL, 'Donald', 'Duck', 'Wieselstraße', '8', '33224', 'Entenhausen', 'HH', NULL, NULL, NULL, NULL, NULL, NULL, 'f61c1o3net1fasedc60ka0u0s2', 'g'),
(31, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'so6917ml7s1ecbgmpj0j3qibt5', 'g'),
(32, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'so6917ml7s1ecbgmpj0j3qibt5', 'g'),
(33, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'so6917ml7s1ecbgmpj0j3qibt5', 'g'),
(34, 3, NULL, 'Angst und Bange', NULL, NULL, NULL, NULL, NULL, 'Dr', 'Herbert', 'Gauser', 'Wuldfrettstrasse', '55', '23876', 'Düsseldorf', 'HB', 'Dr.in', 'Gundula', 'Gause', 'Wulfrestarsse', '22', '23456', 'Düsseldorf', 'BB', NULL, NULL, NULL, NULL, NULL, NULL, '24jrjud2t9330j6u2mssvls2h7', 'g'),
(35, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '24jrjud2t9330j6u2mssvls2h7', 'g'),
(36, 5, NULL, 'Angst und Bange', 'Wulfweg', '66', '12345', 'Hagen', 'BY', 'Notitel', 'Gustel', 'Meiser', 'Feldweg', '77', '87654', 'Münster', 'BB', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'KG Bange', 'Horstweg', '7', '76543', 'Dortmund', 'BE', '66d5b765tuhgm8eornknkph4g4', 'g'),
(37, 4, NULL, 'Kitkatmixer', 'Schokoweg', '77', '65432', 'Bochum', 'BY', 'DrDr', 'Peter', 'Schmidt', 'Wolfweg', '7', '87654', 'Köln', 'BB', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '66d5b765tuhgm8eornknkph4g4', 's'),
(38, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '66d5b765tuhgm8eornknkph4g4', 'g'),
(39, 1, 'dasfgdg', NULL, NULL, NULL, NULL, NULL, NULL, '', 'sdfgsd', 'dfgdsfg', 'arfwerf', '234', '33344', 'rasgasd', 'SN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8ule7klvr03it38bo8425j1hq7', 'g'),
(40, 1, 'dstgewgt', NULL, NULL, NULL, NULL, NULL, NULL, '', 'Peter', 'Gauser', 'Steinstraße 18', '7', '32345', 'Düsseldorf', 'SL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8ule7klvr03it38bo8425j1hq7', 's');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `egvp_gewerbe_arten`
--

CREATE TABLE IF NOT EXISTS `egvp_gewerbe_arten` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Daten für Tabelle `egvp_gewerbe_arten`
--

INSERT INTO `egvp_gewerbe_arten` (`id`, `alias`, `name`, `description`) VALUES
(1, 'freelance', 'Freiberufler', 'Freiberufler'),
(2, 'single', 'Einzelunternehmer', 'Einzelunternehmer'),
(3, 'gbr', 'GbR', 'GbR'),
(4, 'gmbh', 'GmbH', 'GmbH, AG, KG, Verein'),
(5, 'gmbhcokg', 'GmbH + CoKg', 'GmbH + CoKg, AG + CoKg, OHG + CoKg');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `egvp_person`
--

CREATE TABLE IF NOT EXISTS `egvp_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p1_titel` varchar(10) DEFAULT NULL,
  `p1_anrede` varchar(10) DEFAULT NULL,
  `p1_vorname` varchar(80) DEFAULT NULL,
  `p1_nachname` varchar(80) DEFAULT NULL,
  `p1_email` varchar(80) DEFAULT NULL,
  `p1_strasse` varchar(80) DEFAULT NULL,
  `p1_str_nr` varchar(10) DEFAULT NULL,
  `p1_plz` varchar(10) DEFAULT NULL,
  `p1_ort` varchar(80) DEFAULT NULL,
  `p1_b_land` varchar(2) DEFAULT NULL,
  `p2_titel` varchar(10) DEFAULT NULL,
  `p2_anrede` varchar(10) DEFAULT NULL,
  `p2_vorname` varchar(80) DEFAULT NULL,
  `p2_nachname` varchar(80) DEFAULT NULL,
  `p2_email` varchar(80) DEFAULT NULL,
  `p2_strasse` varchar(80) DEFAULT NULL,
  `p2_str_nr` varchar(10) DEFAULT NULL,
  `p2_plz` varchar(10) DEFAULT NULL,
  `p2_ort` varchar(80) DEFAULT NULL,
  `p2_b_land` varchar(2) DEFAULT NULL,
  `vertr1_titel` varchar(10) DEFAULT NULL,
  `vertr1_anrede` varchar(10) DEFAULT NULL,
  `vertr1_vorname` varchar(80) DEFAULT NULL,
  `vertr1_nachname` varchar(80) DEFAULT NULL,
  `vertr1_email` varchar(80) DEFAULT NULL,
  `vertr1_strasse` varchar(80) DEFAULT NULL,
  `vertr1_str_nr` varchar(10) DEFAULT NULL,
  `vertr1_plz` varchar(10) DEFAULT NULL,
  `vertr1_ort` varchar(80) DEFAULT NULL,
  `vertr1_b_land` varchar(2) DEFAULT NULL,
  `vertr2_titel` varchar(10) DEFAULT NULL,
  `vertr2_anrede` varchar(10) DEFAULT NULL,
  `vertr2_vorname` varchar(80) DEFAULT NULL,
  `vertr2_nachname` varchar(80) DEFAULT NULL,
  `vertr2_email` varchar(80) DEFAULT NULL,
  `vertr2_strasse` varchar(80) DEFAULT NULL,
  `vertr2_str_nr` varchar(10) DEFAULT NULL,
  `vertr2_plz` varchar(10) DEFAULT NULL,
  `vertr2_ort` varchar(80) DEFAULT NULL,
  `vertr2_b_land` varchar(2) DEFAULT NULL,
  `session_id` varchar(45) DEFAULT NULL,
  `type` varchar(1) DEFAULT NULL COMMENT 'g (glaubiger) | s (schuldner)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Daten für Tabelle `egvp_person`
--

INSERT INTO `egvp_person` (`id`, `p1_titel`, `p1_anrede`, `p1_vorname`, `p1_nachname`, `p1_email`, `p1_strasse`, `p1_str_nr`, `p1_plz`, `p1_ort`, `p1_b_land`, `p2_titel`, `p2_anrede`, `p2_vorname`, `p2_nachname`, `p2_email`, `p2_strasse`, `p2_str_nr`, `p2_plz`, `p2_ort`, `p2_b_land`, `vertr1_titel`, `vertr1_anrede`, `vertr1_vorname`, `vertr1_nachname`, `vertr1_email`, `vertr1_strasse`, `vertr1_str_nr`, `vertr1_plz`, `vertr1_ort`, `vertr1_b_land`, `vertr2_titel`, `vertr2_anrede`, `vertr2_vorname`, `vertr2_nachname`, `vertr2_email`, `vertr2_strasse`, `vertr2_str_nr`, `vertr2_plz`, `vertr2_ort`, `vertr2_b_land`, `session_id`, `type`) VALUES
(11, NULL, NULL, 'Stefanx', 'Weidner', NULL, 'Bringerstreet', '6', '45678', 'Hamburg', 'RP', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'cda7g6fel5cmmc6s2km1fcuq91', 's'),
(12, NULL, NULL, 'Stefanx', 'Weidner', NULL, 'Bringerstreet', '6', '45678', 'Frankfurt', 'SN', NULL, NULL, 'Stefanx', 'Weidner', NULL, 'Hammacherstr', '3443', '12345', 'Frankfurt', 'SN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '89mha0gr3k6f68ncne5an9cod7', 'g'),
(13, NULL, NULL, 'Gertrud', 'Bitkorn', NULL, 'Hammacherstr', '4', '12345', 'Hamburg', 'SL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '89mha0gr3k6f68ncne5an9cod7', 's'),
(14, NULL, NULL, 'Gertrud', 'Sgdragad', NULL, 'Bringerstreet', '3443', '12345', 'Hamburg', 'SN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '89mha0gr3k6f68ncne5an9cod7', 'g'),
(15, NULL, NULL, 'Stefanx', 'Weidner', NULL, 'Bringerstreet', '3443', '12345', 'Frankfurt', 'SN', NULL, NULL, 'Stefanx', 'Bitkorn', NULL, 'Hammacherstr', '4', '12345', 'Frankfurt', 'SN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0s4duicjt88ar8a507gvdsvr64', 's'),
(16, '', 'herr', 'Torsteb', 'arfgfg', 'ich@web.de', 'dfgadfgaf', '424', '34567', 'Hagen', 'HH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jt70f0k7e3fa7flstokh9k5gv6', 'g'),
(17, 'fghdf', 'herr', 'Gertrud', 'xfgjxfgjx', 'fgjxfg@web.de', 'Hammacherstr', '3443', '12345', 'Bochum', 'SL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'kmu95v0nbvrrvblv39mgkqr7a4', 'g'),
(18, 'fghdf', 'Herr', 'Gertrud', 'Sgdragad', 'fgjxfg@web.de', 'Hammacherstr', '6', '45678', 'Frankfurt', 'SL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9mr4pljmgqt3mhe49ksq3p2vl0', 'g'),
(19, 'fghdf', 'herr', 'Stefanx', 'Bitkorn', 'fgjxfg@web.de', 'Hammacherstr', '4', '12345', 'SDFSdf', 'MV', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9mr4pljmgqt3mhe49ksq3p2vl0', 's'),
(20, 'fghdf', 'herr', 'Gertrud', 'Bitkorn', 'fgjxfg@web.de', 'Hammacherstr', '4', '12345', 'Hamburg', 'SL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'k0fa51n9s1n5lkenfrrn5v5k83', 'g'),
(21, '', 'Frau', 'Stefanx', 'Bitkorn', 'fgjxfg@web.de', 'sdgasdgas', '4', '12345', 'Frankfurt', 'NI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8c1t793olbcfljq5ufuhvgbod2', 'g'),
(22, '', 'Frau', 'Stefan', 'Weidner', 'ich@web.de', 'dfgadfgaf', '6', '12345', 'Frankfurt', 'SN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8c1t793olbcfljq5ufuhvgbod2', 's'),
(23, 'fghdf', 'Frau', 'Stefanx', 'Weidner', 'fgjxfg@web.de', 'Hammacherstr', '6', '12345', 'Hagen', 'NW', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5ub10vk1ae7dt2o5t9papf7nr5', 's'),
(24, '', 'Frau', 'Gertrud', 'Bitkorn', 'fgjxfg@web.de', 'Hammacherstr', '4', '12345', 'dsfghfsghs', 'RP', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7j651o6aqe51ppp9167p4eoth3', 'g'),
(25, '', 'Frau', 'Gertrud', 'Bitkorn', 'fgjxfg@web.de', 'Hammacherstr', '4', '12345', 'Hagen', 'SN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hhdsq9var2d748qp3i20qcnpq7', 'g'),
(26, '', 'Frau', 'Gertrud', 'Bitkorn', 'fgjxfg@web.de', 'Hammacherstr', '4', '45678', 'Bochum', 'SN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hhdsq9var2d748qp3i20qcnpq7', 's'),
(27, 'Her', 'Frau', 'Torsten', 'Briesk', 'mail@bitkorn.de', 'Rembergstr', '5', '23456', 'Hagen', 'NI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '93eugtkacqklgj3id50042aob7', 'g'),
(28, 'Her', 'Frau', 'Torsten', 'Briesk', 'mail@bitkorn.de', 'Rembergstr', '5', '23456', 'Hagen', 'SH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Her', 'Herr', 'Torsten', 'Briesk', 'mail@bitkorn.de', 'Rembergstr', '5', '23456', 'Hagen', 'ST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '93eugtkacqklgj3id50042aob7', 's'),
(29, '', 'Frau', 'Giselher', 'Gaukelei', 'giselher@web.de', 'Mauserstraße', '66', '88021', 'Warburg', 'BB', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ar3r3cl8sceeremstimj64n3m3', 's'),
(30, '', 'Frau', 'Peter', 'Briesk', 'mail@bitkorn.de', 'Rembergstr', '6', '44200', 'Warburg', 'SN', '', 'Frau', 'Siegbert', 'Schmidt', 'siggi@web.de', 'Wunderweg', '666', '12345', 'Karlsknall', 'BB', '', 'Frau', 'Gabriel', 'Dussel', 'dussel@gmx.de', 'Wimmelbild', '88', '88726', 'Suchdorf', 'BY', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 't3qeaie386mklo1vs9dboghou1', 'g'),
(31, '', 'Frau', 'Gabriel', 'Dussel', 'giselher@web.de', 'Mauserstraße', '66', '23456', 'Suchdorf', 'SN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'f61c1o3net1fasedc60ka0u0s2', 'g'),
(32, '', 'Frau', 'Siegbert', 'Lustig', 'dussel@gmx.de', 'Wimmelbild', '666', '12345', 'Bochum', 'SL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'f61c1o3net1fasedc60ka0u0s2', 's'),
(33, 'Her', 'Frau', 'Peter', 'Gaukelei', 'giselher@web.de', 'Mauserstraße', '66', '44200', 'Suchdorf', 'SL', 'Her', 'Frau', 'Siegbert', 'Dussel', 'dussel@gmx.de', 'Feilchenweg', '6', '12345', 'Bochum', 'ST', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'd2dk8dbcgvclgerte4grt7au90', 'g'),
(34, '', 'Herr', 'Peter', 'Lustig', 'peter@lustig.de', 'Grünweg', '22', '32345', 'Bochum', 'BE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '24jrjud2t9330j6u2mssvls2h7', 's');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `egvp_xml`
--

CREATE TABLE IF NOT EXISTS `egvp_xml` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anfrage_id` int(11) NOT NULL,
  `xml` text NOT NULL COMMENT 'generiert aus allen Daten',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `egvp_xslt`
--

CREATE TABLE IF NOT EXISTS `egvp_xslt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `xslt` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `display_name` varchar(50) DEFAULT NULL,
  `password` varchar(128) NOT NULL,
  `state` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`user_id`, `username`, `email`, `display_name`, `password`, `state`) VALUES
(1, NULL, 'mail@bitkorn.de', NULL, '$2y$14$Z0s.l50nO2zdFYoyxVu2fu50qcpASt.7J3So50OTqyyrNqWNHXJXm', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_custom`
--

CREATE TABLE IF NOT EXISTS `user_custom` (
  `id` int(11) NOT NULL,
  `vorname` varchar(45) DEFAULT NULL,
  `nachname` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `strasse` varchar(45) DEFAULT NULL,
  `str_nr` varchar(45) DEFAULT NULL,
  `plz` varchar(45) DEFAULT NULL,
  `ort` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `zfca`
--

CREATE TABLE IF NOT EXISTS `zfca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `testtext` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `aes_customer`
--
ALTER TABLE `aes_customer`
  ADD CONSTRAINT `fk_aes_customer_country` FOREIGN KEY (`country`) REFERENCES `aes_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_aes_customer_country_2ed` FOREIGN KEY (`sec_country`) REFERENCES `aes_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `aes_employee`
--
ALTER TABLE `aes_employee`
  ADD CONSTRAINT `fk_aes_employee_userid` FOREIGN KEY (`user_id`) REFERENCES `bitkorn_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `egvp_anfrage`
--
ALTER TABLE `egvp_anfrage`
  ADD CONSTRAINT `fk_egvp_anfrage_glaubGewerbeArt` FOREIGN KEY (`glaub_gewerbe_art`) REFERENCES `egvp_gewerbe_arten` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_egvp_anfrage_schuldGewerbeArt` FOREIGN KEY (`schuld_gewerbe_art`) REFERENCES `egvp_gewerbe_arten` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
