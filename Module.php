<?php

namespace AmandiaES;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ControllerProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;

/**
 * 
 * 
 */
class Module implements AutoloaderProviderInterface, ControllerProviderInterface, ViewHelperProviderInterface, ServiceProviderInterface {
    
    /**
     * \Zend\EventManager\EventInterface
     * @param \Zend\Mvc\MvcEvent $e
     */
    public function onBootstrap(\Zend\Mvc\MvcEvent $e) {
//        $application = $e->getApplication();
//        $logger      = $application->getServiceManager()->get('logger');
//        $logger->log(\Zend\Log\Logger::DEBUG, get_class($e));
        $this->eventManager = $e->getApplication()->getEventManager(); // \Zend\EventManager\EventManager
//        $this->eventManager->attach('render', array($this, 'registerJsonStrategy'), 1000);
        $this->eventManager->attachAggregate(new Listener\AmandiaListener());
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig() {
        $config = array();
        $configFiles = array(
            __DIR__ . '/config/module.config.php',
            __DIR__ . '/config/router.module.config.php'
        );
        foreach ($configFiles as $configFile) {
            $config = \Zend\Stdlib\ArrayUtils::merge($config, include $configFile);
        }
        return $config;
//        return include __DIR__ . '/config/module.config.php';
    }

    public function init(\Zend\ModuleManager\ModuleManager $manager) {
        $events = $manager->getEventManager();
        $sharedEvents = $events->getSharedManager();
        $sharedEvents->attach(__NAMESPACE__, 'dispatch', function($e) {
            /* @var $e \Zend\Mvc\MvcEvent */
            // fired when an ActionController under the namespace is dispatched.
            $controller = $e->getTarget();
//            $routeMatch = $e->getRouteMatch();
            /* @var $routeMatch \Zend\Mvc\Router\RouteMatch */
//            $routeName = $routeMatch->getMatchedRouteName();
            $controller->layout('layout/amandia'); // zu global und maechtig 2014-02-19
        }, 100);
    }

    public function getControllerConfig() {
        return array(
            'factories' => array(
                'AmandiaES\Controller\Aes' => function(\Zend\Mvc\Controller\ControllerManager $cm) {
                    $ctr = new \AmandiaES\Controller\AesController();
                    return $ctr;
                },
                'AmandiaES\Controller\AdminOrder' => function(\Zend\Mvc\Controller\ControllerManager $cm) {
                    $sl = $cm->getServiceLocator();
                    $ctr = new \AmandiaES\Controller\AdminOrderController();
                    $ctr->setProductTable($sl->get('AmandiaES\Table\Product'));
                    $ctr->setOrderTable($sl->get('AmandiaES\Table\Order'));
                    $ctr->setFactoryOrderTable($sl->get('AmandiaES\Table\FactoryOrder'));
                    $ctr->setCostCenterTable($sl->get('AmandiaES\Table\CostCenter'));
                    return $ctr;
                },
                'AmandiaES\Controller\AdminProduct' => function(\Zend\Mvc\Controller\ControllerManager $cm) {
                    $sl = $cm->getServiceLocator();
                    $ctr = new \AmandiaES\Controller\AdminProductController();
                    $ctr->setAccessService($sl->get('BitkornUser\Service\Access'));
                    $ctr->setProductTable($sl->get('AmandiaES\Table\Product'));
                    $ctr->setProductGroupTable($sl->get('AmandiaES\Table\ProductGroup'));
        //                $ctr->setStockProductTable($sl->get('AmandiaES\Table\StockProduct'));
                    $ctr->setMaterialTable($sl->get('AmandiaES\Table\Material'));
                    $ctr->setArticleTable($sl->get('AmandiaES\Table\Article'));
                    $ctr->setSupplierTable($sl->get('AmandiaES\Table\Supplier'));
                    $ctr->setOrderTable($sl->get('AmandiaES\Table\Order'));
                    $ctr->setFactoryOrderTable($sl->get('AmandiaES\Table\FactoryOrder'));
                    $ctr->setFactoryArticleTable($sl->get('AmandiaES\Table\FactoryArticle'));
                    $ctr->setServiceTable($sl->get('AmandiaES\Table\Service'));
                    $ctr->setLangTable($sl->get('AmandiaES\Table\Language\Lang'));
                    return $ctr;
                },
                'AmandiaES\Controller\AdminArticle' => function(\Zend\Mvc\Controller\ControllerManager $cm) {
                    $sl = $cm->getServiceLocator();
                    $ctr = new \AmandiaES\Controller\AdminArticleController();
                    $ctr->setArticleTable($sl->get('AmandiaES\Table\Article'));
                    $ctr->setArticleGroupTable($sl->get('AmandiaES\Table\ArticleGroup'));
                    $ctr->setProductTable($sl->get('AmandiaES\Table\Product'));
                    $ctr->setFactoryArticleTable($sl->get('AmandiaES\Table\FactoryArticle'));
                    $ctr->setSupplierTable($sl->get('AmandiaES\Table\Supplier'));
                    return $ctr;
                },
                'AmandiaES\Controller\AdminMaterial' => function(\Zend\Mvc\Controller\ControllerManager $cm) {
                    $sl = $cm->getServiceLocator();
                    $ctr = new \AmandiaES\Controller\AdminMaterialController();
                    $ctr->setMaterialTable($sl->get('AmandiaES\Table\Material'));
                    $ctr->setMaterialGroupTable($sl->get('AmandiaES\Table\MaterialGroup'));
                    $ctr->setProductTable($sl->get('AmandiaES\Table\Product'));
                    $ctr->setFactoryArticleTable($sl->get('AmandiaES\Table\FactoryArticle'));
                    $ctr->setSupplierTable($sl->get('AmandiaES\Table\Supplier'));
                    return $ctr;
                },
                'AmandiaES\Controller\AdminService' => function(\Zend\Mvc\Controller\ControllerManager $cm) {
                    $sl = $cm->getServiceLocator();
                    $ctr = new \AmandiaES\Controller\AdminServiceController();
                    $ctr->setLogger($sl->get('logger'));
                    $ctr->setServiceTable($sl->get('AmandiaES\Table\Service'));
                    $ctr->setServiceGroupTable($sl->get('AmandiaES\Table\ServiceGroup'));
                    $ctr->setProductTable($sl->get('AmandiaES\Table\Product'));
                    $ctr->setFactoryArticleTable($sl->get('AmandiaES\Table\FactoryArticle'));
                    return $ctr;
                },
                'AmandiaES\Controller\AdminFactoryOrder' => function(\Zend\Mvc\Controller\ControllerManager $cm) {
                    $sl = $cm->getServiceLocator();
                    $ctr = new \AmandiaES\Controller\AdminFactoryOrderController();
                    $ctr->setFactoryOrderTable($sl->get('AmandiaES\Table\FactoryOrder'));
                    $ctr->setFactoryArticleTable($sl->get('AmandiaES\Table\FactoryArticle'));
                    $ctr->setLocationTable($sl->get('AmandiaES\Table\Location'));
                    return $ctr;
                },
                'AmandiaES\Controller\AdminFactoryArticle' => function(\Zend\Mvc\Controller\ControllerManager $cm) {
                    $sl = $cm->getServiceLocator();
                    $ctr = new \AmandiaES\Controller\AdminFactoryArticleController();
                    $ctr->setLogger($sl->get('logger'));
                    $ctr->setFactoryArticleTable($sl->get('AmandiaES\Table\FactoryArticle'));
                    $ctr->setFactoryArticleGroupTable($sl->get('AmandiaES\Table\FactoryArticleGroup'));
                    $ctr->setFactoryOrderTable($sl->get('AmandiaES\Table\FactoryOrder'));
                    $ctr->setProductTable($sl->get('AmandiaES\Table\Product'));
                    $ctr->setLocationTable($sl->get('AmandiaES\Table\Location'));
                    $ctr->setArticleTable($sl->get('AmandiaES\Table\Article'));
                    $ctr->setMaterialTable($sl->get('AmandiaES\Table\Material'));
                    $ctr->setServiceTable($sl->get('AmandiaES\Table\Service'));
                    return $ctr;
                },
                'AmandiaES\Controller\AdminCustomer' => function(\Zend\Mvc\Controller\ControllerManager $cm) {
                    $sl = $cm->getServiceLocator();
                    $ctr = new \AmandiaES\Controller\AdminCustomerController();
                    $ctr->setCustomerTable($sl->get('AmandiaES\Table\Customer'));
                    $ctr->setCustomerGroupTable($sl->get('AmandiaES\Table\CustomerGroup'));
                    $ctr->setOrderTable($sl->get('AmandiaES\Table\Order'));
                    return $ctr;
                },
                /*
                 * REST
                 */
                'AmandiaES\Controller\Rest\RestCustomer' => function(\Zend\Mvc\Controller\ControllerManager $cm) {
                    $sl = $cm->getServiceLocator();
                    $ctr = new \AmandiaES\Controller\Rest\RestCustomerController();
                    $ctr->setCustomerTable($sl->get('AmandiaES\Table\Customer'));
                    return $ctr;
                },
                'AmandiaES\Controller\Rest\RestProductArticle' => function(\Zend\Mvc\Controller\ControllerManager $cm) {
                    $sl = $cm->getServiceLocator();
                    $ctr = new \AmandiaES\Controller\Rest\RestProductArticleController();
                    $ctr->setArticleTable($sl->get('AmandiaES\Table\Article'));
                    $ctr->setProductTable($sl->get('AmandiaES\Table\Product'));
                    return $ctr;
                },
                'AmandiaES\Controller\Rest\RestProductMaterial' => function(\Zend\Mvc\Controller\ControllerManager $cm) {
                    $sl = $cm->getServiceLocator();
                    $ctr = new \AmandiaES\Controller\Rest\RestProductMaterialController();
                    $ctr->setMaterialTable($sl->get('AmandiaES\Table\Material'));
                    $ctr->setProductTable($sl->get('AmandiaES\Table\Product'));
                    $symbolGeomTable = $sl->get('AmandiaES\Table\SymbolGeom');
                    $symbolWeightTable = $sl->get('AmandiaES\Table\SymbolWeight');
                    $ctr->setSymbolsGeom($symbolGeomTable->getSymbolIdNameAssocc());
                    $ctr->setSymbolsWeight($symbolWeightTable->getSymbolIdNameAssocc());
                    return $ctr;
                },
                'AmandiaES\Controller\Rest\RestProductService' => function(\Zend\Mvc\Controller\ControllerManager $cm) {
                    $sl = $cm->getServiceLocator();
                    $ctr = new \AmandiaES\Controller\Rest\RestProductServiceController();
                    $ctr->setServiceTable($sl->get('AmandiaES\Table\Service'));
                    $ctr->setProductTable($sl->get('AmandiaES\Table\Product'));
                    return $ctr;
                },
                'AmandiaES\Controller\Rest\RestProductFactoryArticle' => function(\Zend\Mvc\Controller\ControllerManager $cm) {
                    $sl = $cm->getServiceLocator();
                    $ctr = new \AmandiaES\Controller\Rest\RestProductFactoryArticleController();
                    $ctr->setFactoryArticleTable($sl->get('AmandiaES\Table\FactoryArticle'));
                    $ctr->setProductTable($sl->get('AmandiaES\Table\Product'));
                    return $ctr;
                },
                'AmandiaES\Controller\Rest\RestFactoryArticleArticle' => function(\Zend\Mvc\Controller\ControllerManager $cm) {
                    $sl = $cm->getServiceLocator();
                    $ctr = new \AmandiaES\Controller\Rest\RestFactoryArticleArticleController();
                    $ctr->setFactoryArticleTable($sl->get('AmandiaES\Table\FactoryArticle'));
                    $ctr->setArticleTable($sl->get('AmandiaES\Table\Article'));
                    return $ctr;
                },
                /*
                 * AJAX
                 */
                'AmandiaES\Controller\Ajax\ModalProduct' => function(\Zend\Mvc\Controller\ControllerManager $cm) { // unused 2014-02-21
                    $sl = $cm->getServiceLocator();
                    $ctr = new \AmandiaES\Controller\Ajax\ModalProductController();
                    $ctr->setAccessService($sl->get('BitkornUser\Service\Access'));
                    $ctr->setProductTable($sl->get('AmandiaES\Table\Product'));
                    $ctr->setProductGroupTable($sl->get('AmandiaES\Table\ProductGroup'));
                    $ctr->setStockProductTable($sl->get('AmandiaES\Table\StockProduct'));
                    $ctr->setSupplierTable($sl->get('AmandiaES\Table\Supplier'));
                    return $ctr;
                },
            )
        );
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                /*
                 *  Forms
                 */
                'AmandiaES\Form\Product' => function(\Zend\ServiceManager\ServiceManager $sm) {
                    $productForm = new \AmandiaES\Form\ProductForm();
//                    $productForm->setSupplierTable($sm->get('AmandiaES\Table\Supplier'));
                    $productForm->setProductGroupTable($sm->get('AmandiaES\Table\ProductGroup'));
//                    $productForm->setShipmentTable($sm->get('AmandiaES\Table\Shipment'));
                    $productForm->init();
                    return $productForm;
                },
                'AmandiaES\Form\Order' => function(\Zend\ServiceManager\ServiceManager $sm) {
                    $orderForm = new \AmandiaES\Form\OrderForm();
                    $orderForm->setCostCenterTable($sm->get('AmandiaES\Table\CostCenter'));
                    $orderForm->setShipmentTable($sm->get('AmandiaES\Table\Shipment'));
                    $orderForm->init();
                    return $orderForm;
                },
                'AmandiaES\Form\Article' => function(\Zend\ServiceManager\ServiceManager $sm) {
                    $form = new \AmandiaES\Form\ArticleForm();
                    $form->setArticleGroupTable($sm->get('AmandiaES\Table\ArticleGroup'));
                    $form->setSupplierTable($sm->get('AmandiaES\Table\Supplier'));
                    $form->setSymbolGeomTable($sm->get('AmandiaES\Table\SymbolGeom'));
                    $form->setSymbolWeightTable($sm->get('AmandiaES\Table\SymbolWeight'));
                    $form->init();
                    return $form;
                },
                'AmandiaES\Form\Material' => function(\Zend\ServiceManager\ServiceManager $sm) {
                    $form = new \AmandiaES\Form\MaterialForm();
                    $form->setMaterialGroupTable($sm->get('AmandiaES\Table\MaterialGroup'));
                    $form->setSupplierTable($sm->get('AmandiaES\Table\Supplier'));
                    $form->setSymbolGeomTable($sm->get('AmandiaES\Table\SymbolGeom'));
                    $form->setSymbolWeightTable($sm->get('AmandiaES\Table\SymbolWeight'));
                    $form->init();
                    return $form;
                },
                'AmandiaES\Form\Service' => function(\Zend\ServiceManager\ServiceManager $sm) {
                    $form = new \AmandiaES\Form\ServiceForm();
                    $form->setServiceGroupTable($sm->get('AmandiaES\Table\ServiceGroup'));
                    $form->init();
                    return $form;
                },
                'AmandiaES\Form\FactoryOrder' => function(\Zend\ServiceManager\ServiceManager $sm) {
                    $form = new \AmandiaES\Form\FactoryOrderForm();
                    $form->setLocationTable($sm->get('AmandiaES\Table\Location'));
                    $form->init();
                    return $form;
                },
                'AmandiaES\Form\FactoryArticle' => function(\Zend\ServiceManager\ServiceManager $sm) {
                    $form = new \AmandiaES\Form\FactoryArticleForm();
                    $form->setFactoryArticleGroupTable($sm->get('AmandiaES\Table\FactoryArticleGroup'));
                    $form->setSymbolGeomTable($sm->get('AmandiaES\Table\SymbolGeom'));
                    $form->setSymbolWeightTable($sm->get('AmandiaES\Table\SymbolWeight'));
                    $form->init();
                    return $form;
                },
                'AmandiaES\Form\Customer' => function(\Zend\ServiceManager\ServiceManager $sm) {
                    $form = new \AmandiaES\Form\CustomerForm();
                    $form->setCustomerGroupTable($sm->get('AmandiaES\Table\CustomerGroup'));
                    $form->setCountryTable($sm->get('AmandiaES\Table\Country'));
                    $form->init();
                    return $form;
                },
                /*
                 *  Search Forms
                 */
                'AmandiaES\Form\SearchOrder' => function(\Zend\ServiceManager\ServiceManager $sm) {
                    $form = new \AmandiaES\Form\SearchOrderForm();
                    $form->setCostCenterTable($sm->get('AmandiaES\Table\CostCenter'));
                    $form->init();
                    return $form;
                },
                'AmandiaES\Form\SearchProduct' => function(\Zend\ServiceManager\ServiceManager $sm) {
                    $form = new \AmandiaES\Form\SearchProductForm();
                    $form->setProductGroupTable($sm->get('AmandiaES\Table\ProductGroup'));
//                    $form->setSupplierTable($sm->get('AmandiaES\Table\Supplier'));
                    $form->init();
                    return $form;
                },
                'AmandiaES\Form\SearchArticle' => function(\Zend\ServiceManager\ServiceManager $sm) {
                    $form = new \AmandiaES\Form\SearchArticleForm();
                    $form->setArticleGroupTable($sm->get('AmandiaES\Table\ArticleGroup'));
                    $form->setSupplierTable($sm->get('AmandiaES\Table\Supplier'));
                    $form->init();
                    return $form;
                },
                'AmandiaES\Form\SearchMaterial' => function(\Zend\ServiceManager\ServiceManager $sm) {
                    $form = new \AmandiaES\Form\SearchMaterialForm();
                    $form->setMaterialGroupTable($sm->get('AmandiaES\Table\MaterialGroup'));
                    $form->setSupplierTable($sm->get('AmandiaES\Table\Supplier'));
                    $form->init();
                    return $form;
                },
                'AmandiaES\Form\SearchService' => function(\Zend\ServiceManager\ServiceManager $sm) {
                    $form = new \AmandiaES\Form\SearchServiceForm();
                    $form->setServiceGroupTable($sm->get('AmandiaES\Table\ServiceGroup'));
                    $form->init();
                    return $form;
                },
                'AmandiaES\Form\SearchFactoryOrder' => function(\Zend\ServiceManager\ServiceManager $sm) {
                    $form = new \AmandiaES\Form\SearchFactoryOrderForm();
                    $form->setCostCenterTable($sm->get('AmandiaES\Table\CostCenter'));
                    $form->init();
                    return $form;
                },
                'AmandiaES\Form\SearchFactoryArticle' => function(\Zend\ServiceManager\ServiceManager $sm) {
                    $form = new \AmandiaES\Form\SearchFactoryArticleForm();
                    $form->setFactoryArticleGroupTable($sm->get('AmandiaES\Table\FactoryArticleGroup'));
                    $form->init();
                    return $form;
                },
                'AmandiaES\Form\SearchCustomer' => function(\Zend\ServiceManager\ServiceManager $sm) {
                    $form = new \AmandiaES\Form\SearchCustomerForm();
                    $form->setCustomerGroupTable($sm->get('AmandiaES\Table\CustomerGroup'));
                    $form->init();
                    return $form;
                },
                /*
                 *  Document
                 */
                'AmandiaES\Service\Document' => function(\Zend\ServiceManager\ServiceManager $sm) {
                    $config = $sm->get('config');
                    $service = new \AmandiaES\Service\DocumentService($config['amandia_es']);
                    return $service;
                },
                /*
                 *  Product Text Form
                 */
                'AmandiaES\Form\ProductText' => function(\Zend\ServiceManager\ServiceManager $sm) {
                    $form = new \AmandiaES\Form\ProductTextForm();
                    $form->init(); // wegen der Produkt ID hier noch nicht
                    return $form;
                },
                /*
                 *  Translator
                 */
                'AmandiaES\Service\Translator' => function(\Zend\ServiceManager\ServiceManager $sm) {
                    $translator = new \AmandiaES\Service\Translator();
                    $translator->initLang();
                    return $translator;
                },
                /*
                 *  Calculator
                 */
                'AmandiaES\Service\Shop\Calculator' => function(\Zend\ServiceManager\ServiceManager $sm) {
                    $calculator = new \AmandiaES\Service\Shop\Calculator();
                    $calculator->setArticleTable($sm->get('AmandiaES\Table\Article'));
                    $calculator->setMaterialTable($sm->get('AmandiaES\Table\Material'));
                    $calculator->setServiceTable($sm->get('AmandiaES\Table\Service'));
                    $calculator->setFactoryArticleTable($sm->get('AmandiaES\Table\FactoryArticle'));
                    $calculator->setShipmentTable($sm->get('AmandiaES\Table\Shipment'));
                    return $calculator;
                },
            )
        );
    }

    public function getViewHelperConfig() {
        return array(
            'factories' => array(
                /*
                 * Group Names
                 */
                'materialGroupName' => function (\Zend\View\HelperPluginManager $hpm) {
                    $sl = $hpm->getServiceLocator();
                    $helper = new \AmandiaES\View\Helper\GroupName\MaterialGroupName();
                    $table = $sl->get('AmandiaES\Table\MaterialGroup');
                    $helper->setMaterialGroups($table->getMaterialGroupsIdNameAssocc());
                    return $helper;
                },
                'articleGroupName' => function (\Zend\View\HelperPluginManager $hpm) {
                    $sl = $hpm->getServiceLocator();
                    $helper = new \AmandiaES\View\Helper\GroupName\ArticleGroupName();
                    $table = $sl->get('AmandiaES\Table\ArticleGroup');
                    $helper->setArticleGroups($table->getArticleGroupsIdNameAssocc());
                    return $helper;
                },
                'productGroupName' => function (\Zend\View\HelperPluginManager $hpm) {
                    $sl = $hpm->getServiceLocator();
                    $helper = new \AmandiaES\View\Helper\GroupName\ProductGroupName();
                    $table = $sl->get('AmandiaES\Table\ProductGroup');
                    $helper->setProductGroups($table->getProductGroupsIdNameAssocc());
                    return $helper;
                },
                /*
                 * complete Entity Helper
                 */
                'factoryArticle' => function (\Zend\View\HelperPluginManager $hpm) {
                    $sl = $hpm->getServiceLocator();
                    $helper = new \AmandiaES\View\Helper\FactoryArticle();
                    $helper->setFactoryArticleTable($sl->get('AmandiaES\Table\FactoryArticle'));
                    $helper->setFactoryArticleGroupTable($sl->get('AmandiaES\Table\FactoryArticleGroup'));
                    return $helper;
                },
                /*
                 * Symbols
                 */
                'symbolGeom' => function (\Zend\View\HelperPluginManager $hpm) {
                    $sl = $hpm->getServiceLocator();
                    $helper = new \AmandiaES\View\Helper\SymbolGeom();
                    $symbolGeomTable = $sl->get('AmandiaES\Table\SymbolGeom');
                    $helper->setSymbolsGeom($symbolGeomTable->getSymbolIdNameAssocc());
                    return $helper;
                },
                'symbolWeight' => function (\Zend\View\HelperPluginManager $hpm) {
                    $sl = $hpm->getServiceLocator();
                    $helper = new \AmandiaES\View\Helper\SymbolWeight();
                    $symbolWeightTable = $sl->get('AmandiaES\Table\SymbolWeight');
                    $helper->setSymbolsWeight($symbolWeightTable->getSymbolIdNameAssocc());
                    return $helper;
                },
                /*
                 * 
                 */
                'supplierName' => function (\Zend\View\HelperPluginManager $hpm) {
                    $sl = $hpm->getServiceLocator();
                    $helper = new \AmandiaES\View\Helper\SupplierName();
                    $supplierTable = $sl->get('AmandiaES\Table\Supplier');
                    $helper->setSuppliers($supplierTable->getSuppliersIdNameAssocc());
                    return $helper;
                },
                'orderNr' => function (\Zend\View\HelperPluginManager $hpm) {
                    $sl = $hpm->getServiceLocator();
                    $helper = new \AmandiaES\View\Helper\OrderNr();
                    $helper->setOrderTable($sl->get('AmandiaES\Table\Order'));
                    return $helper;
                },
                'costCenterName' => function (\Zend\View\HelperPluginManager $hpm) {
                    $sl = $hpm->getServiceLocator();
                    $helper = new \AmandiaES\View\Helper\CostCenterName();
                    $table = $sl->get('AmandiaES\Table\CostCenter');
                    $helper->setCostcenters($table->getCostCentersIdNameAssocc());
                    return $helper;
                },
                'locationName' => function (\Zend\View\HelperPluginManager $hpm) {
                    $sl = $hpm->getServiceLocator();
                    $helper = new \AmandiaES\View\Helper\LocationName();
                    $table = $sl->get('AmandiaES\Table\Location');
                    $helper->setLocations($table->getLocationsIdNameAssocc());
                    return $helper;
                },
                'aesTranslate' => function (\Zend\View\HelperPluginManager $hpm) {
                    $helper = new \AmandiaES\View\Helper\AesTranslate();
                    $helper->setTranslator($hpm->getServiceLocator()->get('AmandiaES\Service\Translator'));
                    return $helper;
                },
                'customerNr' => function (\Zend\View\HelperPluginManager $hpm) {
                    $helper = new \AmandiaES\View\Helper\CustomerNr();
                    $helper->setCustomerTable($hpm->getServiceLocator()->get('AmandiaES\Table\Customer'));
                    return $helper;
                },
                'customerDetails' => function (\Zend\View\HelperPluginManager $hpm) {
                    $helper = new \AmandiaES\View\Helper\CustomerDetails();
                    $helper->setCustomerTable($hpm->getServiceLocator()->get('AmandiaES\Table\Customer'));
                    $helper->setCustomerGroupTable($hpm->getServiceLocator()->get('AmandiaES\Table\CustomerGroup'));
                    $helper->setCountryTable($hpm->getServiceLocator()->get('AmandiaES\Table\Country'));
                    return $helper;
                },
                'productDetails' => function (\Zend\View\HelperPluginManager $hpm) {
                    $helper = new \AmandiaES\View\Helper\ProductDetails();
                    $helper->setProductTable($hpm->getServiceLocator()->get('AmandiaES\Table\Product'));
                    return $helper;
                },
            ),
        );
    }

//    public function registerJsonStrategy(\Zend\Mvc\MvcEvent $e) { // muss garnicht sein!!!
//        $controller = $e->getRouteMatch()->getParam('controller');
//        if(FALSE == strpos($controller, 'rest')) {
//            return;
//        }
//        $serviceManager = $e->getApplication()->getServiceManager();
//        $view = $serviceManager->get('Zend\View\View');
//        $jsonStrategy = $serviceManager->get('ViewJsonStrategy');
//        $view->getEventManager()->attach($jsonStrategy, 1000);
//    }
}
